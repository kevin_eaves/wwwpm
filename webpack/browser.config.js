const ExtractTextPlugin = require('extract-text-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const WebpackIsomorphicTools = require('webpack-isomorphic-tools/plugin');
const { resolve } = require('path');

module.exports = {
    entry: './app/client/browser',

    output: {
        path: './build',
        filename: 'scripts/app.js'
    },

    resolve: {
        extensions: [ '', '.js', '.jsx', '.css' ]
    },

    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                include: /app/,
                loader: 'babel',
                query: {
                    presets: [ 'es2015', 'react', 'stage-1' ]
                }
            },
            {
                test: /\.s?css$/,
                loader: ExtractTextPlugin.extract('css?modules&importLoaders=1&localIdentName=[path]__[local]___[hash:base64:5]!sass')
            }
        ]
    },

    sassLoader: {
        includePaths: [
            resolve(__dirname, '../app/styles')
        ]
    },

    plugins: [
        new ExtractTextPlugin('styles/app.css'),
        new WebpackIsomorphicTools(require('./webpack-isomorphic-tools')),
        new OptimizeCssAssetsPlugin({
            assetNameRegExp: /\.css$/g,
            cssProcessor: require('cssnano'),
            cssProcessorOptions: { discardComments: {removeAll: true } },
            canPrint: true
        })
    ]
};
