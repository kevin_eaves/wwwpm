import configureStore from '../common/stores/configureStore';

const preloadedState = typeof window !== 'undefined' ? window.__PRELOADED_STATE__ : {};
const store = configureStore(preloadedState);

export default store;
