import React from 'react';
import ReactDOM from 'react-dom';
import { Router, browserHistory } from 'react-router';
import { Provider } from 'react-redux';
import store from './boot';
import routes from './routes.jsx';
// needed import for HMR to reload main styles.
import Styles from '../styles/main.scss';


import { syncHistoryWithStore } from 'react-router-redux';

const history = syncHistoryWithStore(browserHistory, store);

ReactDOM.render(
<Provider store={store}>
    <Router history={history}>
        { routes }
    </Router>
</Provider>,
document.getElementById('app'));
