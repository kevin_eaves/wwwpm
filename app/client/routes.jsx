import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import slug from 'slug';

// Import Required Action Creators
import {
    partSearch,
    cart,
    user,
    promotions
} from '../common/actions';

import store from './boot';
import PartsTemplate from '../common/containers/PartsTemplateContainer';
import DefaultTemplate from '../common/containers/DefaultTemplateContainer';
import CalloutTemplate from '../common/containers/CalloutTemplateContainer';
import Details from '../common/containers/DetailsContainer';
import Home from '../common/containers/HomeContainer';
import Cart from '../common/containers/CartContainer';
import CartPreview from '../common/containers/CartPreviewContainer';
import MyPaymentCalloutContainer from '../common/containers/MyPaymentCalloutContainer';
import About from '../common/components/About';
import Shipping from '../common/components/Shipping';
import TermsOfUse from '../common/components/TermsOfUse';
import PartListView from '../common/containers/PartNumberSearchResultsContainer';
import Privacy from '../common/components/Privacy';
import reducers from '../common/reducers';
import PageNotFound from '../common/components/PageNotFound';
import ProcessScreen from '../common/containers/ProcessScreenContainer';
import BrowseMakes from '../common/containers/BrowseMakes';
import BrowseModels from '../common/containers/BrowseModels';
import BrowseYearEngine from '../common/containers/BrowseYearEngine';
import OrderInvoice from '../common/containers/OrderInvoice';

function redirectToDetailsIfNeeded(nextState, replaceState, callback) {
    return store.dispatch(partSearch.searchByPartNumber(nextState.location.query.partNumber))
        .then((parts) => {
            if (Object.keys(parts).length === 1) {
                replaceState(parts[0].ProductInfo.DetailsURL);
            }

            return callback();
        })
        .catch(e => {
            replaceState('/404');

            return callback();
        });
}

const resetScrollPosition = () => typeof window !== 'undefined' && window.scrollTo(0, 0);

const legacyProductRedirect = (nextState, replaceState, callback) => {
    const {
        location: {
            pathname
        }
    } = nextState;
    const [
        partNumber
    ] = pathname
        .split('-')
        .slice(-1);

    // Avoid matching against legitimate routes (ie, engines ending with a number)
    if (partNumber.length < 3) {
        return callback();
    }

    return store.dispatch(partSearch.searchByPartNumber(partNumber))
        .then(parts => {
            if (parts.length === 1) {
                replaceState(parts[0].ProductInfo.DetailsURL);
            }

            replaceState(`/search?partNumber=${partNumber}`);
            callback();
        })
        .catch(e => callback());
};

const Routes = (
    <Route onEnter={() => store.dispatch(user.checkIfLoggedIn())}>
        <Route component={CalloutTemplate}>
            <Route
                path="404"
                component={PageNotFound}
            />
        </Route>
        <Route component={DefaultTemplate}>
            <Route
                path="/car-part/**-(:WHIProductId)"
                component={Details}
                onEnter={resetScrollPosition}
                history={browserHistory}
            />
            <Route
                path="/cart"
                component={CartPreview}
                onEnter={resetScrollPosition}
                history={browserHistory}
            />
            <Route
                path="/cart/checkout"
                component={Cart}
                onEnter={resetScrollPosition}
                onLeave={() => store.dispatch(promotions.updatePromo(''))}
                history={browserHistory}
            />
            <Route
                path="cart/checkout/process"
                component={ProcessScreen}
                onEnter={resetScrollPosition}
                history={browserHistory}
            />
            <Route
                path="cart/checkout/success"
                component={MyPaymentCalloutContainer}
                onEnter={resetScrollPosition}
                history={browserHistory}
            />
            <Route
                path="/about"
                component={About}
                onEnter={resetScrollPosition}
                history={browserHistory}
            />
            <Route
                path="/shipping-and-return-warranty"
                component={Shipping}
                onEnter={resetScrollPosition}
                history={browserHistory}
            />
            <Route
                path="/privacy-policy"
                component={Privacy}
                onEnter={resetScrollPosition}
                history={browserHistory}
            />
            <Route
                path="/terms-of-use"
                component={TermsOfUse}
                onEnter={resetScrollPosition}
                history={browserHistory}
            />
            <Route
                path="/search"
                component={PartListView}
                history={browserHistory}
                onEnter={redirectToDetailsIfNeeded}
            />
            <Route
                path="/vehicles"
                component={BrowseMakes}
                history={browserHistory}
                onEnter={resetScrollPosition}
            />
                <Route
                    path="/vehicles/:make"
                    onEnter={resetScrollPosition}
                    component={BrowseModels}
                />
                <Route
                    path="/vehicles/:make/:model"
                    onEnter={resetScrollPosition}
                    component={BrowseYearEngine}
                />
            <Route
                path="/order/:orderId/invoice"
                component={OrderInvoice}
            />
        </Route>
        <Route
            path="/"
            component={PartsTemplate}
        >
            <IndexRoute
                component={Home}
            />
            <Route
                path="((?:[\w-]+)+)*"
                component={Home}
                onEnter={legacyProductRedirect}
                history={browserHistory}
            />
        </Route>
        <Route component={CalloutTemplate}>
            <Route
                onEnter={(nextState, replace) => replace('/404')}
                path="*"
            />
        </Route>
    </Route>
)
export default Routes;
