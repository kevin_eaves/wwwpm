// Server dependencies
import Express from 'express';
import httpolyglot from 'httpolyglot';
import fs from 'fs';
import { join, resolve } from 'path';
import exphbs from 'express-handlebars';
import fetch from 'isomorphic-fetch';
import cookieParser from 'cookie-parser';
import reactCookie from 'react-cookie';
import Helmet from 'react-helmet';
import modRewrite from 'connect-modrewrite';
import compression from 'compression';


// React dependencies
import React from 'react';
import { renderToString } from 'react-dom/server';
import { Provider } from 'react-redux';
import {
    match,
    RouterContext,
    Router
} from 'react-router';

// App dependencies
// import '../jobs';
import serverConfig from '../config/server';
import configureStore from '../common/stores/configureStore';
import routes from '../client/routes.jsx';

const app = new Express();

// This middleware should be placed "high" within the stack to ensure all responses may be compressed.
app.use(compression());

// Setup template engine

app.engine('.hbs', exphbs({
    extname: '.hbs'
}));
app.set('views', join(__dirname, '../common/base'));
app.set('view engine', '.hbs');

// TODO: Remove static app path when images build task is added
app.use(Express.static('app'));
app.use(Express.static('build'));
app.use((req, res, next) => {
    if (!req.secure && process.env.NODE_ENV === 'production') {
        res.writeHead(301,
            {
                'Location': `https://${req.headers.host}${req.url}`
            }
        )
        return res.end();
    }

    return next();
});
app.use(cookieParser());
app.use(modRewrite([`^/api/(.*)$ ${serverConfig.apiHost}/$1 [P]`]));

// Static asset path
app.use(Express.static(join(__dirname, '../../public')))

app.use((req, res, next) => {
    reactCookie.plugToRequest(req, res);
    const store = configureStore({ });

    match({ routes, location: req.url }, (error, redirectionLocation, renderProps) => {
        if (error) {
            return res.status(500).send(error.message);
        }

        if (redirectionLocation) {
            return res.redirect(302, redirectionLocation.pathname + redirectionLocation.search);
        }

        if (renderProps) {
            return getReduxPromise()
                .then(() => {
                    let head = Helmet.rewind();
                    const preloadedState = store.getState();
                    const appComponent = renderToString(
                        <Provider store={store}>
                            <RouterContext
                                {...renderProps}
                            />
                        </Provider>
                    );

                    return res
                        .status(req.url === '/404' ? 404 : 200)
                        .render('index', {
                            appComponent,
                            head,
                            preloadedState: JSON.stringify(preloadedState)
                        });
                })
                .catch((e) => console.warn(e));
        }

        return res.status(404).redirect('/404');

        function getReduxPromise () {
            const {
                location: {
                    query
                },
                params,
                components
            } = renderProps;
            const routeComponent = components[components.length - 1].WrappedComponent;

            if (routeComponent && typeof routeComponent.preloadData === 'function') {
                return routeComponent.preloadData.call(routeComponent, {
                    params,
                    query,
                    store
                });
            }

            return Promise.resolve();
        }
    });
});

httpolyglot
    .createServer(
        process.env.NODE_ENV === 'production'
            ?   {
                    key: fs.readFileSync('/home/pmadmin/ssl_digicert/partsmonkey_com.key'),
                    cert: fs.readFileSync('/home/pmadmin/ssl/test.crt', 'utf8')
                }
            :   {},
        app
    )
    .listen(3000, err => {
        if (err) {
            return console.log(err);
        }

        console.log(`Listening on 3000`);
    });
