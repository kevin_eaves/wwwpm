import React from 'react';
import { Grid, Row, Col, Clearfix } from 'react-bootstrap';
import PartItem from '../../containers/PartItemContainer';
import { scroller } from 'react-scroll';
import style from './style.scss';

const PartNumberSearchResultsContainer = ({ parts, title = 'Search Results' }) => (
    <div className={style.container} id="searchResultsContainer">
        <h2 className="_fancyUnderline">{title}</h2>
        <Row>
            {
                Object.keys(parts).map((key) => {
                    const item = parts[key];

                    if(!item.ProductInfo){
                        console.warn('Item missing product info', item);
                        return;
                    }

                    const {
                        Images,
                        PartLabel,
                        PartNumber,
                        ManuFacturerName,
                        Inventory,
                        WHIProductId,
                        DetailsURL
                    } = item.ProductInfo;
                    const price = {
                        value: Inventory.Cost,
                        per:   Inventory.SellingMultiple
                    }

                    let partImage = null;
                    if(Images){
                        if (Images.indexOf(';') > -1) {
                            partImage = Images.substr(0, Images.indexOf(';'));
                        } else {
                            partImage = Images;
                        }
                    }

                    return (
                        <Col md={6}>
                            <PartItem
                                key={key}
                                id={PartNumber}
                                image={partImage}
                                title={PartLabel}
                                partNumber={PartNumber}
                                manufacturer={ManuFacturerName}
                                note={item.NoteText}
                                price={price}
                                link={DetailsURL}
                                quantity={Inventory.QuantityOnHand}
                                PartInventoryId={Inventory.PartInventoryId}
                                isRemoteInventory={Inventory.isRemoteInventory}
                                WHIProductId={WHIProductId}
                            />
                        </Col>
                    )
                })
            }
        </Row>
        <Row className={style.scrollToTop}>
            <button
                onClick={() => {
                    scroller.scrollTo('searchResultsContainer', {
                        duration: 500,
                        delay: 25,
                        offset: -175,
                        smooth: true
                    })
                }}
                className="_button"
            >
                Scroll Back To Top
            </button>
        </Row>
    </div>
)

export default PartNumberSearchResultsContainer;
