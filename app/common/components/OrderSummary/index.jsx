import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { submit } from 'redux-form';
import style from './style.scss';
import OrderSummaryItem from '../../containers/OrderSummaryItem';
import Loader from '../Loader';

const OrderSummary = ({
    parts,
    fees,
    paymentTypes,
    shippingSelected
}) => (
    <div className={style.container}>
        <h2 className="_mediumTitle _noTransform _withBackground">Order Summary <Link to="/cart">Edit</Link></h2>
        <ul>
            {
                parts.map(({ ProductInfo } = {}, i) => (
                    <li>
                        {
                            ProductInfo
                            ?   <OrderSummaryItem id={ProductInfo.WHIProductId} />
                            :   <Loader text="Loading part&hellip;" />

                        }
                    </li>
                ))
            }
        </ul>
        <dl>
            <dt>Subtotal</dt>
            <dd>${Number(fees.subWithoutCore).toLocaleString('en', {
                        style: 'decimal',
                        minimumFractionDigits: 2
                    })}&nbsp;CAD
            </dd>
            { fees.discount > 0 && <dt>Promo Discount</dt> }
            { fees.discount > 0 && <dd>(-{fees.discountPercent}%) <span>-${Number(fees.discount).toLocaleString('en', {style: 'decimal', minimumFractionDigits: 2})}&nbsp;CAD</span></dd> }
            { fees.ehc > 0 && <dt>EHC</dt> }
            { fees.ehc > 0 && <dd>${Number(fees.ehc).toLocaleString('en', {style: 'decimal', minimumFractionDigits: 2})}&nbsp;CAD</dd> }
            { shippingSelected && <dt>Shipping</dt> }
            { shippingSelected && <dd>{ fees.shipping > 0 ? `$${Number(fees.shipping).toLocaleString('en', {style: 'decimal', minimumFractionDigits: 2})} CAD` : 'Free' }</dd> }
            { shippingSelected && fees.insuranceCost > 0 && <dt>Shipping Insurance</dt> }
            { shippingSelected && fees.insuranceCost > 0 && <dd>${Number(fees.insuranceCost).toLocaleString('en', {style: 'decimal', minimumFractionDigits: 2})}&nbsp;CAD</dd> }
            { fees.freight > 0 && <dt>Freight</dt> }
            { fees.freight > 0 && <dd>{fees.freight > 0 ? `$${Number(fees.freight).toLocaleString('en', {style: 'decimal', minimumFractionDigits: 2})} CAD` : 'N/A' }</dd> }
            { fees.core > 0 && <dt>Core Cost</dt> }
            { fees.core > 0 && <dd>{fees.core > 0 ? `$${Number(fees.core).toLocaleString('en', {style: 'decimal', minimumFractionDigits: 2})} CAD` : 'N/A' }</dd> }
            { fees.taxCode && <dt>Tax ({ fees.taxCode ? fees.taxCode : 'HST' } { fees.taxPercent ? fees.taxPercent : '13'}%)</dt> }
            { fees.taxCode && <dd>${Number(fees.tax).toLocaleString('en', {style: 'decimal', minimumFractionDigits: 2})}&nbsp;CAD</dd> }
        </dl>
        <div className={style.total}>
            <p>Order Total<span>${Number(fees.total).toLocaleString('en', {style: 'decimal', minimumFractionDigits: 2})}&nbsp;CAD</span></p>
        </div>
    </div>
);

export default connect()(OrderSummary);
