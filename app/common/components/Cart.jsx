import React from 'react';
import { withState, compose } from 'recompose';
import CartFooter from './CartFooter';
import StickySideBar from './StickySideBar';
import { scroller } from 'react-scroll';
import OrderSummary from '../containers/OrderSummary';
import ProcessCheckout from '../containers/CheckoutContainer';
import { Col, Clearfix } from 'react-bootstrap';
import Helmet from './Helmet';

const enhance = compose(
    withState('isSubmitting', 'setIsSubmitting', false),
    withState('scrollToErrors', 'setScrollToErrors', false)
);
const MyCart = enhance(({ pathname, title, openLoginModal, paymentTypes, remoteSubmit, isSubmitting, setIsSubmitting, scrollToErrors, setScrollToErrors }) => (
    <Clearfix>
        <Helmet
            title={title}
        />
        <Col md={12}>
            <h1 className="_largeTitle _noTransform _withSubHeading">Checkout</h1>
        </Col>
        <Col md={7} id="cartProcessingContainer">
            <ProcessCheckout pathname={pathname} setIsSubmitting={setIsSubmitting} previousSubmitValue={isSubmitting} scrollToErrors={scrollToErrors} />
        </Col>
        <Col md={5} id="checkoutSidebar">
            <StickySideBar bottomBoundry="cartProcessingContainer">
                <OrderSummary />
            </StickySideBar>
        </Col>
        <Col md={7}>
            <Col md={12}>
                <button
                    type="button"
                    className={`_button ${ paymentTypes != 'paypal' ? '_green' : '_yellow _light'} _pullRight _block`}
                    disabled={isSubmitting ? true : false}
                    onClick={() => {
                        remoteSubmit(paymentTypes);
                        setScrollToErrors(true);
                        setTimeout(() => checkForErrors(true), 500);
                        setTimeout(() => setScrollToErrors(false), 12000);
                    }}
                >
                    { isSubmitting && 'Processing...' }
                    { paymentTypes != 'paypal' && !isSubmitting && 'Place Order' }
                    { paymentTypes == 'paypal' && !isSubmitting && 'Pay With  ' }
                    { paymentTypes == 'paypal' && !isSubmitting && <img src="../../images/paypal-icon.svg" onerror="this.src='../../images/paypal-icon.png'" alt="PayPal Icon" width="100" /> }
                </button>
            </Col>
        </Col>
    </Clearfix>
));

function checkForErrors (error) {
    if (document.querySelector('.error')) {
        const firstErrorElement = document.querySelector('.error').id;
        scroller.scrollTo(firstErrorElement, {
            duration: 500,
            offset: -175,
            smooth: true
        })
    }
}


export default MyCart;
