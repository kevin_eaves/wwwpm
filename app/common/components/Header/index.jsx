import React from 'react';
import style from './style.scss';

const Header = ({ children }) => (
    <div className={style.header}>
        { children }
    </div>
);

export default Header;
