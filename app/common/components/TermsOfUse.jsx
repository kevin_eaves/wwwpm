import React from 'react';
import { Link } from 'react-router';
import Helmet from './Helmet';

class TermsOfUse extends React.Component {
    render(){
        return(
            <div>
                <Helmet
                    title="Terms of Use"
                />
                <div className="heading">
                    <h2 className="_fancyUnderline">Privacy</h2>
                </div>
                <p>Please review our <Link to="/privacy-policy">Privacy Policy</Link>, which also governs your visit to PartsMonkey, to understand our practices.</p>
                <div className="heading">
                    <h2 className="_fancyUnderline">Electronic Communications</h2>
                </div>
                <p>When you visit and/or use this website or send e-mails to us, you are communicating with us electronically. You consent to receive communications from us electronically. We will communicate with you by e-mail or by posting notices on this site. You agree that all agreements, notices, disclosures and other communications that we provide to you electronically satisfy any legal requirement that such communications be in writing.</p>
                <div className="heading">
                    <h2 className="_fancyUnderline">Copyright</h2>
                </div>
                <p>All content included on this site, such as text, graphics, logos, button icons, images, audio clips, digital downloads, data compilations, and software, is the property of PartsMonkey or its content suppliers and protected by Canadian, American and international copyright laws. The compilation of all content on this site is the exclusive property of PartsMonkey and protected by copyright laws. All software used on this site is the property of PartsMonkey or its software suppliers and protected by copyright laws.</p>
                <div className="heading">
                    <h2 className="_fancyUnderline">Trademarks</h2>
                </div>
                <p>PartsMonkey and other marks indicated on our site are registered trademarks of PartsMonkey or its subsidiaries, in Canada, the United States and other countries. Other PartsMonkey graphics, logos, page headers, button icons, scripts, and service names are trademarks or trade dress of PartsMonkey or its subsidiaries. PartsMonkey's trademarks and trade dress may not be used in connection with any product or service that is not PartsMonkey's, in any manner that is likely to cause confusion among customers or the general public, or in any manner that causes loss, disparages or discredits PartsMonkey. All other trademarks not owned by PartsMonkey or its subsidiaries that appear on this site are the property of their respective owners, who may or may not be affiliated with, connected to, or sponsored by PartsMonkey or its subsidiaries.</p>
                <div className="heading">
                    <h2 className="_fancyUnderline">License and site access</h2>
                </div>
                <p>PartsMonkey grants you a limited license to access and make personal use of this site and not to download (other than page caching) or modify it, or any portion of it, except with express written consent of PartsMonkey. This license does not include any resale or commercial use of this site or its contents; any collection and use of any product listings, descriptions, or prices; any derivative use of this site or its contents; any downloading or copying of account information for the benefit of another merchant; or any use of data mining, robots, or similar data gathering and extraction tools. This site or any portion of this site may not be reproduced, duplicated, copied, sold, resold, visited, or otherwise exploited for any commercial purpose without express written consent of PartsMonkey. You may not frame or utilize framing techniques to enclose any trademark, logo, or other proprietary information (including images, text, page layout, or form) of PartsMonkey and our affiliates without express written consent. You may not use any meta tags or any other "hidden text" utilizing PartsMonkey's name or trademarks without the express written consent of PartsMonkey. Any unauthorized use terminates the permission or license granted by PartsMonkey. You are granted a limited, revocable, and nonexclusive right to create a hyperlink to the home page of PartsMonkey so long as the link does not portray PartsMonkey, its affiliates, or their products or services in a false, misleading, derogatory, or otherwise offensive matter. You may not use any PartsMonkey logo or other proprietary graphic or trademark as part of the link without express written permission.</p>
                <div className="heading">
                    <h2 className="_fancyUnderline">Your Account</h2>
                </div>
                <p>If you use this site, you are responsible for maintaining the confidentiality of your account and password and for restricting access to your computer, and you agree to accept responsibility for all activities that occur under your account or password. PartsMonkey and its affiliates reserve the right to refuse service, terminate accounts, remove or edit content, or cancel orders in their sole discretion.</p>
                <div className="heading">
                    <h2 className="_fancyUnderline">Disclaimer Of Warranties And Limitation Of Liability</h2>
                </div>
                <p>THIS SITE IS PROVIDED BY PartsMonkey ON AN "AS IS" AND "AS AVAILABLE" BASIS. PartsMonkey SPECIFICALLY WARRANTS AND GUARANTEES THE ACCURACY OF ITS SEARCH REPORTS. THE INFORMATION CONTAINED IN OUR REPORTS REPRESENTS THE LIEN RECORDS AND DATA OF THE GOVERNMENT REGISTRARS ON OR BEFORE THE DATE AND TIME INDICATED ON THE REPORT. PartsMonkey MAKES NO REPRESENTATIONS OR WARRANTIES REGARDING ANY LIENS OR SECURITY INTERESTS FILED AFTER THE DATE AND TIME INDICATED ON THE REPORT FOR ANY SPECIFIC VEHICLE IDENTIFICATION NUMBER. IN ADDITION, PartsMonkey SPECIFICALLY DISCLAIMS ANY LIABILITY FOR THE CRIMINAL OR FRAUDULENT ACTS OF THIRD PARTIES. PartsMonkey MAKES NO WARRANTIES OR REPRESENTATIONS AS TO THE TURN AROUND TIME OF ITS SEARCH REPORTS. BY ACCEPTING THIS LICENSE YOU SPECIFICALLY UNDERSTAND THAT UNFORESEEN DELAYS IN THE TRANSMISSION OF DATA BETWEEN PartsMonkey AND THE DIFFERENT GOVERNMENTAL AGENCIES OF CANADA HAPPEN FROM TIME TO TIME. PartsMonkey IS NOT RESPONSIBLE FOR ANY DAMAGES, DIRECT, CONSEQUENTIAL, PUNITIVE OR OTHERWISE, THAT YOU MAY SUFFER AS A RESULT OF AN UNUSUAL DELAY IN THE TRANSMISSION OF OUR REPORTS. BY ACCEPTING THIS LICENSE TO USE OUR WEBSITE YOU ALSO AGREE THAT, AS A CONDITION OF ANY PAYMENT TO YOU, YOUR SUCCESSORS OR ASSIGNS, BY PartsMonkey AS PART OF ITS GUARANTEE OF THE ACCURACY OF ITS REPORTS, YOU, YOUR SUCCESSORS AND ASSIGNS WILL SUBROGATE ANY AND ALL RIGHTS THAT YOU MAY HAVE AGAINST ANY PARTY THAT CAUSED THE LIEN OR SECURITY INTEREST TO BE FILED.</p>
                <div className="heading">
                    <h2 className="_fancyUnderline">Applicable Law</h2>
                </div>
                <p>By visiting PartsMonkey, you agree that the laws of the province of Ontario, without regard to principles of conflict of laws, will govern these Conditions of Use and any dispute of any sort that might arise between you and PartsMonkey or its affiliates.</p>
                <div className="heading">
                    <h2 className="_fancyUnderline">Disputes</h2>
                </div>
                <p>Any dispute relating in any way to your visit to PartsMonkey or to products you purchase through PartsMonkey shall be submitted to confidential arbitration in London, Ontario, except that, to the extent you have in any manner violated or threatened to violate PartsMonkey 's intellectual property rights, PartsMonkey may seek injunctive or other appropriate relief in any state or federal court in the province of Ontario, and you consent to exclusive jurisdiction and venue in such courts. Arbitration under this agreement shall be conducted under the rules then prevailing of the Arbitration Act of Ontario 1991 S.O. c. 17. The arbitrator's award shall be binding and may be entered as a judgment in any court of competent jurisdiction. To the fullest extent permitted by applicable law, no arbitration under this Agreement shall be joined to an arbitration involving any other party subject to this Agreement, whether through class arbitration proceedings or otherwise.</p>
                <div className="heading">
                    <h2 className="_fancyUnderline">Site Policies, Modification, And Severability</h2>
                </div>
                <p>Please review our other policies, such as our Privacy Notice, posted below. These policies also govern your visit to and use of PartsMonkey. We reserve the right to make changes to our site, policies, and these Conditions of Use at any time. If any of these conditions shall be deemed invalid, void, or for any reason unenforceable, that condition shall be deemed severable and shall not affect the validity and enforceability of any remaining condition.</p>
                <p><em>PartsMonkey and its affiliates provide their services to you subject to the following conditions. If you visit and use the PartsMonkey website, you accept these conditions. Please read them carefully.</em></p>

                <Link to="/" className="_button _grey _dark _mediumPadding">Find parts now</Link>
            </div>
        );

    }
}

export default TermsOfUse;
