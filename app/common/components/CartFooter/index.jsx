import React from 'react';
import { Link } from 'react-router';
import style from './style.scss';

const CartFooter = ({ children }) => (
    <div className={style.container}>
        {children}
    </div>
);

export default CartFooter;
