import React from 'react';
import { Link } from 'react-router';
import slug from 'slug';
import Helmet from '../Helmet';
import Loader from '../Loader';
import style from './style.scss';
import { scroller } from 'react-scroll';

export default ({
    title = 'Results',
    instructionalText = 'Select an option',
    results,
    isFetching = true,
    callback,
    extraColumns = false,
    reverse = false,
    description
}) => {
    const categories = Object.keys(results);
    if (reverse) {
        categories.reverse();
    }
    return (
        <div className={style.container}>
            <Helmet
                title={title}
            />
            <div className="heading">
                <p>{instructionalText}</p>
                <h1>Results for <Link to="/vehicles">{title}</Link></h1>
                {description && <p className="normalize" dangerouslySetInnerHTML={{__html: description}}></p>}
            </div>
            {
                isFetching
                    ?   <div className={`_centerText ${style.loading}`}>
                            <Loader /><br />
                            <span className="_subHeading">Loading&hellip;</span>
                        </div>
                    :   <div>
                            {
                                Object.keys(results).length
                                ?   <div>
                                        <JumpNav categories={categories} />
                                        {
                                            categories.map((category, i) => (
                                                <section id={category} key={i}>
                                                    <h2>{category}</h2>
                                                    <ul className={`${style.category} ${extraColumns && style.wideColumns}`}>
                                                        {
                                                            results[category].map((item, i) => (
                                                                <li key={i}>
                                                                    {
                                                                        callback
                                                                            ?   <a onClick={() => callback(item)}>
                                                                                    {item.name}
                                                                                </a>
                                                                            :   <Link to={item.url}>
                                                                                    {item.name}
                                                                                </Link>
                                                                    }
                                                                </li>
                                                            ))
                                                        }
                                                    </ul>
                                                </section>
                                            ))
                                        }
                                        <JumpNav categories={categories} />
                                    </div>
                                :   <div className="_centerText">
                                        <p className="_subHeading">There are no engines for this vehicle</p>
                                    </div>
                            }
                        </div>
            }
            <p className="_centerText">If you need assistance, call us at <a className="_textLink" href="tel:18552237278">1-855-223-7278</a> to speak to one of our parts experts.</p>
        </div>
    );
};

const JumpNav = ({ categories = [] }) => (
    <div className={`_centerText ${style.jumpNav}`}>
        <span>Jump to section:</span>
        {
            categories.map((category, i) => (
                <a
                    className="_textLink"
                    key={i}
                    onClick={() => {
                        scroller.scrollTo(category, {
                            duration: 500,
                            offset: -100,
                            smooth: true
                        });
                        location.href = `#${category}`;
                    }}
                >
                    {category}
                </a>
            ))
        }
        <select
            onChange={(e) => {
                scroller.scrollTo(e.target.value, {
                    duration: 500,
                    smooth: true
                });
                location.href = `#${e.target.value}`;
            }}
        >
            {
                categories.map((category, i) => (
                    <option
                        key={i}
                        value={category}
                    >
                        {category}
                    </option>
                ))
            }
        </select>
    </div>
)
