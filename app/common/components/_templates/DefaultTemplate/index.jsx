import "babel-polyfill";
import React from 'react';
import style from './style.scss';
import { Grid, Row, Col, Clearfix } from 'react-bootstrap';
import Header from '../../Header';
import Footer from '../../Footer';
import VeggieBurger from '../../VeggieBurger';
import Navigation from '../../NavigationControlHeader';
import Modal from '../../../containers/Modal';
import ContactForm from '../../../containers/ContactContainer';
import LoginForm from '../../../containers/LoginContainer';
import YearEngineFormContainer from '../../../containers/YearEngineFormContainer';
import QuoteCodeForm from '../../../containers/QuoteCodeContainer';
import CartUpdated from '../../../containers/CartUpdated';
import PartNumberSearch from '../../NavigationControlHeader/PartNumberSearch';

class App extends React.Component {
    constructor () {
        super();

        this.state = {
            toggleMobileMenu: false,
            showSearchBar: false
        };
    }

    componentDidMount = () => {
        window.addEventListener('scroll', this.handleScroll);
    }

    componentWillUnmount = () => {
        window.removeEventListener('scroll', this.handleScroll);
    }
    handleScroll = (event) => {
        let scrollTop = event.pageY || event.pageY == 0
            ?   event.pageY
            :   event.srcElement.body.scrollTop,
            scrollPosition = Math.min(0, scrollTop/3 - 30);

        this.setState({
          pageScrolled: scrollPosition == 0 ? true : false
        });
    }

    toggleVeggieburger = () => this.setState({
        toggleMobileMenu: !this.state.toggleMobileMenu
    })

    render () {
        const {
            children,
            handleModalRequest,
            cartCount,
            handleLogout
        } = this.props;

        const {
            pageScrolled,
            showSearchBar
        } = this.state;

        return (
            <div className={`${style.container} ${this.state.toggleMobileMenu && style.toggleMobileMenu}`}>
                <div
                    onClick={this.toggleVeggieburger}
                    className={style.veggieContainer}
                >
                    <VeggieBurger toggle={this.state.toggleMobileMenu && 'open'} />
                </div>
                <Header>
                    <Navigation
                        theme="simple"
                        toggleMobileMenu={this.state.toggleMobileMenu}
                        handleModalRequest={handleModalRequest}
                        cartCount={cartCount}
                        handleLogout={handleLogout}
                        searchable={pageScrolled}
                        toggleMenuDrawer={this.toggleVeggieburger}
                        toggleSearchBar={() => this.setState({
                            showSearchBar: !showSearchBar
                        })}
                    />
                    <PartNumberSearch
                        isVisible={showSearchBar}
                    />
                </Header>

                <Grid>
                  <section>
                       { children }
                  </section>
                </Grid>
                <Footer />
                <Modal modalIsFor="LoginModal">
                    <LoginForm />
                </Modal>
                <Modal modalIsFor="ContactModal">
                    <ContactForm />
                </Modal>
                <Modal modalIsFor="QuoteCodeModal">
                    <QuoteCodeForm />
                </Modal>
                <Modal modalIsFor="CartUpdated">
                    <CartUpdated />
                </Modal>
                <Modal modalIsFor="AvailableYears">
                    <YearEngineFormContainer />
                </Modal>
            </div>
        )
    }
}

export default App;
