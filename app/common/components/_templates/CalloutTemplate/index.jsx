import "babel-polyfill";
import React from 'react';
import style from './style.scss';

import { Grid, Row, Col, Clearfix } from 'react-bootstrap';
import Header from '../../Header';
import Footer from '../../Footer';
import VeggieBurger from '../../VeggieBurger';
import Navigation from '../../NavigationControlHeader';
import Modal from '../../../containers/Modal';
import ContactForm from '../../../containers/ContactContainer';
import LoginForm from '../../_forms/Login';

class App extends React.Component {
    constructor () {
        super();

        this.state = {
            toggleMobileMenu: false
        };
    }
    componentDidMount = () => {
        window.addEventListener('scroll', this.handleScroll);
    }

    componentWillUnmount = () => {
        window.removeEventListener('scroll', this.handleScroll);
    }
    handleScroll = (event) => {
        let scrollTop = event.pageY || event.pageY == 0
            ?   event.pageY
            :   event.srcElement.body.scrollTop,
            scrollPosition = Math.min(0, scrollTop/3 - 60);

        this.setState({
          pageScrolled: scrollPosition == 0 ? true : false
        });
    }
    toggleVeggieburger = () => {
        this.setState({toggleMobileMenu: !this.state.toggleMobileMenu});
    }
    render () {
        const {
            children,
            handleModalRequest,
            cartCount,
            wipeSearchHistory
        } = this.props;

        const {
            pageScrolled
        } = this.state;

        return (
            <div className={`${style.container} ${this.state.toggleMobileMenu && style.toggleMobileMenu}`}>
                <Grid className="keepWidthLimit">
                  <section>
                       { children }
                  </section>
                </Grid>
                <Footer />
                <Modal modalIsFor="LoginModal">
                    <LoginForm />
                </Modal>
                <Modal modalIsFor="ContactModal">
                    <ContactForm />
                </Modal>
            </div>
        )
    }
}

export default App;
