import "babel-polyfill";
import React from 'react';
import style from './style.scss';
import { Grid, Row, Col, Clearfix } from 'react-bootstrap';
import Helmet from '../../Helmet';
import Header from '../../Header';
import Footer from '../../Footer';
import VeggieBurger from '../../VeggieBurger';
import CarSearch from '../../../containers/CarSearchContainer';
import Navigation from '../../NavigationControlHeader';
import Modal from '../../../containers/Modal';
import ContactForm from '../../../containers/ContactContainer';
import LoginForm from '../../../containers/LoginContainer';
import PartNumberSearch from '../../../containers/PartNumberSearchContainer';
import RequestAvailability from '../../../containers/RequestAvailabilityContainer';
import RequestVehicleAvailability from '../../../containers/RequestVehicleAvailabilityContainer';
import CartUpdated from '../../../containers/CartUpdated';
import PartNumberSearchSticky from '../../NavigationControlHeader/PartNumberSearch';

class App extends React.Component {
    constructor () {
        super();

        this.state = {
            toggleMobileMenu: false,
            pageScrolled: false
        };
    }

    componentDidMount = () => {
        window.addEventListener('scroll', this.handleScroll);
    }
    componentWillUnmount = () => {
        window.removeEventListener('scroll', this.handleScroll);
    }
    handleScroll = (event) => {
        let scrollTop = 0;
        if (event.pageY || event.pageY == 0) {
            scrollTop = event.pageY;
        } else {
            scrollTop = event.srcElement.body.scrollTop;
        }

        const scrollPosition = Math.min(0, Math.floor(scrollTop / 3 - 90));

        this.setState({
            pageScrolled: scrollPosition === 0
        });
    }
    toggleVeggieburger = () => this.setState({
        toggleMobileMenu: !this.state.toggleMobileMenu
    })

    render () {

        const {
            children,
            handleModalRequest,
            cartCount,
            handleLogout
        } = this.props;

        const {
            pageScrolled
        } = this.state;

        return (
            <div className={`${style.container} ${this.state.toggleMobileMenu && style.toggleMobileMenu} searchTemplate`}>
                <Helmet />
                <div
                    onClick={this.toggleVeggieburger}
                    className={style.veggieContainer}
                >
                    <VeggieBurger toggle={this.state.toggleMobileMenu && 'open'} />
                </div>
                <Header>
                    <Navigation
                        handleModalRequest={handleModalRequest}
                        toggleMobileMenu={this.state.toggleMobileMenu}
                        cartCount={cartCount}
                        searchable={pageScrolled}
                        handleLogout={handleLogout}
                        toggleMenuDrawer={this.toggleVeggieburger}
                    />
                    <PartNumberSearchSticky
                        isVisible={pageScrolled}
                    />
                    <Grid>
                        <CarSearch />
                        <hr className={style.divider} />
                        <Row className={style.partNumberSearch}>
                            <Col xs={6}>
                                <p>Already know the part you're looking for?</p>
                            </Col>
                            <Col xs={6} className={style.partSearchForm}>
                                <PartNumberSearch />
                            </Col>
                        </Row>
                    </Grid>
                </Header>
                <div className={style.banner}>
                    <img src="../../images/parts-banner.png" alt="parts banner image"/>
                </div>
                <Grid>
                   { children }
                </Grid>
                <Footer />
                <Modal modalIsFor="LoginModal">
                    <LoginForm />
                </Modal>
                <Modal modalIsFor="ContactModal">
                    <ContactForm />
                </Modal>
                <Modal modalIsFor="RequestAvailabilityModal">
                    <RequestAvailability />
                </Modal>
                <Modal modalIsFor="RequestVehiclePartsModal">
                    <RequestVehicleAvailability />
                </Modal>
                <Modal modalIsFor="CartUpdated">
                    <CartUpdated />
                </Modal>
            </div>
        )
    }
}

export default App;
