import "babel-polyfill";
import React from 'react';
import style from './style.scss';

import { Grid, Row, Col, Clearfix } from 'react-bootstrap';
import Header from '../../Header';
import Footer from '../../Footer';
import VeggieBurger from '../../VeggieBurger';
import Navigation from '../../NavigationControlHeader';


class App extends React.Component {
    constructor () {
        super();

        this.state = {
            toggleMobileMenu: false
        };
    }
    toggleVeggieburger = () => {
        if(this.state.toggleMobileMenu){
            this.setState({toggleMobileMenu: false});
        }else{
            this.setState({toggleMobileMenu: true});
        }
    }
    render () {
        const {
            children
        } = this.props;
        return (
            <div className={`${style.container} ${this.state.toggleMobileMenu && style.toggleMobileMenu}`}>
                <div onClick={this.toggleVeggieburger} className={style.veggieContainer}>
                    <VeggieBurger toggle={this.state.toggleMobileMenu && 'open'} />
                </div>
                <Header theme="light">
                    <Navigation toggleMobileMenu={this.state.toggleMobileMenu}/>
                </Header>
                <Grid>
                   { children }
                </Grid>
                <Footer />
            </div>
        )
    }
}

export default App;
