import React from 'react';
import style from './style.scss';

const Loader = ({ size = 'medium', color = 'defaultColor', text }) => (
    <div className={style.container}>
        <div className={`${style.loader} ${style[size]} ${style[color]}`}>
            <i></i>
            <i></i>
            <i></i>
        </div>
        {text && <p className="_subHeading">{text}</p>}
    </div>
);

export default Loader;
