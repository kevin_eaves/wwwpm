import React from 'react';
import { Link } from 'react-router';
import { Grid, Row, Col, Clearfix } from 'react-bootstrap';
import CreateAccount from '../../containers/CreateAccountContainer';
import style from './style.scss';

const MyPaymentCallout = ({
    orderNumber,
    email
}) => (
    <Grid className={style.container}>
        <Row className={style.heading}>
            <div>
                <h2>Order Complete</h2>
            </div>
        </Row>
        <Row>
            <Col md={6}>
                <p>Your purchase with PartsMonkey has been approved.</p>
                <p>Your order invoice number is: #{orderNumber}. <a href={`/order/${orderNumber}/invoice`} target="_blank">Click here to view your invoice</a>.</p>
                <p>If you have any questions about your invoice please call <a href="tel:18552237278">1-855-223-7278</a> or email <a href="mailto:info@partsmonkey.com">info@partsmonkey.com</a></p>
                <Link to="/" className="_button _grey">Back To Homepage</Link>
            </Col>
        </Row>
    </Grid>
);

export default MyPaymentCallout;
