import React from 'react';
import Helmet from './Helmet';

class About extends React.Component {
    render(){
        return(
            <div>
                <Helmet
                    title="Privacy Policy"
                />
                <div className="heading">
                    <p>Privacy Policy</p>
                    <h1>Canada's most comprehensive online aftermarket automotive part retailer</h1>
                </div>
                <div className="body">
                    <p>
                        PartsMonkey knows that you care how information about you is used and shared, and we appreciate your trust that we will do so carefully and sensibly. This notice describes our privacy policy. By visiting and/or using the PartsMonkey website, you are accepting the practices described in this Privacy Notice.
                    </p>
                    <p>
                        The information we learn from customers helps us personalize and continually improve your use of our website. Here are the types of information we gather.
                    </p>
                    <p><b>What Personal Information About Customers Does PartsMonkey Gather?</b></p>
                    <ul>
                        <li>Information You Give Us. We receive and store any information you enter on our website or give us in any other way. Scroll to the bottom of this page to see examples of what we collect. You can choose not to provide certain information, but then you might not be able to take advantage of many of our features. We use the information that you provide for such purposes as responding to your search requests and communicating with you.</li>
                        <li>Automatic Information. We receive and store certain types of information whenever you interact with us. For example, like many websites, we use "cookies," and we obtain certain types of information when your Web browser accesses PartsMonkey. Scroll to the bottom of this page to see examples of the information we receive. Although we will not be able to provide you with a personalized experience at PartsMonkey if we cannot recognize you, we want you to be aware that these tools exist.</li>
                        <li>E-mail Communications. To help us make e-mails more useful and interesting, we often receive a confirmation when you open e-mail from PartsMonkey if your computer supports such capabilities. We also compare our customer list to lists received from other companies, in an effort to avoid sending unnecessary messages to our customers.</li>
                        <li>Information from Other Sources. For reasons such as improving personalization of our service (for example, providing better product recommendations or special offers that we think will interest you), we might receive information about you from other sources and add it to our account information.</li>
                    </ul>
                    <p><b>What About Cookies?</b></p>
                    <ul>
                        <li>Cookies are alphanumeric identifiers that we transfer to your computer's hard drive through your Web browser to enable our systems to recognize your browser and to provide more efficient service of our website.</li>
                        <li>The "help" portion of the toolbar on most browsers will tell you how to prevent your browser from accepting new cookies, how to have the browser notify you when you receive a new cookie, or how to disable cookies altogether. However, cookies allow you to take full advantage of some of PartsMonkey’s most advanced features, and we recommend that you leave them turned on.</li>
                    </ul>
                    <p><b>Does PartsMonkey Share the Information It Receives?</b></p>
                    <p>Personal Information about our customers is an important part of our business, and we are not in the business of selling it to others. We share customer information only or providing as described in instances below.</p>
                    <ul>
                        <li>Agents. We employ other companies and individuals to perform functions on our behalf. Examples include fulfilling orders, delivering packages, sending postal mail and e-mail, removing repetitive information from customer lists, analyzing data, providing marketing assistance, processing credit card payments, and providing customer service. They have access to personal information needed to perform their functions, but may not use it for other purposes.</li>
                        <li>Protection of PartsMonkey and Others. We only release personal information when we believe release is appropriate to comply with the law; enforce or apply our Conditions of Use and other agreements; or protect the rights, property, or safety of PartsMonkey, our users, or others. This includes exchanging information with other companies, governmental and quasi-governmental institutions and crime prevention organizations for fraud investigation, detection, prevention and/or protection purposes.</li>
                    </ul>
                    <p><b>How Secure Is Information About Me?</b></p>
                    <ul>
                        <li>We work to protect the security of your information during transmission by using 128 bit Secure Sockets Layer (SSL) software, which encrypts information you input.</li>
                        <li>We reveal only the last five digits of your credit card numbers when confirming an order. Of course, we transmit the entire credit card number to the appropriate credit card company during order processing.</li>
                        <li>t is important for you to protect against unauthorized access to your password and to your computer. Be sure to sign off when finished using a shared computer.</li>
                    </ul>
                    <p><b>Conditions of Use, Notices, and Revisions</b></p>
                    <p>If you choose to visit and/or use the PartsMonkey website, your visit/use and any dispute over privacy is subject to this Notice and our Conditions of Use, including limitations on damages, arbitration of disputes, and application of the law of the province of Ontario. If you have any concern about privacy at PartsMonkey please send us a thorough description to <a href="mailto: info@partsmonkey.com">info@partsmonkey.com</a>, and we will try to resolve it. Our business changes constantly. This Notice and the Conditions of Use will change also, and use of information that we gather now is subject to the Privacy Notice in effect at the time of use. We may e-mail periodic reminders of our notices and conditions, unless you have instructed us not to, but you should check our Website frequently to see recent changes.</p>
                    <p><b>Automatic Information</b></p>
                    <p>Examples of the information we collect and analyze include the Internet protocol (IP) address used to connect your computer to the Internet; login; e-mail address; password; computer and connection information such as browser type and version, operating system, and platform; purchase history; the full Uniform Resource Locators (URL) clickstream to, through, and from our website, including date and time; cookie number; products you viewed or searched for; and phone number used to call our toll free number.</p>
                </div>
            </div>
        );

    }
}

export default About;
