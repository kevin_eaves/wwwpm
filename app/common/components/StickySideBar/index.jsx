import React from 'react';
import style from './style.scss';
import StickyDiv from 'react-stickynode';

const StickySideBar = ({ children, bottomBoundry, customClass }) => (
    <StickyDiv className={`${style[customClass]}`} top={30} enabled={true} bottomBoundary={'#' + bottomBoundry}>
        {children}
    </StickyDiv>
);

export default StickySideBar;
