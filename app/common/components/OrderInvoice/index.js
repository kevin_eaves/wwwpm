import React from 'react';
import { Col, Row } from 'react-bootstrap';
import Helmet from '../Helmet';
import Loader from '../Loader';
import style from './style.scss';

export default ({
    invoice,
    isFetching
}) => (
    <div className={style.container}>
        <Helmet
            title="Order Invoice"
        />
        <div className="heading">
            <p>Order Invoice</p>
            {
                invoice.orderId
                    && <h1>#{invoice.orderId}</h1>
            }
        </div>
        {
            isFetching
                ?   <div className={`_centerText ${style.loading}`}>
                        <Loader /><br />
                        <span className="_subHeading">Loading&hellip;</span>
                    </div>

                :   <Row>
                        <Col className={style.intro} md={6}>
                            <p>
                                This receipt confirms payment for your purchase from PartsMonkey for the items listed below. This order will appear on your credit card statement as <b>PartsMonkey</b>.
                            </p>
                            {
                                invoice.hasSpecialOrderItems &&
                                <p>
                                    Your order contains parts which are <b>approximately 2 weeks away</b> from being shipped. You will receive your <b>tracking number</b> when all parts are ready to ship.
                                </p>
                            }
                            <p>
                                In appreciation of your business and as a thanks from all of us at PartsMonkey, please enter <b>promotional code "RECU5"</b> and receive <b>5% off</b> your next order. We cannot apply this promotional code to orders already processed.
                            </p>
                            <p>
                                To contact us, please send an email to <a className="_textLink" href="mailto:info@partsmonkey.com" target="_blank">info@partsmonkey.com</a> or call <a className="_textLink" href="tel:1-855-223-7278" value="+18552237278" target="_blank">1-855-223-7278</a>.
                            </p>
                            <p>
                                Thank you for using PartsMonkey.
                            </p>
                        </Col>

                        <Col md={6}>
                            <h2>
                                Payment Receipt
                            </h2>
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tbody>
                                    <tr>
                                        <td style={{ width: '50%' }}>
                                            Transaction Date/Time:
                                        </td>
                                        <td>
                                            {invoice.date}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            P.O. Number:
                                        </td>
                                        <td>
                                            {invoice.purchaseOrderNumber}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Order Number:
                                        </td>
                                        <td>
                                            {invoice.orderId}-{invoice.source}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Approval Code:
                                        </td>
                                        <td>
                                            006611
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Amount:
                                        </td>
                                        <td>
                                            ${invoice.charges.total} (CAD)
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Transaction Type:
                                        </td>
                                        <td>
                                            Purchase
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Source:
                                        </td>
                                        <td>
                                            <a className="_textLink" href="http://www.partsmonkey.com" target="_blank">www.partsmonkey.com</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <h2>
                                Billing Information
                            </h2>
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tbody>
                                    <tr>
                                        <td style={{ width: '50%' }}>
                                            First and Last Name:
                                        </td>
                                        <td>
                                            {invoice.billing.address.FirstName} {invoice.billing.address.LastName}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Company:
                                        </td>
                                        <td>
                                            {invoice.billing.address.Company}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Address:
                                        </td>
                                        <td>
                                            {invoice.billing.address.Address1},<br />
                                            {
                                                invoice.billing.address.Address2.length > 0 &&
                                                <span>{invoice.billing.address.Address2},<br/></span>
                                            }
                                            {invoice.billing.address.City} {invoice.billing.address.ProvinceStateCode}&nbsp;&nbsp;{invoice.billing.address.PostalZIPCode}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Phone:
                                        </td>
                                        <td>
                                            <a className="_textLink" href="tel:invoice.billing.address.Phone">{invoice.billing.address.Phone}</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Email:
                                        </td>
                                        <td>
                                            <a className="_textLink" href="mailto:{invoice.billing.address.Email}">{invoice.billing.address.Email}</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <h2>
                                Shipping Information
                            </h2>
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tbody>
                                    {
                                        invoice.shipping.address.Address1 != invoice.billing.address.Address1 &&
                                            <tr>
                                                <td style={{ width: '50%' }}>
                                                    Name:
                                                </td>
                                                <td>
                                                    {invoice.shipping.address.FirstName} {invoice.shipping.address.LastName}
                                                </td>
                                            </tr>
                                    }
                                    {
                                        invoice.shipping.address.Address1 != invoice.billing.address.Address1 &&
                                            <tr>
                                                <td>
                                                    Company:
                                                </td>
                                                <td>
                                                    {invoice.shipping.address.Company}
                                                </td>
                                            </tr>
                                    }
                                    {
                                        invoice.shipping.address.Address1 != invoice.billing.address.Address1 &&
                                            <tr>
                                                <td>
                                                    Address:
                                                </td>
                                                <td>
                                                    {invoice.shipping.address.Address1},<br/>
                                                    {
                                                        invoice.shipping.address.Address2.length > 0 &&
                                                        <span>{invoice.shipping.address.Address2},<br/></span>
                                                    }
                                                {invoice.shipping.address.City} {invoice.shipping.address.ProvinceStateCode}&nbsp;&nbsp;{invoice.shipping.address.PostalZIPCode}
                                                </td>
                                            </tr>
                                    }
                                    {
                                        invoice.shipping.address.Address1 != invoice.billing.address.Address1 &&
                                            <tr>
                                                <td>
                                                    Phone:
                                                </td>
                                                <td>
                                                    {invoice.shipping.address.Phone}
                                                </td>
                                            </tr>
                                    }
                                    {
                                        invoice.shipping.address.Address1 != invoice.billing.address.Address1 &&
                                            <tr>
                                                <td>
                                                    Email:
                                                </td>
                                                <td>
                                                    {invoice.shipping.address.Email}
                                                </td>
                                            </tr>
                                    }
                                    {
                                        invoice.shipping.address.Address1 == invoice.billing.address.Address1 &&
                                            <tr>
                                                <td style={{ width: '50%' }}>
                                                    Ship To:
                                                </td>
                                                <td>
                                                    Billing Address
                                                </td>
                                            </tr>
                                    }
                                    <tr>
                                        <td>
                                            Shipping Option:
                                        </td>
                                        <td>
                                            {invoice.shipping.type} {invoice.shipping.insuranceSelected && '(shipping insurance selected)'}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Pickup Date:
                                        </td>
                                        <td>
                                            {invoice.shipping.pickup}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Estimated arrival date:
                                        </td>
                                        <td>
                                            {invoice.shipping.estimatedDelivery} *
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <h2>
                                Payment Information
                            </h2>
                            {
                                invoice.payment.creditCard &&
                                    <table border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <td style={{ width: '50%' }}>
                                                    Payment Type
                                                </td>
                                                <td>
                                                    {
                                                        invoice.payment.creditCard
                                                            ?   "Credit Card"
                                                            :   "PayPal"
                                                    }
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Name on Card/Account:
                                                </td>
                                                <td>
                                                    {invoice.payment.creditCard.NameOnCard}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Credit card number:
                                                </td>
                                                <td>
                                                    {invoice.payment.creditCard.CardNumber}
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                            }

                            {
                                invoice.payment.payPal &&
                                    <table border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <td style={{ width: '50%' }}>
                                                    Payment Type:
                                                </td>
                                                <td>
                                                    PayPal
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                            }

                            <h2>
                                Order Items
                            </h2>
                            <table border="0" cellSpacing="0" cellpadding="0" style={{ width: '100%' }}>
                                <tbody>
                                    <tr>
                                        <td colSpan="2">
                                            Description
                                        </td>
                                        <td>
                                            Price
                                        </td>
                                        <td>
                                            QTY
                                        </td>
                                        <td>
                                            Subtotal
                                        </td>
                                    </tr>


                                    {
                                        invoice.cart.map((item, i) => (
                                            <tr key={i}>
                                                <td>
                                                    {item.ProductInfo.OriginalPartNumber}
                                                </td>
                                                <td>
                                                    {item.ProductInfo.PartLabel} {!item.ProductInfo.Inventory.isRefundable && '(nr)'}<br />
                                                    {item.Vehicle.Year} {item.Vehicle.Make} {item.Vehicle.Model} {item.Vehicle.Engine}
                                                </td>
                                                <td className={style.alignRight}>
                                                    ${Number(item.ProductInfo.Inventory.Cost).toLocaleString('en', {
                                                        style: 'decimal',
                                                        minimumFractionDigits: 2
                                                    })}
                                                </td>
                                                <td className={style.alignRight}>
                                                    {item.Quantity}
                                                </td>
                                                <td className={style.alignRight}>
                                                    ${Number(item.SubTotal).toLocaleString('en', {
                                                        style: 'decimal',
                                                        minimumFractionDigits: 2
                                                    })}
                                                </td>
                                            </tr>
                                        ))
                                    }

                                    <tr className={style.alignRight}>
                                        <td colSpan="4">
                                            Subtotal:
                                        </td>
                                        <td>
                                            ${Number(invoice.charges.subTotal).toLocaleString('en', {
                                                style: 'decimal',
                                                minimumFractionDigits: 2
                                            })}
                                        </td>
                                    </tr>
                                    {
                                        invoice.charges.discount > 0 &&
                                        <tr className={style.alignRight}>
                                            <td colSpan="4">
                                                Promo. Discount
                                            </td>
                                            <td>
                                                -${Number(invoice.charges.discount).toLocaleString('en', {
                                                    style: 'decimal',
                                                    minimumFractionDigits: 2
                                                })}
                                            </td>
                                        </tr>
                                    }
                                    {
                                        invoice.charges.ehc > 0 &&
                                        <tr className={style.alignRight}>
                                            <td colSpan="4">
                                                EHC:
                                            </td>
                                            <td>
                                                ${Number(invoice.charges.ehc).toLocaleString('en', {
                                                    style: 'decimal',
                                                    minimumFractionDigits: 2
                                                })}
                                            </td>
                                        </tr>
                                    }
                                    {
                                        invoice.charges.freight > 0 &&
                                        <tr className={style.alignRight}>
                                            <td colSpan="4">
                                                Freight:
                                            </td>
                                            <td>
                                                ${Number(invoice.charges.freight).toLocaleString('en', {
                                                    style: 'decimal',
                                                    minimumFractionDigits: 2
                                                })}
                                            </td>
                                        </tr>
                                    }
                                    {
                                        invoice.charges.core > 0 &&
                                        <tr className={style.alignRight}>
                                            <td colSpan="4">
                                                Core Cost:
                                            </td>
                                            <td>
                                                ${Number(invoice.charges.core).toLocaleString('en', {
                                                    style: 'decimal',
                                                    minimumFractionDigits: 2
                                                })}
                                            </td>
                                        </tr>
                                    }
                                    {
                                        invoice.charges.shippingInsurance > 0 &&
                                        <tr className={style.alignRight}>
                                            <td colSpan="4">
                                                Shipping Insurance:
                                            </td>
                                            <td>
                                                ${Number(invoice.charges.shippingInsurance).toLocaleString('en', {
                                                    style: 'decimal',
                                                    minimumFractionDigits: 2
                                                })}
                                            </td>
                                        </tr>
                                    }
                                    <tr className={style.alignRight}>
                                        <td colSpan="4">
                                            GST:
                                        </td>
                                        <td>
                                            ${Number(invoice.charges.taxes).toLocaleString('en', {
                                                style: 'decimal',
                                                minimumFractionDigits: 2
                                            })}
                                        </td>
                                    </tr>
                                    <tr className={style.alignRight}>
                                        <td colSpan="4">
                                            Total:
                                        </td>
                                        <td>
                                            ${Number(invoice.charges.total).toLocaleString('en', {
                                                style: 'decimal',
                                                minimumFractionDigits: 2
                                            })}&nbsp;CAD
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <p className={style.alignRight}>
                                GST/HST # 83063 1289 RT0001.
                            </p>
                            <p>
                                *PartsMonkey does not guarantee the arrival date.
                            </p>
                            <p>
                                Parts marked with <b>(nr)</b> are not returnable as they were part of a special order.
                            </p>
                            <a
                                className="_textLink"
                                href="/shipping-and-return-warranty"
                            >
                                Click here to read our policy on shipping, returns, and warranty
                            </a>.
                        </Col>
                    </Row>
        }
    </div>
);
