import React from 'react';
import { Link } from 'react-router';
import Helmet from './Helmet';

class About extends React.Component {
    render(){
        return(
            <div>
                <Helmet
                    title="About"
                />
                <div className="heading">
                    <p>About PartsMonkey</p>
                    <h1>Canada's most comprehensive online aftermarket automotive part retailer</h1>
                </div>
                <div className="body">
                    <p>
                        We at <a className="_textLink" href="http://PartsMonkey.com" target="_blank">www.PartsMonkey.com</a> are proud Canadians and are proud to serve our fellow Canadians. Located in a major metropolitan city in Ontario, with over 60,000 unique SKUs and $40,000,000 worth of inventory available immediately, getting what you want has never been easier. We offer a full range of automotive products from chassis components to auto body parts and everything in between. We carry the premium brands of the automotive aftermarket industry including Federal Mogal, Spectra Premium, Standard Motor Products, Monroe, Bosch and Magnaflow. We also offer a wide arrangement of value brands at exceptional value. If there is a product you are looking for and we are currently out of stock we will special order the product from the manufacturer.
                    </p>
                    <p>
                        PartsMonkey is committed to the environment as well. To provide fast and efficient delivery of products while reducing our carbon footprint, we strive to use the most efficient and environmentally shipping and delivery practices currently being offered in the industry.
                    </p>
                    <p>
                        Our goal is to provide you with any and all automotive products that may be required. Our exceptional relationship with our suppliers allows us to source any parts you may require within a short time frame.
                    </p>
                    <p>
                        Given our easy and convenient quote option, we make it possible to locate those hard to find products. With this feature, combined with one of the simplest shipping and returns process, we can offer you the most hassle-free shopping experiences on the web.
                    </p>
                    <p>
                        With PartsMonkey there are no hidden costs of duty, brokerage or special handling costs that would be present from an American supplier. Our costs are in Canadian dollars and we will never have COD deliveries to our customers.
                    </p>

                    <p><em>We hope you enjoy your experience with us and thank you for shopping at PartsMonkey.</em></p>
                    <Link to="/" className="_button _grey _dark _mediumPadding">Find parts now</Link>
                </div>
            </div>
        );

    }
}

export default About;
