import React from 'react';
import Helmet from 'react-helmet';


const AppHelmet = ({
    title       = '',
    description = 'PartsMonkey is Canada’s premier on-line aftermarket automotive parts retailer offering the PowerHouse Brands of the industry.',
    type        = 'website',
    url         = 'http://partsmonkey.com',
    image       = '/images/logo.png',
    siteName    = 'PartsMonkey'
}) => (
    <Helmet
        title={title}
        defaultTitle="PartsMonkey"
        titleTemplate="%s | PartsMonkey"
        meta={[
            {"name": "description", "content": description},
            {property: 'og:title', content: title ? `${title} | PartsMonkey` : 'PartsMonkey'},
            {property: 'og:type', content: type},
            {property: 'og:url', content: url},
            {property: 'og:image', content: image},
            {property: 'og:site_name', content: siteName},
            {property: 'og:type', content: type},
            {property: 'og:description', content: description},
            {property: 'twitter:site', content: siteName},
            {property: 'twitter:title', content: title ? `${title} | PartsMonkey` : 'PartsMonkey'},
            {property: 'twitter:description', content: description},
            {property: 'twitter:image', content: image}
        ]}
    />
)

export default AppHelmet;
