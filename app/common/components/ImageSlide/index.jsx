import React from 'react';
import Slider from 'react-slick';
import style from './style.scss';

const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: false
};

const SlickSlider = ({ images = ['/images/logo.png'], title }) => (
    <div className={style.container}>
        <Slider {...settings}>
            {images.map((image, i) => <div key={`part_image-${title}-${i}`}><img src={image} alt={`${title} image`}/></div>)}
        </Slider>
    </div>
);

export default SlickSlider;
