import React from 'react';
import LinkedImage from '../LinkedImage';
import QuantityPicker from '../../containers/QuantityPickerContainer';
import style from './style.scss';

const OrderSummaryItem = ({
    id,
    image,
    title,
    partNumber,
    manufacturer,
    coreCost,
    freightCost,
    ehc,
    price,
    quantity,
    total,
    SellingMultiple = 1,
    url = '',
    vehicle = {}
}) => (
    <div className={style.container}>
        <LinkedImage image={image} alt={title} slug={url} />
        <div className={style.content}>
            <p>
                <strong>{title}</strong>
                <span>{manufacturer}</span>
                <span><strong>Part Number</strong>: {partNumber}</span>
                {
                    coreCost > 0 &&
                    <span className={style.subNote}>
                        <strong>Core</strong>: ${Number(coreCost).toLocaleString('en', {
                            style: 'decimal',
                            minimumFractionDigits: 2
                        })}&nbsp;CAD
                    </span>
                }
                {
                    ehc > 0 &&
                    <span className={style.subNote}>
                        <strong>EHC</strong>: ${Number(ehc).toLocaleString('en', {
                            style: 'decimal',
                            minimumFractionDigits: 2
                        })}&nbsp;CAD
                    </span>
                }
                {
                    freightCost > 0 &&
                    <span className={style.subNote}>
                        <strong>Freight</strong>: ${Number(freightCost).toLocaleString('en', {
                            style: 'decimal',
                            minimumFractionDigits: 2
                        })}&nbsp;CAD
                    </span>
                }
                {
                    (typeof vehicle.Year !== 'undefined' && vehicle.Year > 0) &&
                        <em className={style.searchTerm}>Searched: {vehicle.Year} / {vehicle.Make} / {vehicle.Model} / {vehicle.Engine}</em>
                }
            </p>
            <span className={style.quantity}>Qty: {quantity}</span>
            <span className={style.price}>${Number(total).toLocaleString('en', {style: 'decimal', minimumFractionDigits: 2})}&nbsp;CAD</span>
        </div>
    </div>
);

export default OrderSummaryItem;
