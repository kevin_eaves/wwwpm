import React from 'react';
import { Field } from 'redux-form';
import style from './style.scss';
import BasicInput from '../_inputs/BasicInput';
import PromoCode from '../../../containers/PromoCodeContainer';
import { Clearfix, Col } from 'react-bootstrap';

const PurchaseOrderAndPromoCode = ({ purchaseOrderNumber = {meta: {}} }) => (
    <Clearfix className={style.container}>
        <Col md={12}>
            <h1 className="_mediumTitle _noTransform _withBackground">Purchase Order & Promo Code</h1>
        </Col>
        <Col md={6}>
            <Field
                name="purchaseOrderNumber"
                component={BasicInput}
                props={{
                    disabled: false,
                    placeholder: 'PO Number'
                }}
            />
            <p className={`_inputNote ${style.poNote}`}><span className="fa fa-info-circle" aria-hidden="true"></span> Optionally, enter a purchase order number&nbsp;above.</p>
        </Col>
        <Col md={6}>
            <PromoCode />
        </Col>
    </Clearfix>
);

export default PurchaseOrderAndPromoCode;
