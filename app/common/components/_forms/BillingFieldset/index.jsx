import React from 'react';
import { Col, Clearfix } from 'react-bootstrap';
import { Field } from 'redux-form';
import { scroller } from 'react-scroll';
import { withState } from 'recompose';
import BasicInput from '../_inputs/BasicInput';
import Select from '../_inputs/Select';
import style from './style.scss';

const enhance = withState('disabledFieldset', 'toggleDisabledFieldset', true);
const BillingFieldset = enhance(({
    disabledFieldset,
    toggleDisabledFieldset,
    countriesOptions,
    regionOptions,
    scrollToErrors
}) => (
    <Clearfix className={`${style.container} ${disabledFieldset && style.disabled}`}>
        <Col md={12} className={style.titleHeading}>
            <h1 className="_mediumTitle _noTransform _withBackground">Billing Address</h1>
            <div className={`${style.formToggler} fancyCheckbox`}>
                <Field
                    name="sameAsShippingAddress"
                    component={(props) => {
                        return (
                            <input
                                type="checkbox"
                                id="sameAsShippingAddress"
                                onChange={(e) => {
                                     e.target.checked ? toggleDisabledFieldset(true) : toggleDisabledFieldset(false);
                                     props.input.onChange(e.target.checked ? true : false)
                                }}
                                checked={disabledFieldset}
                                value="1"
                            />
                        )
                    }}
                />
                <label htmlFor="sameAsShippingAddress">
                    <span></span>
                    Same as shipping address
                </label>
            </div>
        </Col>
        <fieldset>
            <Col md={6}>
                <Field
                    name="firstName_billing"
                    component={BasicInput}
                    props={{
                        disabled: false,
                        placeholder: 'First Name'
                    }}
                />
            </Col>
            <Col md={6}>
                <Field
                    name="lastName_billing"
                    component={BasicInput}
                    props={{
                        disabled: false,
                        placeholder: 'Last Name'
                    }}
                />
            </Col>
            <Col md={6}>
                <Field
                    name="company_billing"
                    component={BasicInput}
                    props={{
                        disabled: false,
                        placeholder: 'Company'
                    }}
                />
            </Col>
            <Col md={6}>
                <Field
                    name="phone_billing"
                    component={BasicInput}
                    props={{
                        disabled: false,
                        placeholder: 'Phone Number'
                    }}
                />
            </Col>
            <Col md={12}>
                <Field
                    name="email_billing"
                    component={BasicInput}
                    props={{
                        disabled: false,
                        placeholder: 'Email Address'
                    }}
                />
            </Col>
            <Col md={12}>
                <Field
                    name="address_billing"
                    component={BasicInput}
                    props={{
                        disabled: false,
                        placeholder: 'Address Line 1'
                    }}
                />
            </Col>
            <Col md={12}>
                <Field
                    name="second_address_billing"
                    component={BasicInput}
                    props={{
                        disabled: false,
                        placeholder: 'Address Line 2'
                    }}
                />
            </Col>
            <Clearfix>
                <Col md={6}>
                    <Field
                        name="city_billing"
                        component={BasicInput}
                        props={{
                            disabled: false,
                            placeholder: 'City'
                        }}
                    />
                </Col>
                <Col md={6}>
                    <Field
                        name="country_billing"
                        component={Select}
                        props={{
                            inputProps: {autoComplete: 'off', autoCorrect: 'off', spellCheck: 'off' },
                            theme: "square",
                            placeholder: "Country",
                            options: countriesOptions,
                            hideLabel: true,
                            isSearchable: true,
                            isClearable: false,
                            label: "Country"
                        }}
                    />
                </Col>
            </Clearfix>
            <Col md={6}>
                <Field
                    name="province_billing"
                    component={Select}
                    props={{
                        inputProps: {autoComplete: 'off', autoCorrect: 'off', spellCheck: 'off' },
                        theme: "square",
                        placeholder: "Province",
                        options: regionOptions,
                        hideLabel: true,
                        isSearchable: true,
                        isClearable: false,
                        label: "Province / State"
                    }}
                />
            </Col>
            <Col md={6}>
                <Field
                    name="postalCode_billing"
                    component={BasicInput}
                    props={{
                        disabled: false,
                        placeholder: 'Postal / Zip Code'
                    }}
                />
            </Col>
        </fieldset>
    </Clearfix>
));

export default BillingFieldset;
