import React from 'react';
import { Grid, Row, Col, Clearfix } from 'react-bootstrap';
import { Field } from 'redux-form';
import DeliveryOptions from '../DeliveryOptions';
import style from './style.scss';

const CheckoutSummary = ({ parts, formDetails, shippingType, fees, billingSameAsShipping, error }) => (
    <fieldset className={style.container}>
        {
            error && error.length > 0 &&
                <ul className="_alertError" id="shippingAndDelivery-errors">
                    <p>{error}</p>
                </ul>
        }
        <Row>
            <Col md={6}>
                <div className={style.title}>
                    <p>Item Overview</p>
                </div>
                <ul>
                    {parts.map((part, i) => (
                        <li key={i}>
                            {
                                (part.vehicle && part.vehicle.Year) &&
                                    <em>Searched: {part.vehicle.Year} / {part.vehicle.Make} / {part.vehicle.Model} / {part.vehicle.Engine}</em>
                            }
                            <strong>{part.quantity} x {part.ProductInfo.PartLabel} <em>({part.ProductInfo.PartNumber})</em></strong>
                            ${Number(part.ProductInfo.Inventory.Cost * part.quantity).toLocaleString('en', {
                                style: 'decimal',
                                minimumFractionDigits: 2
                            })}&nbsp;CAD
                        </li>
                    ))}
                </ul>
            </Col>
            <Col md={6}>
                <div className={style.title}>
                    <p>Shipping to</p>
                </div>
                <p>
                    {formDetails && formDetails.values && `${formDetails.values.firstName_shipping} ${formDetails.values.lastName_shipping}`}
                    {formDetails && formDetails.values && formDetails.values.company_shipping && <span>{formDetails.values.company_shipping}</span>}
                    {formDetails && formDetails.values && formDetails.values.address_shipping && <span>{formDetails.values.address_shipping}</span>}
                    {formDetails && formDetails.values && formDetails.values.city_shipping && <span>{`${formDetails.values.city_shipping}, ${formDetails.values.province_shipping} ${formDetails.values.postalCode_shipping}`}</span>}
                    {shippingType && <span className={style.footer}>{shippingType}</span>}
                </p>
            </Col>
        </Row>
        <Row>
            <Col md={6}>
                <div className={style.title}>
                    <p>Cost Summary</p>
                </div>
                <p>
                    <span>
                        Subtotal: ${Number(fees.subTotal).toLocaleString('en', {
                            style: 'decimal',
                            minimumFractionDigits: 2
                        })}&nbsp;CAD
                    </span>
                    <span>
                        Shipping: ${Number(fees.shipping).toLocaleString('en', {
                            style: 'decimal',
                            minimumFractionDigits: 2
                        })}&nbsp;CAD
                    </span>
                    {
                        fees.insuranceCost > 0 &&
                            <span>
                                Shipping Insurance: ${Number(fees.insuranceCost).toLocaleString('en', {
                                    style: 'decimal',
                                    minimumFractionDigits: 2
                                })}&nbsp;CAD
                            </span>
                    }
                    {
                        fees.ehc > 0 &&
                            <span>
                                EHC: ${Number(fees.ehc).toLocaleString('en', {
                                    style: 'decimal',
                                    minimumFractionDigits: 2
                                })}&nbsp;CAD
                            </span>
                    }
                    {
                        fees.freight > 0 &&
                            <span>
                                Freight: ${Number(fees.freight).toLocaleString('en', {
                                    style: 'decimal',
                                    minimumFractionDigits: 2
                                })}&nbsp;CAD
                            </span>
                    }
                    {
                        fees.discount > 0 &&
                            <span>
                                Promo. Discount: -${Number(fees.discount).toLocaleString('en', {
                                    style: 'decimal',
                                    minimumFractionDigits: 2
                                })}&nbsp;CAD
                            </span>
                        }
                    <span>
                        Taxes ({fees.taxCode}:&nbsp;
                            {Number(fees.taxPercent).toLocaleString('en', {
                            style: 'decimal',
                            minimumFractionDigits: 0
                        })}%):&nbsp;

                        ${Number(fees.tax).toLocaleString('en', {
                            style: 'decimal',
                            minimumFractionDigits: 2
                        })}&nbsp;CAD
                    </span>
                    Total: <em>${Number(fees.total).toLocaleString('en', {
                        style: 'decimal',
                        minimumFractionDigits: 2
                    })}&nbsp;CAD</em>
                </p>
            </Col>
            <Col md={6}>
                <div className={style.title}>
                    <p>Billing to</p>
                </div>
                <p>
                    {
                        billingSameAsShipping
                            ? formDetails && formDetails.values && `${formDetails.values.firstName_shipping} ${formDetails.values.lastName_shipping}`
                            : formDetails && formDetails.values && formDetails.values.firstName_billing
                                ? `${formDetails.values.firstName_billing} ${formDetails.values.lastName_billing}`
                                : `${formDetails.values.firstName_shipping} ${formDetails.values.lastName_shipping}`
                    }
                    {
                        billingSameAsShipping
                            ? formDetails && formDetails.values && formDetails.values.company_shipping && <span>{formDetails.values.company_shipping}</span>
                            : formDetails && formDetails.values && formDetails.values.company_billing
                                ? <span>{formDetails.values.company_billing}</span>
                                : <span>{formDetails.values.company_shipping}</span>
                    }
                    {
                        billingSameAsShipping
                            ? formDetails && formDetails.values && formDetails.values.address_shipping && <span>{formDetails.values.address_shipping}</span>
                            : formDetails && formDetails.values && formDetails.values.address_billing
                                ? <span>{formDetails.values.address_billing}</span>
                                : <span>{formDetails.values.address_shipping}</span>
                    }
                    {
                        billingSameAsShipping
                            ? formDetails && formDetails.values && formDetails.values.city_shipping && <span>{`${formDetails.values.city_shipping}, ${formDetails.values.province_shipping} ${formDetails.values.postalCode_shipping}`}</span>
                            : formDetails && formDetails.values && formDetails.values.city_billing
                                ? <span>{`${formDetails.values.city_billing}, ${formDetails.values.province_billing} ${formDetails.values.postalCode_billing}`}</span>
                                : <span>{`${formDetails.values.city_shipping}, ${formDetails.values.province_shipping} ${formDetails.values.postalCode_shipping}`}</span>
                    }
                    {formDetails && formDetails.values && formDetails.values.cardNumber && <span className={style.footer}>{formDetails.values.cardNumber}</span>}
                </p>
            </Col>
        </Row>
    </fieldset>
);

export default CheckoutSummary;
