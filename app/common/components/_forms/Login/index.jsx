import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { Row, Col } from 'react-bootstrap';
import cookie from 'react-cookie';
import style from './style.scss';

const Login = ({ email, password, cartId, handleSubmit, pristine, submitting, initialValues }) => (
    <form onSubmit={handleSubmit} {...initialValues}>
        <div className={style.title}>
            <p>
                <em>Login To PartsMonkey</em>
                <span>Login to receive the best PartsMonkey experience possible</span>
            </p>
        </div>
        <Row className="_noSpacing">
            <Col md={12}>
                <label className="_hideLabel">Name</label>
                <div>
                    <Field  {...email} name="email" component="input" type="email" placeholder="Email" />
                </div>
            </Col>
            <Col md={12}>
                <label className="_hideLabel">Email</label>
                <div>
                    <Field  {...password} name="password" component="input" type="password" placeholder="Password" />
                </div>
            </Col>
            <Field  {...cartId} name="cartId" component="input" type="hidden" />
        </Row>
        <div className={style.footer}>
            <button type="submit" className="_button _primary _mediumPadding" disabled={submitting || pristine}>Login</button>
        </div>
    </form>
);

const cardId = cookie.load('pm-cartID') ? cookie.load('pm-cartID') : '';

export default reduxForm({
    form: 'LoginForm',
    fields: [
        'email',
        'password',
        'cartId'
    ],
    initialValues: {
        cartId: cardId
    }
})(Login);
