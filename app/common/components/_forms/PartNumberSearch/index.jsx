import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { Row, Col } from 'react-bootstrap';
import cookie from 'react-cookie';
import style from './style.scss';

const PartNumberSearch = ({ active, partNumber, submitting, pristine }) => (
    <form
        className={style.container}
        method="GET"
        action={`/search?partNumber=${partNumber}`}
    >
        <Field  {...partNumber} name="partNumber" component="input" type="text" placeholder="Part Number" />
        <button type="submit" className={`${style.submit} ${active && style.active}`} disabled={submitting || pristine}>
            <i className="fa fa-search" aria-hidden="true"></i>
        </button>
    </form>
);

export default reduxForm({
    form: 'PartNumberSearchForm',
    fields: [
        'partNumber'
    ]
})(PartNumberSearch);
