import React from 'react';
import { withState } from 'recompose';
import { Row, Col } from 'react-bootstrap';
import style from './style.scss';

const enhance = withState('value', 'setValue', '');
const PromoCode = enhance(({ value, setValue, fetchPromo, error, code, discount }) => (
    <div className={style.container}>
        <Row className="_noSpacing">
            <div className={style.inner}>
                <input
                    className={style.input}
                    name="promoCode"
                    type="text"
                    placeholder="Promo Code"
                    onChange={(e) => setValue(e.target.value)}
                    onKeyDown={(e) => e.keyCode === 13 ? fetchPromo(value) : ''}
                />
                <button
                    type="button"
                    className={`_button _blue _light _mediumPadding ${style.button}`}
                    disabled={!value.length}
                    onClick={() => fetchPromo(value)}
                >
                    Apply Code
                </button>
            </div>
        </Row>
        {
            error &&
            <div className="_alertError">
                <p>{error}</p>
            </div>
        }
        {
            discount > 0 &&
            <div className="_alertSuccess">
                <p><strong>Promo Code Applied</strong>"{code}" - {discount}% off order</p>
            </div>
        }
    </div>
));

export default PromoCode;
