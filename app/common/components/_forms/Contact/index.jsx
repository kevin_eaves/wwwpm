import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { Row, Col } from 'react-bootstrap';
import style from './style.scss';
import BasicInput from '../_inputs/BasicInput';
import validate from './validation';

const Contact = ({
        handleSubmit,
        name,
        email,
        phone,
        notes,
        confirmationEmail,
        initialValues,
        pristine,
        submitting,
        error
    }) => (

    <form
        onSubmit={handleSubmit}
        itemScope
        itemType="https://schema.org/ContactPage"
    >

        <div className={style.title}>
            <p>
                <em>Contact Us</em>
                <span className={style.caption} itemProp="mainContentOfPage">Have a question or comment? Send us a message</span>
                <span><strong>Address:</strong> 1275 Hubrey Road, Unit 3<br/>London, ON N6N1E2</span>
                <span><strong>Phone:</strong> <a href="tel:1-855-223-7278">1-855-223-7278</a></span>
            </p>
        </div>
        <Row className="_noSpacing">
            <Col md={6}>
                <label className="_hideLabel">First Name</label>
                <div>
                    <Field name="name" component={(props)=>{
                        return (
                            <BasicInput
                                input={props.input}
                                meta={props.meta}
                                disabled={false}
                                placeholder="Name"
                            />
                        )
                    }} type="text" />
                </div>
            </Col>
            <Col md={6}>
                <label className="_hideLabel">Email</label>
                <div>
                    <Field name="email" component={(props)=>{
                        return (
                            <BasicInput
                                input={props.input}
                                meta={props.meta}
                                disabled={false}
                                placeholder="Email"
                            />
                        )
                    }} type="email" />
                </div>
            </Col>
        </Row>
        <Row className="_noSpacing">
            <Col md={6}>
                <label className="_hideLabel">Confirm Email</label>
                <div>
                    <Field name="confirmationEmail" component={(props)=>{
                       return (
                            <BasicInput
                                input={props.input}
                                meta={props.meta}
                                disabled={false}
                                placeholder="Confirm Email"
                            />
                        )
                    }} type="email" />
                </div>
            </Col>
            <Col md={6}>
                <label className="_hideLabel">Phone Number</label>
                <div>
                    <Field name="phone" component={(props)=>{
                        return (
                            <BasicInput
                                input={props.input}
                                meta={props.meta}
                                disabled={false}
                                placeholder="Phone Number"
                            />
                        )
                    }} type="tel" />
                </div>
            </Col>
        </Row>
        <Row className="_noSpacing">
            <Col md={12}>
                <label className="_hideLabel">Notes</label>
                <div>
                    <Field {...notes} name="notes" component="textarea" placeholder="Notes" required />
                </div>
            </Col>
        </Row>
        <div className={style.footer}>
            <button type="submit" className="_button _primary _mediumPadding" disabled={submitting}>Send Message</button>
        </div>
    </form>
);

export default reduxForm({
    form: 'ContactForm',
    fields: [
        'name',
        'email',
        'phone',
        'notes',
        'confirmationEmail'
    ],
    validate
})(Contact);
