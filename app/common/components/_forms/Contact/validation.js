import React from 'react';

const validate = values => {
    const errors = {};

    if (!values.name) {
        errors.name = 'A Name is Required';
    }

    if (!values.email) {
        errors.email = 'A Email is Required';
    }

    if (!values.confirmationEmail) {
        errors.confirmationEmail = 'Confirmation Email is Required';
    }else if(values.confirmationEmail !== values.email){
        errors.confirmationEmail = 'Confirmation Email Is not The Same As Email'
    }

    if (!values.notes) {
        errors.notes = 'Notes is Required';
    }

    return errors;
};

export default validate;
