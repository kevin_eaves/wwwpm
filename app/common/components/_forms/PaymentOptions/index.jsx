import React from 'react';
import { Col, Clearfix } from 'react-bootstrap';
import { Field } from 'redux-form';
import BasicInput from '../_inputs/BasicInput';
import { scroller } from 'react-scroll';
import Select from '../_inputs/Select';
import DeliveryOptions from '../DeliveryOptions';
import style from './style.scss';
import moment from 'moment';

const validExpiryMonths = [...Array(12)].map((x, i) => {
    const month = moment().month(i);

    return {
        value: month
            .format('MM')
    }
});

const numExpiryYears = 8;
const validExpiryYears = [...Array(numExpiryYears)].map((x, i) => ({
    value: moment()
        .add(i, 'years')
        .format('YYYY')
}));

const PaymentMethods = ({ paymentType, scrollToErrors }) => (
    <Clearfix className={style.container}>
        <Col md={12}>
            <h1 className="_mediumTitle _noTransform _withBackground">Payment Options</h1>
        </Col>
        <Options />
        <CreditCardFields hidden={paymentType == 'paypal'} scrollToErrors={scrollToErrors} />
    </Clearfix>
);

const CreditCardFields = ({ hidden, scrollToErrors }) => (
    <Clearfix className={`${style.paymentFields} ${hidden && style.disable}`}>
        <Col md={12} className={style.title}>
            <p>Credit Card Details</p>
            <ul>
                <li><img src="../../images/visa.png" alt="Visa" /></li>
                <li><img src="../../images/visa-debit.png" alt="Visa Debit" /></li>
                <li><img src="../../images/mastercard.png" alt="Mastercard" /></li>
                <li><img src="../../images/mastercard-debit.png" alt="Mastercard Debit" /></li>
            </ul>
        </Col>
        <Clearfix>
            <Col md={6}>
                <Field
                    name="cardNumber"
                    component={BasicInput}
                    props={{
                        disabled: false,
                        placeholder: 'Card Number',
                        maxLength: 16
                    }}
                />
            </Col>
            <Col sm={2} className={style.inlineLabel}>
                <label>Expiry</label>
            </Col>
            <Col sm={2}>
                <label className="_hideLabel">Expiry Date (Month)</label>
                    <Field
                        name="month"
                        component={Select}
                        props={{
                            theme: "square",
                            placeholder: "Month",
                            options: validExpiryMonths,
                            hideLabel: true,
                            isSearchable: true,
                            isClearable: false,
                            label: "Month",
                            labelKey: "value",
                            centerOptionText: true
                        }}
                    />
            </Col>
            <Col sm={2}>
                <label className="_hideLabel">Expiry Date (year)</label>
                    <Field
                        name="year"
                        component={Select}
                        props={{
                            theme: "square",
                            placeholder: "Year",
                            options: validExpiryYears,
                            hideLabel: true,
                            isSearchable: true,
                            isClearable: false,
                            label: "Year",
                            labelKey: "value",
                            centerOptionText: true
                        }}
                    />
            </Col>
        </Clearfix>
        <Col sm={3} className={style.inlineLabel}>
            <label>Security Code (?)</label>
        </Col>
        <Col sm={3}>
            <Field
                name="cvv"
                component={BasicInput}
                props={{
                    disabled: false,
                    placeholder: 'CVV',
                    maxLength: 16
                }}
            />
        </Col>
    </Clearfix>
);

const Options = () => (
    <div>
        <Col md={6}>
            <Field className={style.hidden} name="paymentTypes" component="input" type="radio" value="credit" id="creditCheck" />
            <label htmlFor="creditCheck" className={style.labelButton}>
                <span className="fa fa-check" aria-hidden="true"></span> Pay With Credit Card
            </label>
        </Col>
        <Col md={6}>
            <Field className={style.hidden} name="paymentTypes" component="input" type="radio" value="paypal" id="paypalCheck" />
            <label htmlFor="paypalCheck" className={style.labelButton}>
                Pay With Paypal <img src="../../images/paypal-icon.svg" onerror="this.src='../../images/paypal-icon.png'" alt="PayPal Icon" />
            </label>
        </Col>
    </div>
);

export default PaymentMethods;
