import React from 'react';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import { scroller } from 'react-scroll';
import PaymentOptions from '../../../containers/PaymentOptionsContainer';
import ShippingFieldset from '../../../containers/ShippingFieldsetContainer';
import PurchaseOrderAndPromoCode from '../PurchaseOrderAndPromoCode';
import DeliveryOptions from '../../../containers/DeliveryOptionsContainer';
import BillingFieldset from '../../../containers/BillingFieldsetContainer';
import validate from './validation';
import style from './style.scss';

let ProcessCheckout = ({ handleSubmit, formDetails, onSubmit, errorMessage, setIsSubmitting, submitting, previousSubmitValue, scrollToErrors }) => (
    <form
        id="processFormError"
        className={style.container}
        onSubmit={() => {
            handleSubmit(values => {
                onSubmit(values);
            });
        }}
    >
        { shouldSetSubmitting(submitting, previousSubmitValue, setIsSubmitting) }
        { errorMessage && scrollToError(errorMessage, scrollToErrors) }
        <ShippingFieldset scrollToErrors={scrollToErrors} />
        <PurchaseOrderAndPromoCode />
        <DeliveryOptions />
        <BillingFieldset scrollToErrors={scrollToErrors} />
        <PaymentOptions scrollToErrors={scrollToErrors} />
    </form>
);

function scrollToError (errorMessage, scrollToErrors) {
    if (errorMessage, scrollToErrors) {
        scroller.scrollTo('headingError', {
            duration: 500,
            offset: -175,
            smooth: true
        })
    }

    return <div id="headingError" className="error _alertError _noBottomSpacing"><p>{errorMessage}</p></div>;
}

function shouldSetSubmitting (submitting, previousSubmitValue, callback) {
    if (submitting) {
        callback(true);
    } else if (!submitting && previousSubmitValue != submitting) {
        callback(false);
    }
}

// Hook up redux form to component
ProcessCheckout = reduxForm({
    validate,
    form: 'ProcessCheckoutForm'
})(ProcessCheckout)

ProcessCheckout = connect(
  state => ({
    initialValues: {
        deliveryInsurance: true,
        paymentTypes: 'credit'
    }
  })
)(ProcessCheckout)

export default ProcessCheckout
