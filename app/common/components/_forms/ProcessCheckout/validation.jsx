import React from 'react';
import moment from 'moment';
import { Field, reduxForm } from 'redux-form';

const currentDate = moment().format("YYYYMM");
const expirationDate = moment().add(8, "years").format("YYYYMM");

const validate = values => {
    const errors = {};
    // FRONT END SHIPPING AND DELIVERY VALIDATION
    if (!values.firstName_shipping) {
        errors.firstName_shipping = 'A First Name is Required';
    }
    if (!values.lastName_shipping) {
        errors.lastName_shipping = 'A Last Name is Required';
    }
    if (!values.phone_shipping) {
        errors.phone_shipping = 'A Phone Number is Required';
    }else if (values.phone_shipping.length > 10){
        errors.phone_shipping = 'Shipping Phone Number Should Be No More Than 10 Characters ex: 1234445555';
    }
    if (!values.address_shipping) {
        errors.address_shipping = 'An Address is Required';
    }
    if (!values.city_shipping) {
        errors.city_shipping = 'A City is Required';
    }
    if (!values.postalCode_shipping) {
        errors.postalCode_shipping = 'A Postal Code is Required';
    }
    if (!values.province_shipping) {
        errors.province_shipping = 'A Province is Required';
    }
    if (!values.country_shipping) {
        errors.country_shipping = 'A Country is Required';
    }
    if (!values.email_shipping) {
        errors.email_shipping = 'An Email is Required';
    } else if (!/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(values.email_shipping)){
        errors.email_shipping = 'A Valid Email is Required';
    }

    // FRONT END BILLING ADDRESS VALIDATION
    if(!values.sameAsShippingAddress){
        if (!values.firstName_billing) {
            errors.firstName_billing = 'A First Name is Required';
        }
        if (!values.lastName_billing) {
            errors.lastName_billing = 'A Last Name is Required';
        }
        if (!values.phone_billing) {
            errors.phone_billing = 'A Phone Number is Required';
        }
        else if (values.phone_billing.length > 10){
            errors.phone_billing = 'Billing Phone Number Should Be No More Than 10 Characters ex: 1234445555';
        }
        if (!values.address_billing) {
            errors.address_billing = 'An Address is Required';
        }
        if (!values.city_billing) {
            errors.city_billing = 'A City is Required';
        }
        if (!values.postalCode_billing) {
            errors.postalCode_billing = 'A Postal Code is Required';
        }
        if (!values.province_billing) {
            errors.province_billing = 'A Province is Required';
        }
        if (!values.country_billing) {
            errors.country_billing = 'A Country is Required';
        }
        if (!values.email_billing) {
            errors.email_billing = 'An Email is Required';
        } else if (!/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(values.email_billing)){
            errors.email_billing = 'A Valid Email is Required';
        }
    }

    // FRONT END PAYMENT VAILIDATION
    if(values.paymentTypes != 'paypal'){
        if (!values.cardNumber) {
            errors.cardNumber = 'A Card Number is Required';
        } else if (isNaN(Number(values.cardNumber))) {
            errors.cardNumber = 'Please Enter a Valid Credit Card Number';
        } else if (!values.cardNumber.length > 16) {
            errors.cardNumber = 'Credit Card Should Be No More Than 16 Characters';
        }
        if (!values.cvv) {
            errors.cvv = 'A CVV is Required';
        } else if (values.cvv.length != 3) {
            errors.cvv = 'A CVV Number Must Be Exactly 3 Number';
        } else if (isNaN(Number(values.cvv))) {
            errors.cvv = 'A CVV Must Only Contain Numbers';
        }
        if (!values.month) {
            errors.month = 'An Expiration Month is Required';
        }
        if (!values.year) {
            errors.year = 'A Expiration Year is Required';
        } else if (`${parseInt(`${values.year}${values.month}`)}` > expirationDate || `${parseInt(`${values.year}${values.month}`)}` < currentDate) {
            errors.year = 'Please Enter a Valid Expiration Date';
        }
    }

    // FRONT END DELIVERY VAILIDATION
    if (!values.deliveryType) {
        errors.deliveryType = 'A Delivery Type is Required';
    }

    return errors
};

export default validate;
