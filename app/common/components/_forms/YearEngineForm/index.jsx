import React from 'react';
import Select from '../_inputs/Select';
import style from './style.scss';

const VehicleTypeForm = ({ yearField, years, searchForPart, engines, setEngine, filters, setYear }) => (
    <div>
        <div className={style.title}>
            <p>
                <em>Select A Year and Engine</em>
                <span>Select your vehicle's year and engine before shopping for parts</span>
            </p>
        </div>
        <div className={style.select}>
            <Select
                onChange={setYear}
                placeholder="Year"
                options={years}
                multi={false}
                propValue={filters.year}
                hideLabel
                labelKey="value"
                valueKey="value"
                label="Year"
                isSearchable={true}
                isLoading={!years.length}
                theme="square"
                isClearable={false}
            />
        </div>
        <div className={style.select}>
            <Select
                onChange={setEngine}
                placeholder="Engine"
                options={engines}
                multi={false}
                propValue={filters.engine}
                hideLabel
                labelKey="label"
                valueKey="value"
                label="Engine"
                isSearchable={true}
                isLoading={filters.year && !engines.length}
                disabled={!filters.year}
                theme="square"
                isClearable={false}
            />
        </div>
        <button onClick={() => searchForPart(filters)} className="_button _primary _mediumPadding" disabled={!filters.engine ? true : false}>Search For Parts</button>
    </div>
);

export default VehicleTypeForm;
