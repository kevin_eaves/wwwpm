import validator from 'validator';

export default ({
    name = '',
    email = '',
    confirmationEmail = '',
    phone = '',
    notes = ''
} = {}) => {
    let errors = {};

    if (!name) {
        errors.name = 'Name is required';
    }

    if (!validator.isEmail(email)) {
        errors.email = 'Email is required';
    }

    if (!validator.isEmail(email) || email !== confirmationEmail) {
        errors.confirmationEmail = 'Confirm your email';
    }

    if (!phone) {
        errors.phone = 'Phone number is required';
    }

    return errors;
};
