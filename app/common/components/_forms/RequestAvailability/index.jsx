import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { Row, Col } from 'react-bootstrap';
import BasicInput from '../_inputs/BasicInput';
import style from './style.scss';
import validate from './validation';

const RequestAvailability = ({
    handleSubmit,
    pristine,
    partTitle,
    partNumber,
    manufacturer,
    submitting,
    error
} = {}) => (
    <form onSubmit={handleSubmit}>
        <div className={style.title}>
            <p>
                <em>Request Availability</em>
                <strong className="_mediumTitle">{partTitle}</strong>
                <span>Part Number: {partNumber}</span>
                <span>Manufacturer: {manufacturer}</span>
            </p>
        </div>

        {
            error &&
                <div className="_alertError">
                    <p>{error}</p>
                </div>
        }

        <Row className="_noSpacing">
            <Col md={6}>
                <label className="_hideLabel">First Name</label>
                <div>
                    <Field
                        name="name"
                        component={({ input, meta }) => <BasicInput
                            input={input}
                            meta={meta}
                            placeholder="Name"
                        />}
                    />
                </div>
            </Col>
            <Col md={6}>
                <label className="_hideLabel">Phone Number</label>
                <div>
                    <Field
                        name="phone"
                        component={({ input, meta }) => <BasicInput
                            input={input}
                            meta={meta}
                            placeholder="Phone number"
                            type="tel"
                        />}
                    />
                </div>
            </Col>
        </Row>
        <Row className="_noSpacing">
            <Col md={6}>
                <label className="_hideLabel">Email</label>
                <div>
                    <Field
                        name="email"
                        component={({ input, meta }) => <BasicInput
                            input={input}
                            meta={meta}
                            placeholder="Email"
                            type="email"
                        />}
                    />
                </div>
            </Col>
            <Col md={6}>
                <label className="_hideLabel">Confirm Email</label>
                <div>
                    <Field
                        name="confirmationEmail"
                        component={({ input, meta }) => <BasicInput
                            input={input}
                            meta={meta}
                            placeholder="Confirm email"
                            type="email"
                        />}
                    />
                </div>
            </Col>
        </Row>
        <Row className="_noSpacing">
            <Col md={12}>
                <label className="_hideLabel">Notes</label>
                <div>
                    <Field name="notes" component="textarea" placeholder="Notes"/>
                </div>
            </Col>
        </Row>
        <div className={style.footer}>
            <button type="submit" className="_button _primary _mediumPadding" disabled={submitting}>Submit Request</button>
        </div>
    </form>
);

export default reduxForm({
    form: 'RequestAvailabilityForm',
    validate
})(RequestAvailability);
