import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { Row, Col } from 'react-bootstrap';
import validate from './validation';
import BasicInput from '../_inputs/BasicInput';
import style from './style.scss';

const QuoteCode = ({ handleSubmit, pristine, submitting, error }) => (
    <form onSubmit={handleSubmit}>
        <div className={style.title}>
            <p>
                <em>Quote Code</em>
                <span>Enter your quote code below</span>
            </p>
        </div>
        <Row className="_noSpacing">
            <Col md={12}>
                {
                    error &&
                    <div className="_alertError">
                        <p>{error}</p>
                    </div>
                }
                <label className="_hideLabel">Quote Code</label>
                <div>
                    <Field
                        name="quoteCode"
                        component={(props) => {
                            return (
                                <BasicInput
                                    input={props.input}
                                    meta={props.meta}
                                    disabled={false}
                                    placeholder="Quote Code"
                                />
                            )
                        }
                    }/>
                </div>
            </Col>
        </Row>
        <div className={style.footer}>
            <button type="submit" className="_button _primary _mediumPadding _block" disabled={submitting}>Submit Code</button>
        </div>
    </form>
);

export default reduxForm({
    form: 'quoteCodeForm',
    validate,
    fields: [
        'quoteCode'
    ]
})(QuoteCode);
