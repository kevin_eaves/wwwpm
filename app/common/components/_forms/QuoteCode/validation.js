import React from 'react';

const validate = values => {
    const errors = {};

    if (!values.quoteCode) {
        errors.quoteCode = 'A Quote Code is Required';
    }

    return errors;
};

export default validate;
