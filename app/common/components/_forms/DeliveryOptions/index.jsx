import React from 'react';
import { Row, Col, Clearfix } from 'react-bootstrap';
import { Field } from 'redux-form';
import Radio from '../_inputs/Radio';
import Loader from '../../Loader';
import BasicInput from '../_inputs/BasicInput';
import style from './style.scss';
import moment from 'moment';

const DeliveryOptions = ({ availableShippingTypes = {}, handleUpdatingDeliveryType, shippingType, loading, error, deliveryInsurance, insuranceCostValue = 0, recalculateShippingOptions, pleaseChooseOption }) => (
    <Row className={style.container}>
        <Col md={12} className={style.titleHeading}>
            <h3 className="_mediumTitle _noTransform _withBackground">Shipping Options</h3>
            <button className={`_button ${style.updateButton}`} type="button" onClick={recalculateShippingOptions}>Get Shipping Options</button>
        </Col>
        <Clearfix className={style.options}>
            {
                loading
                ?   <div className={style.zeroState}>
                        <Loader text="Please hold on while we find the best shipping options for you" />
                    </div>
                :   <Results
                        availableShippingTypes={availableShippingTypes}
                        error={error}
                        pleaseChooseOption={pleaseChooseOption}
                        handleUpdatingDeliveryType={handleUpdatingDeliveryType}
                        shippingType={shippingType}
                        recalculateShippingOptions={recalculateShippingOptions}
                    />
            }
        </Clearfix>
        <Insurance
            availableShippingTypes={availableShippingTypes}
            error={error}
            loading={loading}
            deliveryInsurance={deliveryInsurance}
            insuranceCostValue={insuranceCostValue}
        />
    </Row>
)

const FetchingNotice = ({ error, pleaseChooseOption, recalculateShippingOptions }) => (
    <div className={style.zeroState}>
       {
           pleaseChooseOption || error
           ?    <Col md={12}>
                    <Errors error={error} pleaseChooseOption={pleaseChooseOption} />
                </Col>
           :    <p>Enter a shipping address above and <button className="_fancyUnderline" type="button" onClick={recalculateShippingOptions}>click here</button> to generate shipping&nbsp;options.</p>
       }
    </div>
);

const Errors = ({ error, pleaseChooseOption }) => (
    <Col md={12} id="deliveryError" className={`${style.zeroState} error`}>
       {
           !error
           ?   <div className="_alertError">
                   <p>{ pleaseChooseOption }</p>
               </div>
           :   <div className="_alertError">
                   <p>Looks like we couldn't find you! Please verify the address information you entered.</p>
               </div>
       }
    </Col>
)

const Results = ({ availableShippingTypes = {}, error, handleUpdatingDeliveryType, shippingType, recalculateShippingOptions, pleaseChooseOption }) => (
    <div>
        {
            Object.keys(availableShippingTypes).length && !error && pleaseChooseOption && !shippingType
                ? <Col md={12} id="deliveryError" className="error _alertError">
                     <p>{ pleaseChooseOption }</p>
                  </Col>
                : ''
        }
        {
            Object.keys(availableShippingTypes).length && !error
            ?   Object.keys(availableShippingTypes)
                    .sort((a, b)=>availableShippingTypes[a].cost - availableShippingTypes[b].cost)
                    .map((key) => (
                        <Col md={6} key={key} className={style.option}>
                            <Field
                                className={style.hidden}
                                name="deliveryType"
                                component="input"
                                type="radio"
                                onClick={(e) => handleUpdatingDeliveryType(e.target.value)}
                                id={`${availableShippingTypes[key].shippingOptionId}-option`}
                                props={{
                                    value: availableShippingTypes[key].shippingOptionId,
                                    id: `${availableShippingTypes[key].shippingOptionId}-option`,
                                    name: "deliveryType",
                                    type: availableShippingTypes[key].label,
                                    shippingType
                                }}
                            />
                            <label className="_radioLabel" htmlFor={`${availableShippingTypes[key].shippingOptionId}-option`}>
                                { availableShippingTypes[key].label }&nbsp;-&nbsp;
                                <em>{
                                    availableShippingTypes[key].cost > 0
                                        ?   '$' + Number(availableShippingTypes[key].cost).toLocaleString('en', {
                                                style: 'decimal',
                                                minimumFractionDigits: 2
                                            }) + ' CAD'
                                        :   'Free'
                                }</em>
                            <span>Estimated Arrival: {moment(availableShippingTypes[key].estimatedDelivery).format("DD/MM/YYYY")}</span>
                                <span className={`${style.radio} ${availableShippingTypes[key].label === shippingType && style.checked}`}><i></i></span>
                            </label>
                        </Col>
                    ))
            :   <Col md={12}>
                    <FetchingNotice error={error} pleaseChooseOption={pleaseChooseOption} recalculateShippingOptions={recalculateShippingOptions} />
                </Col>
        }
    </div>
);

const Insurance = ({ availableShippingTypes, error, loading, deliveryInsurance, insuranceCostValue }) => (
    <div>
        {
            !!Object.keys(availableShippingTypes).length && !error && !loading &&
            <Col md={12} className={style.insurance}>
                {
                    insuranceCostValue > 0
                    ?   <div className='fancyCheckbox'>
                            <Field
                                name="deliveryInsurance"
                                component={(props) => {
                                    return (
                                        <input
                                            value="1"
                                            type="checkbox"
                                            id="deliveryInsurance"
                                            onChange={() => props.input.onChange(deliveryInsurance ? false : true)}
                                            checked={deliveryInsurance ? true : false}
                                        />
                                    )
                                }}
                            />
                            <label htmlFor="deliveryInsurance">
                                <span></span>
                                Add shipping insurance for <strong>
                                    ${Number(insuranceCostValue).toLocaleString('en', {
                                        style: 'decimal',
                                        minimumFractionDigits: 2
                                    })}&nbsp;CAD
                                </strong>
                            </label>
                        </div>
                    :   <p><span className="fa fa-info-circle" aria-hidden="true"></span>Shipping insurance is included free of charge.</p>
                }
            </Col>
        }
    </div>
);

export default DeliveryOptions;



function checkForErrors (error, target) {
    if (error && document.querySelector('.error')) {
        scroller.scrollTo('deliveryError', {
            duration: 500,
            offset: -175,
            smooth: true
        })
    }
}
