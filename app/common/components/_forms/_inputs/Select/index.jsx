import React from 'react';
import SelectSearch from 'react-select';
import styles from './style.scss';
import { scroller } from 'react-scroll';

class SelectTag extends React.Component {
    handleSelectChange = (value) => {
        this.props.onChange(value);
    }
    renderOption = (option) => {
        const customLabel = this.props.labelKey;
        const centerText =  this.props.centerOptionText;
            return (
            <li className={`${styles.optionItem} ${centerText && styles.centerText}`}>
                {
                    option.icon &&
                    <i className={`fa fa-${option.icon}`} aria-hidden="true"></i>
                }
                {customLabel ? option[customLabel] : option.label}
            </li>
        );
    }
    render () {
        const {
            label,
            hideLabel,
            parent,
            multi,
            placeholder,
            propValue,
            disabled,
            theme,
            meta,
            name,
            onChange,
            isLoading = false,
            isClearable = true,
            isSearchable = false,
            input,
            valueKey,
            checkForError
        } = this.props;

        return (
            <div className={`${styles.container} ${meta && meta.touched && meta.error && 'error'}`} id={name}>
                <label className={hideLabel && '_hideLabel'}>{label}</label>
                <SelectSearch
                    {...this.props}
                    className={`${styles.select} ${disabled && styles.disabled} ${multi && styles.multiSelect} ${theme && styles[theme]} ${meta && meta.touched && meta.error && styles.error}`}
                    multi={multi}
                    name={parent}
                    value={input ? input.value : propValue}
                    placeholder={placeholder}
                    onChange={(item) => {
                        if (!item) {
                            onChange(null);
                            return false;
                        }

                        if (input) {
                            input.onChange(item.value || item[valueKey]);
                        }

                        if (typeof onChange === 'function') {
                            onChange(item.value || item[valueKey]);
                        }
                    }}
                    searchable={isSearchable}
                    clearable={isClearable}
                    theme={theme}
                    isLoading={!!isLoading}
                    optionRenderer={this.renderOption}
                />
                {checkForError && checkForError(!disabled && meta.touched && meta.error ? true : false, name)}
                {
                    meta && meta.touched && meta.error &&
                    <div className="_errorContainer select">
                        <span className="inputError">
                            <i className="fa fa-exclamation" aria-hidden="true"></i>
                        </span>
                        <span className="message">{meta.error}</span>
                    </div>
                }
            </div>
        )
    }
}

export default SelectTag;
