import React from 'react';

const Radio = ({ value, name, type, label, id, placeholder, handleOnChange, shippingType }) => (
    <input placeholder={label} data-type={type} value={value} name={name} type="radio" placeholder={placeholder} id={id} onChange={handleOnChange} checked={shippingType === type} />
);

export default Radio;
