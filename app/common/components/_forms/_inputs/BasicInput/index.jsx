import React from 'react';
import { scroller } from 'react-scroll';


const basicInput = ({ input, meta, type = 'text', placeholder, label, onBlur, disabled = false, maxLength = 255, onChange, checkForError, name }) => (
    <div className="basicInputContainer">
        <label className="_hideLabel">{label ? label : placeholder}</label>
        <input
            id={name}
            {...input}
            type={type}
            placeholder={placeholder}
            className={!disabled && meta.touched && meta.error && 'error'}
            onKeyDown={onBlur}
            disabled={disabled}
            maxLength={maxLength}
            onChange={({
                target: {
                    value
                }
            }) => {
                input.onChange(value);

                if (typeof onChange === 'function') {
                    onChange(value);
                }
            }}
        />
        {
            !disabled && meta.touched && meta.error &&
            <div className="_errorContainer">
                <span className="inputError">
                    <i className="fa fa-exclamation" aria-hidden="true"></i>
                </span>
                <span className="message">{meta.error}</span>
            </div>
        }
        {checkForError ? checkForError(!disabled && meta.touched && meta.error ? true : false, name) : ''}
    </div>
);

export default basicInput;
