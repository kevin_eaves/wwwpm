import React from 'react';
import { Field, reduxForm, change } from 'redux-form';
import { Row, Col } from 'react-bootstrap';
import style from './style.scss';

const RequestVehicleAvailability = ({ dispatch, name, email, notes, year, make, model, engine, handleSubmit, pristine, submitting, car }) => (
    <form
        onSubmit={handleSubmit}
        onChange={() => {
            dispatch(change('RequestVehicleAvailabilityForm', 'year', car.year));
            dispatch(change('RequestVehicleAvailabilityForm', 'make', car.make));
            dispatch(change('RequestVehicleAvailabilityForm', 'model', car.model));
            dispatch(change('RequestVehicleAvailabilityForm', 'engine', car.engine));
        }}
    >
        <div className={style.title}>
            <p>
                <em>Request Vehicle Availability</em>
                <span>Have a question about a vehicle's part, leave us a comment</span>
            </p>
        </div>
        <Row className="_noSpacing">
            <Col md={6}>
                <label className="_hideLabel">First Name</label>
                <div>
                    <Field {...name} name="name" component="input" type="text" placeholder="Name"/>
                </div>
            </Col>
            <Col md={6}>
                <label className="_hideLabel">Email</label>
                <div>
                    <Field {...email} name="email" component="input" type="email" placeholder="Email"/>
                </div>
            </Col>
        </Row>
        <Row className="_noSpacing">
            <Col md={12}>
                <label className="_hideLabel">Notes</label>
                <div>
                    <Field {...notes} name="notes" component="textarea" placeholder="Notes"/>
                </div>
            </Col>
        </Row>
        <Field {...year} name="year" component="input" type="hidden" />
        <Field {...make} name="make" component="input" type="hidden" />
        <Field {...model} name="model" component="input" type="hidden" />
        <Field {...engine} name="engine" component="input" type="hidden" />
        <div className={style.footer}>
            <button type="submit" className="_button _primary _mediumPadding" disabled={submitting}>Send Message</button>
        </div>
    </form>
);

export default reduxForm({
    form: 'RequestVehicleAvailabilityForm',
    fields: [
        'name',
        'email',
        'notes',
        'year',
        'make',
        'model',
        'engine'
    ]
})(RequestVehicleAvailability);
