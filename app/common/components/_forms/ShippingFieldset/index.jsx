import React from 'react';
import { Col, Clearfix } from 'react-bootstrap';
import { Field } from 'redux-form';
import { scroller } from 'react-scroll';
import BasicInput from '../_inputs/BasicInput';
import Select from '../_inputs/Select';
import style from './style.scss';

const ShippingFieldset = ({
    countriesOptions,
    regionOptions,
    clearShippingOptions,
    scrollToErrors
}) => (
    <Clearfix className={style.container}>
        <Col md={12}>
            <h1 className="_mediumTitle _noTransform _withBackground">Shipping Address</h1>
        </Col>
        <fieldset>
            <Col md={6}>
                <Field
                    name="firstName_shipping"
                    component={BasicInput}
                    props={{
                        disabled: false,
                        placeholder: 'First Name'
                    }}
                />
            </Col>
            <Col md={6}>
                <Field
                    name="lastName_shipping"
                    component={BasicInput}
                    props={{
                        disabled: false,
                        placeholder: 'Last Name'
                    }}
                />
            </Col>
            <Col md={6}>
                <Field
                    name="company_shipping"
                    component={BasicInput}
                    props={{
                        disabled: false,
                        placeholder: 'Company'
                    }}
                />
            </Col>
            <Col md={6}>
                <Field
                    name="phone_shipping"
                    component={BasicInput}
                    props={{
                        disabled: false,
                        placeholder: 'Phone Number'
                    }}
                />
            </Col>
            <Col md={12}>
                <Field
                    name="email_shipping"
                    component={BasicInput}
                    props={{
                        disabled: false,
                        placeholder: 'Email Address'
                    }}
                />
            </Col>
            <Col md={12}>
                <Field
                    name="address_shipping"
                    component={BasicInput}
                    props={{
                        disabled: false,
                        placeholder: 'Address Line 1'
                    }}
                />
            </Col>
            <Col md={12}>
                <Field
                    name="second_address_shipping"
                    component={BasicInput}
                    props={{
                        disabled: false,
                        placeholder: 'Address Line 2'
                    }}
                />
            </Col>
            <Clearfix>
                <Col md={6}>
                    <Field
                        name="city_shipping"
                        component={BasicInput}
                        props={{
                            disabled: false,
                            placeholder: 'City',
                            onChange: () => clearShippingOptions()
                        }}
                    />
                </Col>
                <Col md={6}>
                    <Field
                        name="country_shipping"
                        component={Select}
                        props={{
                            inputProps: {autoComplete: 'off', autoCorrect: 'off', spellCheck: 'off' },
                            theme: "square",
                            placeholder: "Country",
                            options: countriesOptions,
                            hideLabel: true,
                            isSearchable: true,
                            isClearable: false,
                            label: "Country",
                            onChange: () => clearShippingOptions()
                        }}
                    />
                </Col>
            </Clearfix>
            <Col md={6}>
                <Field
                    name="province_shipping"
                    component={Select}
                    props={{
                        inputProps: {autoComplete: 'off', autoCorrect: 'off', spellCheck: 'off' },
                        theme: "square",
                        placeholder: "Province",
                        options: regionOptions,
                        hideLabel: true,
                        isSearchable: true,
                        isClearable: false,
                        label: "Province / State",
                        onChange: () => clearShippingOptions()
                    }}
                />
            </Col>
            <Col md={6}>
                <Field
                    name="postalCode_shipping"
                    component={BasicInput}
                    props={{
                        disabled: false,
                        placeholder: 'Postal / Zip Code',
                        onChange: () => clearShippingOptions()
                    }}
                />
            </Col>
        </fieldset>
    </Clearfix>
);

export default ShippingFieldset;
