import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { Row, Col } from 'react-bootstrap';
import style from './style.scss';

const createAccount = ({ email, password, orderId, handleSubmit, pristine, submitting, partTitle, partNumber, manufacturer, createAccount, initialValues, Error }) => (
    <form onSubmit={handleSubmit} {...initialValues}>
        {
            Error &&
            <Col md={12}>
                <div className="_alertError">
                    <p>{Error}</p>
                </div>
            </Col>
        }
        <Row className="_noSpacing">
            <Col md={12}>
                <label className="_hideLabel">Email</label>
                <div>
                    <Field  {...email} name="email" component="input" type="email" placeholder="Email" />
                </div>
            </Col>
            <Col md={12}>
                <label className="_hideLabel">Password</label>
                <div>
                    <Field  {...password} name="password" component="input" type="password" placeholder="Password" />
                </div>
            </Col>
            <Field  {...orderId} name="orderId" component="input" type="hidden" />
        </Row>
        <div className={style.footer}>
            <button type="submit" className="_button _grey _dark _mediumPadding" disabled={submitting}>Create Account</button>
        </div>
    </form>
);

function getParameterByName(name, url) {
    if(typeof window == 'undefined'){
        return;
    }
    if(!url){
        url = window.location.href;
    };
    name          = name.replace(/[\[\]]/g, "\\$&");
    const regex   = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)");
    const results = regex.exec(url);
    if(!results){
        return null;
    };
    if(!results[2]){
        return '';
    };
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
const userEmail   = getParameterByName('email');
const orderNumber = getParameterByName('orderNumber');

export default reduxForm({
    form: 'createAccountForm',
    fields: [
        'email',
        'password',
        'orderId'
    ],
    initialValues: {
        email: userEmail,
        orderId: orderNumber
    }
})(createAccount);
