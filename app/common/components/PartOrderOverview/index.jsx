import React from 'react';
import LinkedImage from '../LinkedImage';
import QuantityPicker from '../../containers/QuantityPickerContainer';
import style from './style.scss';

const PartItem = ({
    id,
    image,
    title,
    partNumber,
    manufacturer,
    coreCost,
    freightCost,
    price,
    quantity,
    ehc,
    total,
    SellingMultiple = 1,
    url = ''
}) => (
    <div className={style.container}>
        <LinkedImage image={image} alt={title} slug={url} />
        <div className={style.content}>
            <p>
                <strong>{title}</strong>
                <span><em>Part Number</em>: {partNumber}</span>
                <span><em>Manufacturer</em>: {manufacturer}</span>
                <span>
                    <em>Price</em>:&nbsp;
                    ${Number(price).toLocaleString('en', {
                        style: 'decimal',
                        minimumFractionDigits: 2
                    })}&nbsp;CAD&nbsp;
                    {
                        SellingMultiple <= 1
                            ?   'each'
                            :   `per ${SellingMultiple}`
                    }
                </span>
                {
                    coreCost > 0 &&
                        <span>
                            <em>Core Cost</em>:&nbsp;
                            ${Number(coreCost).toLocaleString('en', {
                                style: 'decimal',
                                minimumFractionDigits: 2
                            })}&nbsp;CAD
                        </span>
                }
                {
                    freightCost > 0 &&
                        <span>
                            <em>Freight Cost</em>:&nbsp;
                            ${Number(freightCost).toLocaleString('en', {
                                style: 'decimal',
                                minimumFractionDigits: 2
                            })}&nbsp;CAD
                        </span>
                }
                {
                    ehc > 0 &&
                        <span>
                            <em>EHC</em>:&nbsp;
                            ${Number(ehc).toLocaleString('en', {
                                style: 'decimal',
                                minimumFractionDigits: 2
                            })}&nbsp;CAD
                        </span>
                }
            </p>
            <span className={style.quantity}>
                <QuantityPicker
                    partId={id}
                />
            </span>
            <span className={style.price}>Item Total: {Number(total).toLocaleString('en', {
                style: 'decimal',
                minimumFractionDigits: 2
            })}&nbsp;CAD</span>
        </div>
    </div>
);

export default PartItem;
