import React from 'react';
import { Grid, Row, Col, Clearfix } from 'react-bootstrap';
import ImageSlide from '../ImageSlide';
import QuantityPicker from '../../containers/DetailsQuantityPicker';
import style from './style.scss';

const PartThumbnail = ({
    images,
    partId,
    disabled
}) => (
    <div className={style.container}>
        <ImageSlide images={images} />
        {
            disabled &&
            <Row>
                <Col md={12}>
                    <QuantityPicker
                        partId={partId}
                    />
                </Col>
            </Row>
        }
    </div>
);

export default PartThumbnail;
