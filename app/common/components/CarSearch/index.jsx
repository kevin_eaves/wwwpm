import React from 'react';
import SelectSearch from '../_forms/_inputs/Select';
import { Grid, Row, Col, Clearfix } from 'react-bootstrap';
import style from './style.scss';

const PartSearch = ({
      handleYearChange,
      handleMakeChange,
      handleModelChange,
      handleEngineChange,
      year,
      make,
      model,
      engine,
      minimized = false
}) => (
    <Row lassName={style.container}>
        {
            !minimized &&
            <Col md={12}>
                <h1 className="_largeTitle" >I'm looking for parts for...</h1>
            </Col>
        }
        <Col xs={6} md={3}>
            <SelectSearch
                onChange={handleYearChange}
                placeholder="Year"
                options={year.options}
                multi={false}
                propValue={year.value}
                hideLabel
                labelKey="value"
                valueKey="value"
                label="Year"
                isSearchable={true}
                isLoading={!year.options.length}
                disabled={year.disabled}
            />
        </Col>
        <Col xs={6} md={3}>
            <SelectSearch
                onChange={handleMakeChange}
                placeholder="Make"
                options={make.options}
                multi={false}
                label="Make"
                hideLabel
                labelKey="MakeName"
                valueKey="MakeId"
                propValue={make.value}
                disabled={make.disabled}
                isSearchable={true}
                isLoading={year.value && !make.options.length}
            />
        </Col>
        <Col xs={6} md={3}>
            <SelectSearch
                onChange={handleModelChange}
                placeholder="Model"
                options={model.options}
                multi={false}
                label="Model"
                hideLabel
                labelKey="ModelName"
                valueKey="ModelID"
                propValue={model.value}
                disabled={model.disabled}
                isSearchable={true}
                isLoading={make.value && !model.options.length}
                />
        </Col>
        <Col xs={6} md={3}>
            <SelectSearch
                onChange={handleEngineChange}
                placeholder="Engine"
                options={engine.options}
                multi={false}
                label="Engine"
                labelKey="EngineDescription"
                valueKey="EngineConfigId"
                hideLabel
                propValue={engine.value}
                disabled={engine.disabled}
                isSearchable={true}
                isLoading={model.value && !engine.options.length}
            />
        </Col>
    </Row>
);

export default PartSearch;
