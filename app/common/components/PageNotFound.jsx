import React from 'react';
import Helmet from './Helmet';
import { Link } from 'react-router';

export const PageNotFound = ({}) => (
    <div className="pageNotFound inner">
        <Helmet
            title="404"
        />
        <div className="overlay"></div>
        <div className="container">
            <div className="copy alignRight">
                <h2 className="_fancyUnderline _largeTitle">Page not found</h2>
                <p>Sorry, but the page you are looking for cannot be found. Try checking the url for errors and refreshing the page</p>
                <Link to="/" className="_button">Search for parts</Link>
            </div>
        </div>
    </div>
)

export default PageNotFound;
