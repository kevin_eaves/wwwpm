import React from 'react';
import { Link } from 'react-router';
import Helmet from './Helmet';

class Shipping extends React.Component {
    render(){
        return(
            <div>
                <Helmet
                    title="Shipping and Return Warrenty"
                />
                <div className="heading">
                    <h2 className="_fancyUnderline">Shipping</h2>
                </div>
                <div className="body">
                    <p>We strive to provide our customers with the highest quality of service. As a result, if your order is over $75 (before taxes and other charges) the order will ship for free within Canada via ground shipping (see below for exceptions). If ground shipping is not available for the shipping address then shipping will not be free. There are also faster shipping methods available to our customers if requested. Most orders will ship out within 24 hours after payment has been approved. Estimated delivery time varies from 1 to 5 business days depending on the shipping address and availability.</p>

                    <p>Once the shipment has been processed, an email confirmation with a tracking number will be provided (please be advised that it may take up to 24 hours for the tracking number to be functional). We do not ship to P.O. Boxes. All addresses must be a physical street address.</p>

                    <p>The following postal codes do not qualify for free shipping B0E2P0, E3Y0B7, E7A0A6, R4L1B4, R5A1E3, R5G0N8, R6W0A9, R8A0C5, S9X1T8, T1A7G2, T9H0B5, V8A0A7, V9G0A1, V9G0A7, V9L0C2, V9W0A7, and V9W0A9. Also, postal codes that start with A0, A8A, E2L, E4A, E4C, E5, E6A, E6H, E6K, E7N, E9C, G0, G4T, G4V, G4X, G5J, G7N, G7P, G7X, G8, J0, J9, P0L, P0T, P0V, R0, R9A, S0, S2V, T0, V0, V1, V2J, V9Z, X0, Y0, or Y1A may not qualify for free shipping. This list may change without prior notice. Postal codes that do not qualify for free shipping may still qualify for a shipping discount.</p>
                </div>
                <div className="heading">
                    <h2 className="_fancyUnderline">Returns</h2>
                </div>
                <div className="body">
                    <p>New items must be returned within 45 days of the original invoice date. Please ensure that all original packaging is intact as we will not accept product without the original packaging.</p>

                    <p>In the event that a product does need to be returned, we try to make the process seamless and as hassle-free as possible. To start the return process, we require that a Return Merchandise Authorization "RMA" form be completed. Upon receiving the RMA, we will issue our returns department address to our customer. The return shipping costs will be the responsibility of the customer unless the error was a direct result of PartsMonkey error. Once the product is received, it will be inspected to ensure the original packaging has not been damaged, no parts or hardware are missing, all installation instructions are present, the part is not damaged or disassembled, and no evidence is present of the product being installed and removed, then a full credit will be issued. If the product is returned as new and unused then a 15% restocking fee will be applied. Initial shipping costs are non-refundable.</p>

                    <p>Please note that all new products must be returned in the same manner as they were first shipped. If a product is returned and the original packaging of the product (not the shipping packaging) has been defaced in any manner a 50% restocking fee may apply.</p>

                    <p>Where a core is to be returned, the product must be sent back in the original packaging, all fluids must be completely drained and drain plugs installed where applicable and the product must be wrapped in plastic. Upon receiving the core it will be inspected to ensure it can be rebuilt and, if the part passes inspection, a credit will be issued. If the housing is cracked or damaged, or pieces of the original are missing a partial credit may be issued. The shipping charges for return cores will be the responsibility of the customer; however, PartsMonkey can generate the mailing label and charge the customer the appropriate rate, saving the customer on shipping costs.</p>

                    <p>
                        <em>The following items cannot be returned:</em>
                        <ul>
                            <li>Electrical components (such as computers, mass air flow sensors, relays, etc.)</li>
                            <li>Special order products</li>
                            <li>Promotional or discontinued products</li>
                        </ul>
                    </p>
                </div>
                <div className="heading">
                    <h2 className="_fancyUnderline">Warranty</h2>
                </div>
                <div className="body">
                    <p>Please contact customer service regarding any warranty concerns. Please note that we will not cover any labour claims against defective products. If the product is defective then we will do our best effort to have a replacement part sent out in a timely manner or issue a credit for the product.</p>

                    <p><em>Note: The information shown on this web site has been compiled from the latest available data. We do not assume any liability for omissions or errors.</em></p>

                    <Link to="/" className="_button _grey _dark _mediumPadding">Find parts now</Link>
                </div>
            </div>
        );

    }
}

export default Shipping;
