import React from 'react';
import style from './style.scss';

const QuantityPicker = ({
    min = 1,
    max = 1,
    value = 1,
    adjustQuantity,
    onSubmit
}) => (
    <div className={style.container}>
        <div className={`${style.picker} picker`}>
            <button
                onClick={() => adjustQuantity(value - 1)}
                disabled={value === min}
                type="button"
            >
                <span className={`${style.inputNumberDecrement} fa fa-minus`}></span>
            </button>
            <input
                className={style.inputNumber}
                type="number"
                value={value}
                min={min}
                max={max}
                onChange={value => {
                    if (value.target.value < min) {
                        return adjustQuantity(min);
                    }

                    if (value.target.value > max) {
                        return adjustQuantity(max);
                    }

                    adjustQuantity(value.target.value);
                }}
            />
            <button
                onClick={() => adjustQuantity(value + 1)}
                disabled={value >= max}
                type="button"
            >
                <span className={`${style.inputNumberIncrement} fa fa-plus`}></span>
            </button>
        </div>
        {
            onSubmit
                ?   <button
                        className="_button _grey _dark addToCart"
                        disabled={value > max}
                        onClick={onSubmit}
                        type="button"
                    >
                        Add to cart
                    </button>
                :   <a
                        href="#"
                        onClick={(e) => {
                            e.preventDefault();
                            adjustQuantity(0);
                        }}
                    >
                        Remove
                    </a>
        }
    </div>
);

export default QuantityPicker;
