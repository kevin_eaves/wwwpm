import React from 'react';
import { Link, browserHistory } from 'react-router';
import style from './style.scss';

const Modal = ({ modalType, modalIsFor, resetModal, children, error, success }) => (
    <div className={`_modal ${style.container} ${modalIsFor == modalType && style.open}`}>
        <div className={style.overlay} onClick={resetModal}></div>
        <div className={style.modal}>
            {
                error &&
                <div className="_alertError">
                    <p>{error}</p>
                </div>
            }
            <a className={`${style.close}`} onClick={resetModal}>
                <div className={style.outer}>
                    <div className={style.inner}>
                        <label>Back</label>
                    </div>
                </div>
            </a>
            {
                success
                ?
                <div className="_alertSuccess">
                    <p>{success}</p>
                </div>
                :
                children
            }
        </div>
    </div>
)

export default Modal;
