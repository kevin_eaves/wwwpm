import React from 'react';
import { Grid, Row, Col, Clearfix } from 'react-bootstrap';
import PartThumbnail from './PartThumbnail';
import PartSpecs from './PartSpecs';
import Helmet from './Helmet';

const Details = ({
    part = false,
    group,
    images,
    populateUrl
}) => (
    <div>
        <Helmet
            title={`${part ? part.ProductInfo.PartLabel : 'Product'}`}
        />
        <Grid>
            <Row>
                <Col sm={3} xs={12}>
                    <PartThumbnail
                        images={images}
                        title={part ? part.ProductInfo.PartLabel : 'Part Thumbnail'}
                        partId={part.ProductInfo.WHIProductId}
                        disabled={!!part.ProductInfo.Inventory.QuantityOnHand}
                    />
                </Col>
                <Col sm={9} xs={12} className="_contentSection">
                    {
                        part
                            ?   <PartSpecs details={part} group={group} populateUrl={populateUrl} />
                            :   <h2 className="_fancyUnderline _largeTitle">Part not found</h2>
                    }
                </Col>
            </Row>
        </Grid>
    </div>
);

export default Details;
