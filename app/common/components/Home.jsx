import React from 'react';
import { Grid, Row, Col, Clearfix } from 'react-bootstrap';
import QueryTitle from './QueryTitle';
import Loader from './Loader';
import SearchResults from '../containers/SearchResultsContainer';
import Helmet from './Helmet';

class Home extends React.Component {
    render(){
        const {
            showResults,
            location,
            car,
            selectedGroup,
            isLoading = false,
            urlParams,
            availableGroups,
            parsedVehicle,
            handleVehicleRequest,
            title
        } = this.props;

        let homeRender;
        if(isLoading){
            homeRender = <LoaderState />;
        }else if (showResults == 0 ) {
            homeRender = <NoResultsState
                searchTerm={car}
            />;
        }else if(!showResults){
            homeRender = <ZeroState />;
        }else if(availableGroups.error){
            homeRender = <ErrorState handleVehicleRequest={handleVehicleRequest} parsedVehicle={parsedVehicle} />;
        }else{
            homeRender = <Row>
                            <Col xs={12}>
                                <QueryTitle
                                    car={car}
                                    category={selectedGroup.group}
                                    subCategory={selectedGroup.subGroup}
                                />
                            </Col>
                            <Col xs={12}>
                                <SearchResults
                                    urlParams={urlParams}
                                    pathName={location.pathname}
                                    car={car}
                                    selectedGroup={selectedGroup}
                                    availableGroups={availableGroups}
                                />
                            </Col>
                        </Row>
        }

        return(
            <div>
                <Helmet
                    title={title}
                />
                <section>
                    {homeRender}
                </section>
            </div>
        );

    }
}

const ErrorState = ({ handleVehicleRequest, parsedVehicle }) => (
    <Row className="_centerText section">
        <p className="_subHeading _noBottomSpacing">
            We don't currently have any parts for this vehicle in our inventory, but we might be able to find them for you! <br></br><a href="javascript:void(0)" onClick={() => handleVehicleRequest({
                year: parsedVehicle.year,
                make: parsedVehicle.make,
                model: parsedVehicle.model,
                engine: parsedVehicle.engine
            })}>Click here</a> to request a part.
        </p>
    </Row>
);

const ZeroState = ({  }) => (
    <Row className="_centerText section">
        <p className="_subHeading _noBottomSpacing">To view parts&hellip;</p>
        <h2 className="_fancyUnderline _largeTitle">Select your car using the fields above</h2>
    </Row>
);

const LoaderState = ({  }) => (
    <Row className="_centerText section">
        <Loader />
        <p className="_subHeading _noBottomSpacing">Please hold tight</p>
        <h2 className="_fancyUnderline _largeTitle">We are searching for your parts</h2>
    </Row>
);

const NoResultsState = ({ searchTerm }) => (
    <Row className="_centerText section">
        <p className="_subHeading _noBottomSpacing">Please refine your search and try again</p>
        <h2 className="_fancyUnderline _largeitle">No result found for {searchTerm}</h2>
    </Row>
);


export default Home;
