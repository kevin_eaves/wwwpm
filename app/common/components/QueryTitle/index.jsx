import React from 'react';
import style from './style.scss';

const QueryTitle = ({ car, category, subCategory }) => (
    <div className={style.queryTitle}>
        <p>Browsing parts for...</p>
        <h2>{ car }</h2>
        {category &&
        <p className="_uppercased">{ category } <i className="fa fa-caret-right" aria-hidden="true"> </i> { subCategory && subCategory }</p>
        }
    </div>
);

export default QueryTitle;
