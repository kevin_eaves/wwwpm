import React from 'react';
import { Link } from 'react-router';
import style from './style.scss';

const LinkedImage = ({
    image = '/images/logo.png',
    alt,
    slug,
    localImage,
    onClick = () => {}
}) => (
    <div className={`${style.logo} linkedImage`}>
        <Link
            to={slug}
            onClick={onClick}
        >
            <img src={`${localImage ? '/images/' : ''}${image && image != null ? image : '/images/logo.png'}`} alt={alt}/>
        </Link>
    </div>
);

export default LinkedImage;
