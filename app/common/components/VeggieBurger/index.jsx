import React from 'react';
import style from './style.scss';

const VeggieBurger = ({ toggle }) => (
    <div className={`${style.container} ${toggle && style.open}`}>
      <span></span>
      <span></span>
      <span></span>
      <span></span>
    </div>
);

export default VeggieBurger;
