import React from 'react';
import CartFooter from '../CartFooter';
import { browserHistory } from 'react-router';
import Loader from '../Loader';
import { Col } from 'react-bootstrap';
import PartOrderOverview from '../../containers/PartOrderOverview';
import Helmet from '../Helmet';
import style from './style.scss';

export default ({ parts, totalPartsInOrder, subTotal, openLoginModal, openQuoteCodeModal }) => (
    <section className={`_hideOverflow ${style.cartPreview}`}>
        <Helmet
            title="Cart Overview"
        />
        <div className={style.overview}>
            <Col md={12} className={style.message}>
                <h1 className="_largeTitle">Cart Overview</h1>
            </Col>
            {
                parts.map(({ ProductInfo } = {}, i) => (
                    <Col md={6} key={i} className={style.partItem}>
                        {
                            ProductInfo
                                ?   <PartOrderOverview
                                        id={ProductInfo.WHIProductId}
                                    />
                                :   <Loader
                                        text="Loading part&hellip;"
                                    />

                        }
                    </Col>
                ))
            }
            {
                !parts.length &&
                    <div className="_centerText">
                        <p className="_subHeading">Your cart is empty</p>
                    </div>
            }

            <Col md={12}>
                <CartFooter>
                    <p>
                        <span>
                            {totalPartsInOrder}
                        </span>
                        <strong>${Number(subTotal).toLocaleString('en', {
                            style: 'decimal',
                            minimumFractionDigits: 2
                        })} CAD</strong>
                    </p>
                    <div className="rightSide">
                        <button
                            type="button"
                            onClick={openQuoteCodeModal}
                            className="_button _grey _mediumPadding"
                        >
                            Quote Code
                        </button>
                        {
                            parts.length > 0 &&
                            <button
                                type="button"
                                onClick={() => {browserHistory.push('/cart/checkout')}}
                                className="_button _grey _dark _mediumPadding"
                            >
                                Continue
                            </button>
                        }
                    </div>
                </CartFooter>
            </Col>
        </div>
    </section>
);
