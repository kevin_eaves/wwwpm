import React, { Component } from 'react';
import { pure } from 'recompose';
import style from './style.scss';

export default pure(({
    body,
    visible
}) => (
    <div className={`${style.container} ${visible && style.visible}`}>
        <p>{ body }</p>
    </div>
));
