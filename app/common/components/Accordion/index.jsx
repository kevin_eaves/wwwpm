import React from 'react';
import { Link } from 'react-router';
import style from './style.scss';

const SubItem = ({ value, onClick, slug }) => (
    <li>
        <Link
            onClick={onClick}
            to={`/${slug}`}
            className="_underline"
        >{ value }</Link>
    </li>
);

class Item extends React.Component{

    constructor(props){
        super(props);

        this.state = {
            active:props.active
        }
    }

    componentDidMount () {
        if(this.props.open){
            this.setState({
                active: true
            });
        }
    }

    onClick = (e) => {
        e.stopPropagation();

        const {
            selectGroup,
            value,
        } = this.props;


        selectGroup(value);
        this.setState({
            active: !this.state.active
        });
    }

    render(){

        const {
            children,
            value,
            active,
            selectGroup,
            open,
            slug
        } = this.props;

        return(
            <li>
                <Link
                    onClick={this.onClick}
                    to={`/${slug}`}
                >
                    <p className={`${this.state.active && style.open}`}>{ value }</p>
                    <i className={`${style.icon} ${!this.state.active && style.collapsed}`}></i>
                </Link>
                <ul className={`${style.subItemContainer} ${this.state.active && style.active}`}>
                    { children }
                </ul>
            </li>
        );
    }
}

const Accordion = ({ children }) => (
    <ul className={style.container}>{ children }</ul>
);

export { Accordion, Item, SubItem };
