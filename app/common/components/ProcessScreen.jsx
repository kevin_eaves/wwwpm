import React from 'react';
import { Grid, Row, Col, Clearfix } from 'react-bootstrap';
import Loader from './Loader';
import Helmet from './Helmet';


class ProcessScreen extends React.Component {

    render(){

        return(
            <div>
                <Helmet
                    title="Procesing Order"
                />
                <div>
                  <LoaderState />
                </div>
            </div>
        );

    }
}

const LoaderState = ({  }) => (
    <Row className="_centerText section">
        <Loader />
        <p className="_subHeading _noBottomSpacing">Please hold tight</p>
        <h2 className="_fancyUnderline _largeTitle">We're processing your order</h2>
    </Row>
);


export default ProcessScreen;
