import React from 'react';
import { Link } from 'react-router';
import style from './style.scss';


class MyCart extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
            animate: false
        }
    }
    componentWillReceiveProps (nextProps) {
        if(nextProps.cartCount == this.props.cartCount){
            return;
        }

        this.setState({animate: true});

        setTimeout(() => {
            this.setState({animate: false});
        }, 400);
    }

    render () {
        const {
            cartCount
        } = this.props;

        return (
            <Link className={`${style.myCart} ${this.state.animate && style.explode}`} to="/cart">
                My Cart
                <em>{cartCount}</em>
            </Link>
        )
    }
}

export default MyCart;
