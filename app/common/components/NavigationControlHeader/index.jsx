import React from 'react';
import { Link } from 'react-router';
import MyCart from './MyCart';
import Banner from '../Banner';
import CarSearch from '../../containers/CarSearchContainer';
import style from './style.scss';
import { Grid, Row, Col, Clearfix } from 'react-bootstrap';
import LinkedImage from '../LinkedImage';
import cookie from 'react-cookie';

const Navigation = ({ logo, toggleMobileMenu, handleModalRequest, cartCount, wipeSearchHistory, searchable, theme, handleLogout, toggleMenuDrawer, toggleSearchBar, showBanner}) => (
    <Grid className={`${style.navigation} ${toggleMobileMenu && style.open} ${theme && theme} ${searchable && style.searchable}`}>
        <div className="container">
            <div className={style.logo}>
                <LinkedImage localImage image="logo.png" alt="Part Money Logo" slug="/" />
            </div>
            <div className={`${style.inner} ${searchable && style.searchable}`}>
                <Banner visible={!searchable} body={[<strong>Free Shipping*</strong>, "on orders over $75 CAD"]} />
                <nav>
                    <ul>
                        {logo &&
                        <li>
                            <LinkedImage slug="/" />
                        </li>
                        }
                        <li>
                            <Link to="/">
                                Find Parts
                            </Link>
                        </li>
                        <li>
                            <Link to="/vehicles">
                                Makes
                            </Link>
                        </li>
                        <li>
                            <Link to="/about">
                                About
                            </Link>
                        </li>
                        <li>
                            <a
                                href="javascript:void(0)"
                                onClick={() => {
                                    handleModalRequest({modalType: "ContactModal"});
                                    toggleMenuDrawer();
                                }}
                            >
                                Contact
                            </a>
                        </li>
                    </ul>
                </nav>
                <div className={style.search}>
                    <CarSearch minimized />
                </div>
            </div>
            <div className={style.absoluteItems}>
                <MyCart cartCount={cartCount} inSearchable={searchable} />
                {
                    toggleSearchBar &&
                        <button
                            className={style.toggleSearch}
                            onClick={toggleSearchBar}
                            title="Search for part"
                            type="button"
                        >
                            <i
                                className="fa fa-search"
                                aria-hidden="true"
                            ></i> Search
                        </button>
                }
            </div>
        </div>
    </Grid>
);

export default Navigation;
