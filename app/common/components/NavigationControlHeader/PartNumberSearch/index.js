import React from 'react';
import classNames from 'classnames';
import style from './style.scss';
import PartNumberSearch from '../../_forms/PartNumberSearch';

export default ({
    isVisible = true
}) => (
    <div
        className={classNames(
            style.container,
            {
                [style.visible]: isVisible
            }
        )}
    >
        <p>Already know the part you're looking for?</p>
        <div className={style.partSearch}>
            <PartNumberSearch />
        </div>
    </div>
);
