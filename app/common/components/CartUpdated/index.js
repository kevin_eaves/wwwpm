import React from 'react';
import { Link } from 'react-router';
import style from './style.scss';

export default ({
    viewCart,
    dismissModal
}) => (
    <div className={style.title}>
        <p>
            <em>Shopping Cart</em>
            <span>Your part was added to the Shopping Cart.</span>
        </p>
        <ul className={style.actions}>
            <li>
                <button
                    className="_button"
                    onClick={dismissModal}
                >
                    Continue shopping
                </button>
            </li>
            <li>
                <button
                    className="_button _grey"
                    onClick={viewCart}
                >
                    View cart
                </button>
            </li>
        </ul>
    </div>
);
