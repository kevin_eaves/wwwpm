import React from 'react';
import { Grid, Row, Col, Clearfix } from 'react-bootstrap';
import style from './style.scss';
import Loader from '../Loader';

const PartThumbnail = ({
    details,
    group,
    populateUrl
}) => (
    <div className={style.container}>
        <p>{details.ProductInfo.ManuFacturerName}</p>
        <h2 className="_fancyUnderline">{details.ProductInfo.PartLabel}</h2>
        <dl>
            <dt>Part Number</dt><dd>{details.ProductInfo.PartNumber ? details.ProductInfo.PartNumber : 'NA'}</dd>
            <dt>Manufacturer</dt><dd>{details.ProductInfo.ManuFacturerName ? details.ProductInfo.ManuFacturerName : 'NA'}</dd>
            {group.group ? <div><dt>Category</dt><dd>{group.group}</dd></div> : null}
            {group.subGroup ? <div><dt>Subcategory</dt><dd>{group.subGroup}</dd></div> : null}
            <dt>Availability</dt>
            <dd>
                {
                    details.ProductInfo.Inventory.QuantityOnHand > 0
                        ? details.ProductInfo.Inventory.BranchCode == -4
                            ?   <em className={style.green}> 3 weeks</em>
                            :   details.ProductInfo.Inventory.isRemoteInventory
                                    ?   <em className={style.green}> 1-2 days</em>
                                    :   <em className={style.green}> In stock</em>
                                :   <em className={style.red}> Out of stock</em>
                }
            </dd>
            <dt>Price</dt>
            <dd>
                <em>
                {
                    details.ProductInfo.Inventory.Cost
                    ? `$${details.ProductInfo.Inventory.Cost} CAD/${details.ProductInfo.Inventory.SellingMultiple <= 1 ? 'each' : `per ${details.ProductInfo.Inventory.SellingMultiple}`}`
                    : 'NA'

                }
                </em>
            </dd>
            {
                details.ProductInfo.Inventory.FreightCost > 0 &&
                    <dt>Freight Cost</dt>
            }
            {
                details.ProductInfo.Inventory.FreightCost > 0 &&
                    <dd>
                        ${Number(details.ProductInfo.Inventory.FreightCost).toLocaleString('en', {
                            style: 'decimal',
                            minimumFractionDigits: 2
                        })} per order
                    </dd>
            }

            {
                details.ProductInfo.Inventory.CoreCost > 0 &&
                    <dt>Core Cost</dt>
            }
            {
                details.ProductInfo.Inventory.CoreCost > 0 &&
                    <dd>
                        ${Number(details.ProductInfo.Inventory.CoreCost).toLocaleString('en', {
                            style: 'decimal',
                            minimumFractionDigits: 2
                        })}
                    </dd>
            }

            {
                details.ProductInfo.Inventory.EHC > 0 &&
                    <dt>EHC</dt>
            }
            {
                details.ProductInfo.Inventory.EHC > 0 &&
                    <dd>
                        ${Number(details.ProductInfo.Inventory.EHC).toLocaleString('en', {
                            style: 'decimal',
                            minimumFractionDigits: 2
                        })}
                    </dd>
            }
        </dl>
        {
            details.ProductInfo.ProductAttributes &&
            <div>
                <h3>Additional Specifications</h3>
                <dl>
                    {
                        details.ProductInfo.ProductAttributes.map((att) =>(
                            <div className={style.item}>
                                <dt>{att.attribute}</dt>
                                <dd>{att.value}</dd>
                            </div>
                        ))
                    }
                </dl>
            </div>
        }


        <div className={style.fitments}>
            <h3>Vehicle Fitment</h3>
            {
                details.fitments
                    ?   <ul>
                            {
                                details.fitments.map((item, i) => {
                                    const {
                                        Year,
                                        MakeName,
                                        ModelName,
                                        EngineDescription,
                                        NoteText
                                    } = item;
                                    const car = `${Year} ${MakeName} ${ModelName}`;
                                    return (
                                        <li key={i}>
                                            <p>
                                                <em>{car}</em>
                                                <span>{EngineDescription}</span>
                                                {NoteText && `Notes: ${NoteText}`}
                                            </p>
                                        </li>
                                    )
                                })
                            }
                        </ul>
                    :   <Loader />
            }
        </div>
        <a
            onClick={populateUrl}
            className="_button _grey"
        >
            Return to browsing
        </a>
    </div>
);

export default PartThumbnail;
