import React from 'react';
import { Link } from 'react-router';
import LinkedImage from '../LinkedImage';
import FooterNav from './NavigationControlFooter';
import InfoList from './InfoList';
import MakeList from './MakeList';
import { Grid, Row, Col, Clearfix } from 'react-bootstrap';
import style from './style.scss';

const Footer = () => (
    <div className={style.container}>
        <div className={style.banner}>
            <img src="../../images/parts-banner-footer.png" alt="footer banner image"/>
        </div>
        <div className={style.footer}>
            <Grid>
                <MakeList />
                <InfoList />
                <FooterNav />
            </Grid>
        </div>
    </div>
);

export default Footer;
