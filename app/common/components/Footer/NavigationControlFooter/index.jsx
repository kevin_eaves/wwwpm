import React from 'react';
import { Link } from 'react-router';
import style from './style.scss';
import { Grid, Row, Col, Clearfix } from 'react-bootstrap';

const NavigationFooter = ({  }) => (
    <div className={style.navigation}>
        <ul>
             <li>
                <Link  to="/about">
                    About&nbsp;PartsMonkey
                </Link>
            </li>
            <li>
                <Link  to="/privacy-policy">
                    Privacy&nbsp;Policy
                </Link>
            </li>
            <li>
                <Link  to="/terms-of-use">
                    Terms of&nbsp;Use
                </Link>
            </li>
            <li>
                <Link  to="/shipping-and-return-warranty">
                    Shipping &amp; Returns&nbsp;Warranty
                </Link>
            </li>
        </ul>
    </div>
);

export default NavigationFooter;
