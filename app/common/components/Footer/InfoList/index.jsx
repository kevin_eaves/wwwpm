import React from 'react';
import style from './style.scss';

const InfoList = ({  }) => (
    <div className={ style.infoList }>
        <ul>
            <li>&copy; 2016 Parts Monkey</li>
            <li><a href="mailto:info@partsmonkey.com">info@partsmonkey.com</a></li>
            <li><a href="tel:18552237278">1-855-223-7278</a></li>
            <li>Mon-Fri 8AM-5PM</li>
        </ul>
    </div>
);

export default InfoList;
