import React from 'react';
import { Link } from 'react-router';
import style from './style.scss';
import slug from 'slug';

const makes = [
    'Acura',
    'Audi',
    'BMW',
    'Buick',
    'Cadillac',
    'Chevrolet',
    'Chrysler',
    'Dodge',
    'Ford',
    'GMC',
    'Honda',
    'Hyundai',
    'Kia',
    'Lincoln',
    'Mazda',
    'Mercedes-Benz',
    'Nissan',
    'Subaru',
    'Toyota',
    'Volkswagen'
];

const MakeList = () => (
    <div className={ style.makeList }>
        <ul>
            {makes.map(make => <li><Link to={`/vehicles/${slug(make)}`}>{make}</Link></li>)}
        </ul>
    </div>
);

export default MakeList;
