import React from 'react';
import { Grid, Row, Col, Clearfix } from 'react-bootstrap';
import { Accordion, Item, SubItem } from '../Accordion';
import slug from 'slug';
import Loader from '../Loader';
import PartItem from '../../containers/PartItemContainer';
import style from './style.scss';
import { scroller } from 'react-scroll';

class SearchResults extends React.Component {
    componentDidMount () {
        scroller.scrollTo('SearchResults', {
            duration: 500,
            delay: 25,
            offset: -365,
            smooth: true
        })
    }
    render () {
        const {
            categories,
            parts,
            showLoader,
            selectGroupFilter,
            pathName,
            selectSubGroupFilter,
            urlParams,
            vehicleSlug
        } = this.props;

        return (
            <section className={style.container}>
                <span className={style.sideBar}></span>
                <Col md={5} className={`${style.filters} _removePadding`}>
                    <Accordion>
                        {
                            categories.parents &&
                            Object.keys(categories.parents).map((key) => {
                                const category = categories.parents[key];
                                return (
                                    <Item
                                        key={`${key}-${category.GroupName}`}
                                        value={category.GroupName}
                                        slug={`${vehicleSlug}/` + slug(category.GroupName)}
                                        selectGroup={() => selectGroupFilter(category.WHGroupId)}
                                        open={urlParams.group == slug(category.GroupName) ? true : false}
                                    >
                                        {
                                            categories.parents[key].subGroups.map((subGroupId, i) => {
                                                return <SubItem
                                                            key={i}
                                                            slug={`${vehicleSlug}/${slug(category.GroupName)}/${slug(categories.subGroups[subGroupId].PartTypeDescription)}`}
                                                            value={categories.subGroups[subGroupId].PartTypeDescription}
                                                            onClick={() => {
                                                                selectSubGroupFilter(subGroupId);
                                                                scroller.scrollTo('SearchResults', {
                                                                    duration: 500,
                                                                    delay: 25,
                                                                    offset: -185,
                                                                    smooth: true
                                                                })
                                                            }}
                                                        />
                                            })
                                        }
                                    </Item>
                                )
                            })
                        }
                    </Accordion>
                </Col>
                <Col md={7} className={`${style.itemList} ${!Object.keys(parts).length && style.noResults}`} id="SearchResults">
                    <div className={style.inner}>
                        {
                            Object.keys(parts).length
                                ?   Object.keys(parts).map((key) => {
                                    const item = parts[key];

                                    if(!item.ProductInfo){
                                        console.warn('Item missing product info', item);
                                        return;
                                    }

                                    const {
                                        Images,
                                        PartLabel,
                                        PartNumber,
                                        ManuFacturerName,
                                        Inventory,
                                        WHIProductId,
                                        DetailsURL,
                                        ProductAttributes
                                    } = item.ProductInfo;
                                    const price = {
                                        value: Inventory.Cost,
                                        per:   Inventory.SellingMultiple
                                    }

                                    const placementOnVehicle = ProductAttributes &&
                                        ProductAttributes.find(item => item.attribute === 'Placement On Vehicle');

                                    let partImage = null;
                                    if(Images){
                                        if (Images.indexOf(';') > -1) {
                                            partImage = Images.substr(0, Images.indexOf(';'));
                                        } else {
                                            partImage = Images;
                                        }
                                    }

                                    return (
                                        <PartItem
                                            key={`${key}-${PartNumber}`}
                                            id={PartNumber}
                                            image={partImage}
                                            title={PartLabel}
                                            partNumber={PartNumber}
                                            manufacturer={ManuFacturerName}
                                            note={item.NoteText}
                                            price={price}
                                            link={DetailsURL}
                                            quantity={Inventory.QuantityOnHand}
                                            PartInventoryId={Inventory.PartInventoryId}
                                            isRemoteInventory={Inventory.isRemoteInventory}
                                            WHIProductId={WHIProductId}
                                            placement={placementOnVehicle && placementOnVehicle.value}
                                            electronicsHandlingCharge={Inventory.EHC}
                                            coreCost={Inventory.CoreCost}
                                        />
                                    )
                                })
                            :   showLoader
                                    ?   <div className={style.itemMessage}>
                                            <Loader />
                                            <p className="_subHeading _noBottomSpacing">Please hold while we are searching for your parts</p>
                                        </div>
                                    :   <div className={style.itemMessage}>
                                            <p className="_subHeading _noBottomSpacing">Please select a group to see your parts</p>
                                        </div>
                        }
                    </div>
                </Col>
            </section>
        )
    }
}
export default SearchResults;
