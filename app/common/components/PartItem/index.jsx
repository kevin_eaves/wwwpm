import React from 'react';
import { Link } from 'react-router';
import LinkedImage from '../LinkedImage';
import style from './style.scss';

const PartItem = ({
    id,
    image = '../../images/part-placeholder.png',
    title,
    partNumber,
    manufacturer,
    note,
    price = {
        value: '0',
        per: 0
    },
    quantity,
    handleAvailabilityRequest,
    addCartItem,
    link = '',
    PartInventoryId,
    WHIProductId,
    isRemoteInventory,
    placement,
    electronicsHandlingCharge,
    coreCost,
    viewItem
}) => (
    <div className={style.container}>
        <LinkedImage
            image={image}
            alt={title}
            slug={link}
            onClick={() => viewItem({
                partId: WHIProductId
            })}
        />
        <div className={style.content}>
            <p>
                <strong>{title}</strong>
                <span><em>Part Number</em>: {partNumber}</span>
                <span><em>Manufacturer</em>: {manufacturer}</span>
                {
                    placement &&
                        <span><em>Placement On Vehicle</em>: {placement}</span>
                }
                <span><em>Note</em>: {note}</span>
                {
                    electronicsHandlingCharge > 0 &&
                        <span>
                            <em>EHC</em>: ${Number(electronicsHandlingCharge).toLocaleString('en', {
                                style: 'decimal',
                                minimumFractionDigits: 2
                            })}
                        </span>
                }
                {
                    coreCost > 0 &&
                        <span>
                            <em>Core</em>: ${Number(coreCost).toLocaleString('en', {
                                style: 'decimal',
                                minimumFractionDigits: 2
                            })}
                        </span>
                }
                <span className={style.price}>
                    ${Number(price.value).toLocaleString('en', {
                        style: 'decimal',
                        minimumFractionDigits: 2
                    })} CAD&nbsp;
                    {
                        price.per <= 1
                            ?   'each'
                            :   `per ${price.per}`
                    }

                    {
                        quantity > 0
                            ?   isRemoteInventory
                                    ?   <em className={style.green}> 1-2 days</em>
                                    :   <em className={style.green}> In stock</em>
                                :   <em className={style.red}> Out of stock</em>
                    }
                </span>
            </p>
            {
                quantity > 0
                    ?   <button
                            onClick={() => addCartItem({
                                partId: WHIProductId,
                                quantity: 1
                            })}
                            className={`${style.addToCart} _button _grey _dark`}
                        >
                            Add to cart
                        </button>
                    :   <button
                            onClick={() => handleAvailabilityRequest({
                                partId: WHIProductId
                            })}
                            className={`${style.addToCart} _button _grey _dark`}
                        >
                            Request Availability
                        </button>
            }
            <Link
                to={link}
                className="_button _grey"
                onClick={() => viewItem({
                    partId: WHIProductId
                })}
            >More Info</Link>
        </div>
    </div>
);

export default PartItem;
