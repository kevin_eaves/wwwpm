import 'whatwg-fetch';
import querystring from 'querystring';
import { browserHistory } from 'react-router';
import serverConfig from '../../config/server';
import slug from 'slug';
import ServerLocation from "react-router-server-location";
import {
    modal
} from './index';

// Action Types
export const POPULATE_YEAR_OPTIONS          = 'POPULATE_YEAR_OPTIONS';
export const RECEIVE_YEAR                   = 'RECEIVE_YEAR';
export const SET_FILTER_MAKE                = 'SET_FILTER_MAKE';
export const SET_FILTER_MODEL               = 'SET_FILTER_MODEL';
export const SET_FILTER_ENGINE              = 'SET_FILTER_ENGINE';
export const RECEIVE_MAKES                  = 'RECEIVE_MAKES';
export const RECEIVE_MODELS                 = 'RECEIVE_MODELS';
export const POPULATE_ENGINE_SELECT_OPTIONS = 'POPULATE_ENGINE_SELECT_OPTIONS';
export const RECEIVE_GROUPS                 = 'RECEIVE_GROUPS';
export const RECEIVE_PARTS                  = 'RECEIVE_PARTS';
export const RECEIVE_PART                   = 'RECEIVE_PART';
export const RECEIVE_ENGINES                = 'RECEIVE_ENGINES';
export const SET_CATEGORY_GROUP             = 'SET_CATEGORY_GROUP';
export const SET_CATEGORY_SUB_GROUP         = 'SET_CATEGORY_SUB_GROUP';
export const RECEIVE_FITMENTS               = 'RECEIVE_FITMENTS';
export const RECEIVE_QUANTITY               = 'RECEIVE_QUANTITY';
export const WIPE_SEARCH_HISTORY            = 'WIPE_SEARCH_HISTORY';
export const CLEAR_PART_SEARCH              = 'CLEAR_PART_SEARCH';
export const RECEIVE_TEMPORARY_FILTERS      = 'RECEIVE_TEMPORARY_FILTERS';
export const PART_VIEW_DETAILS              = 'PART_VIEW_DETAILS';

export const viewPartDetails = ({ partId } = {}) => ({
    type: PART_VIEW_DETAILS,
    partId
});

//BEGINNING OF GROUP ACTIONS
export const fetchGroups = ({ year, makeId, modelId, engineId } = {}) => {
    return (dispatch, getState) => {
        const { filters } = getState().partSearch;
        const query = {};

        if (year) {
            query.year = year;
        } else if (filters.year) {
            query.year = filters.year;
        }
        if (makeId) {
            query.makeId = makeId;
        } else if (filters.make) {
            query.makeId = filters.make;
        }
        if (modelId) {
            query.modelId = modelId;
        } else if (filters.model) {
            query.modelId = filters.model;
        } else {
            return;
        }
        if (engineId) {
            query.engineId = engineId;
        } else if (filters.engine) {
            query.engineId = filters.engine;
        }

        return fetch(`${serverConfig.apiHost}/groups?${querystring.stringify(query)}`)
            .then((response) => response.json())
            .then(groups => {
                dispatch(receiveGroups(groups));
                return Promise.resolve(groups);
            });
    }
}

export const receiveGroups = (value) => ({
    type: RECEIVE_GROUPS,
    value
})
//END OF GROUP ACTIONS

//BEGINNING OF SELECTED YEAR CHANGES
export const handleYearChange = (year, keepSearchValues) => {
    return (dispatch, getState) => {
        if (!keepSearchValues) {
            dispatch(wipeNecessarySearchValues('year'));
        }
        dispatch(receiveYear(year))

        if (typeof year === 'string'  && !year.length) {
            dispatch(receiveGroups());
            return Promise.resolve();
        }
        return dispatch(fetchMakes({ year }))
            .catch((error) => console.warn('ERROR: ', error));

        return Promise.resolve();
    }
};

export const fetchYears = () => {
    return (dispatch, getState) => {
        return fetch(`${serverConfig.apiHost}/years/`)
        .then((response) => response.json())
        .then(response => {
            dispatch(populateYearOptions(response));
            return Promise.resolve(response);
        });
    }
};

export const fetchYearsForVehicle = (makeId, modelId) => {
    return (dispatch, getState) => {
        return fetch(`${serverConfig.apiHost}${makeId ? `/makes/${makeId}` : ''}${modelId ? `/models/${modelId}` : ''}/years`)
        .then((response) => response.json())
        .then(response => {
            dispatch(populateYearOptions(response));
            return Promise.resolve(response);
        });
    }
};

export const populateYearOptions = (value) => ({
    type: POPULATE_YEAR_OPTIONS,
    value
});

export const receiveYear = (value) => ({
    type: RECEIVE_YEAR,
    value
});
//END OF SELECTED YEAR CHANGES

//BEGINNING OF SELECTED MAKE CHANGES
export const handleMakeChange = (makeId, keepSearchValues) => {
    return (dispatch, getState) => {
        if (!keepSearchValues) {
            dispatch(wipeNecessarySearchValues('make'));
        }
        dispatch(setFilterMake(makeId))

        const {
            year
        } = getState().partSearch.filters;

        dispatch(receiveGroups());

        if (!makeId) {
            return Promise.resolve();
        }

        return dispatch(fetchModels({ year, makeId }))
            .catch((error) => console.warn('ERROR: ', error))
    }
};

export const fetchMakes = ({ year } = {}) => {
    return (dispatch, getState) => {
        let endpoint = 'makes';
        if (year) {
            endpoint = `years/${year}/makes`;
        }

        return fetch(`${serverConfig.apiHost}/${endpoint}`)
            .then(response => response.json())
            .then(makes => {
                dispatch(receiveMakes(makes));
                return Promise.resolve(makes);
            });
    }
}

export const setFilterMake = (value) => ({
    type: SET_FILTER_MAKE,
    value
});

export const receiveMakes = (value) => ({
    type: RECEIVE_MAKES,
    value
});
//END OF SELECTED MAKE CHANGES

//BEGINNING OF SELECTED MODEL CHANGES
export const handleModelChange = (model, keepSearchValues) => {
    return (dispatch, getState) => {
        if (!keepSearchValues) {
            dispatch(wipeNecessarySearchValues('model'));
        }

        dispatch(setFilterModel(model));
        dispatch(receiveGroups());

        const {
            year,
            make
        } = getState().partSearch.filters;

        return dispatch(fetchEngines({ year, make, model }));
    }
};

export const fetchModels = ({ makeId, year }) => {
    return (dispatch, getState) => {
        let endpoint = `makes/${makeId}/models`;
        if (year) {
            endpoint = `years/${year}/makes/${makeId}/models`;
        }

        return fetch(`${serverConfig.apiHost}/${endpoint}`)
            .then(response => response.json())
            .then(response => {
                if (response.error) {
                    throw new Error(response.error);
                }
                dispatch(receiveModels(response));

                return Promise.resolve(response);
            })
    };
}

export const setFilterModel = (value) => ({
    type: SET_FILTER_MODEL,
    value
});

export const receiveModels = (value) => ({
    type: RECEIVE_MODELS,
    value
});
//END OF SELECTED MODEL CHANGES

//BEGINNING OF SELECTED ENGINE CHANGES
export const handleEngineChange = (engine, keepSameUrl) => {
    return (dispatch, getState) => {
        dispatch(wipeNecessarySearchValues('engine'));
        dispatch(setFilterEngine(engine));
        if (!keepSameUrl) {
            dispatch(updateSearchUrl());
        }

        dispatch(receiveGroups());

        return Promise.resolve();
    }
};

export const fetchEngines = ({ year, make:makeId, model:modelId } = {}) => {
    return (dispatch, getState) => {
        let endpoint =  querystring.stringify({ year, makeId, modelId });

        return fetch(`${serverConfig.apiHost}/engines?${endpoint}`)
            .then(response => response.json())
            .then(engines => {
                dispatch(receiveEngines(engines));

                return Promise.resolve(engines);
            })
    };
}

export const setFilterEngine = (value) => ({
    type: SET_FILTER_ENGINE,
    value
});

export const receiveEngines = (value) => ({
    type: RECEIVE_ENGINES,
    value
});
//END OF SELECTED ENGINE CHANGES

// START STORE SELECTED FILTER GROUP
export const handleGroupChange = (value) => {
    return (dispatch, getState) => {
        dispatch(setCategoryGroup(value));
        dispatch(updateSearchUrl());
    }
}

export const setCategoryGroup = (value) => ({
    type: SET_CATEGORY_GROUP,
    value
})
// END STORE SELECTED FILTER GROUP

// START STORE SELECTED FILTER GROUP
export const handleSubGroupChange = (value) => {
    return (dispatch, getState) => {
        const parent = getState().categories.available.subGroups[value].WHGroupId;

        dispatch(wipeSearchHistory());
        dispatch(setCategoryGroup(parent));
        dispatch(setCategorySubGroup(value));
        dispatch(updateSearchUrl());
    }
}

export const setCategorySubGroup = (value) => ({
    type: SET_CATEGORY_SUB_GROUP,
    value
})
// END STORE SELECTED FILTER GROUP

// BEGINNING OF POPULATING PART ITEMS FROM FILTER GROUPS
export const fetchPartSearchResults = (id) => {
    return (dispatch, getState) => {
        const year   = getState().partSearch.filters.year;
        const make   = getState().partSearch.filters.make;
        const model  = getState().partSearch.filters.model;
        const engine = getState().partSearch.filters.engine;
        const query = {};

        if (year) {
            query.year = year;
        }
        if (make) {
            query.makeId = make;
        }
        if (model) {
            query.modelId = model;
        }
        if (engine) {
            query.engineId = engine;
        }
        return fetch(`${serverConfig.apiHost}/groups/${id}/products?${querystring.stringify(query)}`)
            .then((response) => response.json())
            .then(parts => {
                dispatch(receiveParts(parts));
                return parts;
            });
    }
}

export const receiveParts = (value) => ({
    type: RECEIVE_PARTS,
    value
})

export const clearPartSearch = (value) => ({
    type: CLEAR_PART_SEARCH,
    value
})
// END OF POPULATING PART ITEMS FROM FILTER GROUPS

// BEGINNING OF FETCHING PART INFO IN NEEDED
export const fetchPartIfNeeded = (WHIProductId, grabFitments) => {
    return (dispatch, getState) => {
        return fetch(`${serverConfig.apiHost}/products/${WHIProductId}`)
            .then((response) => response.json())
            .then(part => {
                if (grabFitments) {
                    dispatch(fetchFitments(WHIProductId));
                }

                return dispatch(receivePart(part));
            });
    }
}

export const receivePart = (value) => ({
    type: RECEIVE_PART,
    value
})
// END OF FETCHING PART INFO IN NEEDED

// BEGINNING OF POPULATING PART ITEMS FROM FILTER GROUPS
export const fetchFitments = (WHIProductId) => {
    return (dispatch, getState) => {
        return fetch(`${serverConfig.apiHost}/products/${WHIProductId}/fitment`)
            .then((response) => response.json())
            .then(fitment => {
                return dispatch(receiveFitments({
                    WHIProductId,
                    fitment
                }));
            });
    }
}

export const receiveFitments = (value) => ({
    type: RECEIVE_FITMENTS,
    value
})
// END OF POPULATING PART ITEMS FROM FILTER GROUPS


export const receiveQuantity = (value) => ({
    type: RECEIVE_QUANTITY,
    value
});

export const wipeSearchHistory = () => ({
    type: WIPE_SEARCH_HISTORY,
    value: []
});

export const updateSearchUrl = () => {
    return (dispatch, getState) => {
        const {
            availableYears,
            availableMakes,
            availableModels,
            availableEngines,
            filters: {
                year,
                make,
                model,
                engine
            }
        } = getState().partSearch;
        const {
            available,
            selected: {
                group,
                subGroup
            }
        } = getState().categories;

        let url = '';
        let vehicle = '';

        if (year) {
            vehicle = vehicle.concat(`${slug(year)}`);
            url   = vehicle;
        }
        if (make) {
            vehicle = vehicle.concat(`${year ? '_' : ''}${slug(availableMakes[make].MakeName)}`);
            url   = vehicle;
        }
        if (model) {
            vehicle = vehicle.concat(`_${slug(availableModels[model].ModelName)}`);
            url   = vehicle;
        }
        if (engine) {
            vehicle = vehicle.concat(`_${slug(availableEngines[engine].EngineDescription)}`);
            url   = vehicle;
        }
        if (group && available.parents[group]) {
            const parentGroup = slug(available.parents[group].GroupName);
            url = `${url}/${parentGroup}`;
        }
        if (subGroup && available.subGroups[subGroup]) {
            const childGroup = slug(available.subGroups[subGroup].PartTypeDescription);
            url = `${url}/${childGroup}`;
        }

        if (engine) {
            browserHistory.push(`/${url}`);
        }

        return Promise.resolve();
    }
}

export const wipeNecessarySearchValues = (changedValue) => {
    return (dispatch, getState) => {
        const {
            year,
            make,
            model,
            engine
        } = getState().partSearch.filters;

        switch (changedValue) {
            case 'nuke':
                dispatch(receiveYear());
                dispatch(setFilterMake());
                dispatch(setFilterModel());
                dispatch(setFilterEngine());
                dispatch(setCategoryGroup());
                dispatch(setCategorySubGroup());
                dispatch(receiveGroups());
                dispatch(wipeSearchHistory());
                break;
            case 'year':
                dispatch(setFilterMake());
                dispatch(setFilterModel());
                dispatch(setFilterEngine());
                dispatch(setCategoryGroup());
                dispatch(setCategorySubGroup());
                dispatch(wipeSearchHistory());
                break;
            case 'make':
                dispatch(setFilterModel());
                dispatch(setFilterEngine());
                dispatch(setCategoryGroup());
                dispatch(setCategorySubGroup());
                dispatch(wipeSearchHistory());
                break;
            case 'model':
                dispatch(setFilterEngine());
                dispatch(setCategoryGroup());
                dispatch(setCategorySubGroup());
                dispatch(wipeSearchHistory());
                break;
            case 'engine':
                dispatch(setCategoryGroup());
                dispatch(setCategorySubGroup());
                dispatch(wipeSearchHistory());
                break;
            case 'group':
                dispatch(storeSelectedFilterSubGroup());
                dispatch(wipeSearchHistory());
                break;
        }
    }
}

export const loadSearchBasedOnUrl = ({ year, make, model, engine, group, subGroup } = {}) => {
    return (dispatch, getState) => {
        if (!year) {
            dispatch(wipeNecessarySearchValues('nuke'));
            dispatch(receiveGroups());
        }

        return dispatch(handleYearChange(parseInt(year, 10), true))
            .then(() => {
                if (make) {
                    const availableMakes = getState().partSearch.availableMakes;
                    const makeId = Object.keys(availableMakes).find((key) => make === slug(availableMakes[key].MakeName));

                    return dispatch(handleMakeChange(makeId, true))
                        .then(() => {
                            const availableModels = getState().partSearch.availableModels;
                            const modelId = Object.keys(availableModels)
                                .find((key) => model === slug(availableModels[key].ModelName));

                            return dispatch(handleModelChange(modelId, true))
                                .then(() => {
                                    if (!engine) {
                                        return Promise.resolve();
                                    }

                                    const availableEngines = getState().partSearch.availableEngines;
                                    const engineId = Object.keys(availableEngines).find((key) => engine === slug(availableEngines[key].EngineDescription));

                                    return dispatch(handleEngineChange(engineId, true))
                                        .then(() => dispatch(fetchGroups()));
                                })
                                .then(() => {
                                    if (!group) {
                                        return Promise.resolve();
                                    }

                                    const {
                                        categories: {
                                            available: {
                                                parents: availableGroups
                                            }
                                        }
                                    } = getState();

                                    const groupId = Object
                                        .keys(availableGroups)
                                        .find(key => group === slug(availableGroups[key].GroupName));

                                    return Promise.resolve(dispatch(setCategoryGroup(groupId)));
                                })
                                .then(() => {
                                    if (!subGroup) {
                                        return Promise.resolve();
                                    }

                                    const {
                                        categories: {
                                            available: {
                                                subGroups: availableSubGroups
                                            }
                                        }
                                    } = getState();

                                    const subGroupId = Object
                                        .keys(availableSubGroups)
                                        .find((key) => subGroup === slug(availableSubGroups[key].PartTypeDescription));

                                    dispatch(setCategorySubGroup(subGroupId));
                                    return dispatch(fetchPartSearchResults(subGroupId))
                                        .then(() => subGroupId);
                                });
                        })
                }
            })

        return Promise.resolve();
    }
}


// BEGINNING OF SEARCHING BASED ON PART NUMBER
export const searchByPartNumber = (partNumber) => {
    return (dispatch, getState) => {
        return fetch(`${serverConfig.apiHost}/products?partNumber=${partNumber}`)
            .then(parts => parts.json())
            .then(parts => {
                if (parts.error) {
                    throw new Error(parts.error);
                }

                dispatch(receiveParts(parts));

                return Promise.resolve(parts);
            });
    }
}
// END OF SEARCHING BASED ON PART NUMBER

// START OF SETTING TEMPORARY FILTER VALUES
export const receiveTemporaryFilter = (value) => ({
    type: RECEIVE_TEMPORARY_FILTERS,
    value
});
// END OF SETTING TEMPORARY FILTER VALUES
