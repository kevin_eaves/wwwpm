import 'whatwg-fetch';
import querystring from 'querystring';
import { browserHistory } from 'react-router';
import serverConfig from '../../config/server';
import slug from 'slug';
import cookie from 'react-cookie';
import {
    mailing,
    modal
} from './index';

export const RECEIVE_USER_DATA         = 'RECEIVE_USER_DATA';
export const RECEIVE_LOGGED_IN_STATUS  = 'RECEIVE_LOGGED_IN_STATUS';
export const RECEIVE_ERRORS            = 'RECEIVE_ERRORS';

export const createUser = ({ email, password, orderId }) => {
    return (dispatch, getState) => {
        return fetch(`${serverConfig.apiHost}/users`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                 email,
                 password,
                 orderId
             })
        })
            .then((response) => response.json())
            .then((response) => {
                if(response.error){
                    dispatch(receiveErrors(response.error));
                    return;
                }
                return dispatch(authenticate({email, password}));
            })
            .catch((error) => {
                console.warn('error:', error);
            })
    }
}

export const authenticate = ({email, password, cartId}) => {
    return (dispatch, getState) => {
        dispatch(mailing.formSubmission({
            name: 'Login'
        }));

        return fetch(`${serverConfig.apiHost}/authenticate`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email,
                password,
                cartId
            })
        })
            .then((response) => response.json())
            .then((response) => {
                if(response.error){
                    dispatch(modal.receiveErrors(response.error));
                    return;
                }

                cookie.save('pm-token', response.token, serverConfig.cookieOptions);
                cookie.save('pm-cartID', response.cart.GUID, serverConfig.cookieOptions);

                location.reload(true);
            })
            .catch((error) => console.warn('ERROR: ', error))
    }
}

export const getUser = (userId) => {
    return (dispatch, getState) => {
        const token = cookie.load('pm-token') || '';
        return fetch(`${serverConfig.apiHost}/users/${userId}`, {
            method: 'GET',
            headers: {
                'Authorization': token
            }
        })
            .then((response) => response.json())
            .then((response) => {
                return Promise.resolve(response.UserId);
            })
            .then((id) => dispatch(getUserAddress(id)))
            .then((id) => {
                dispatch(receiveLoggedInStatus());
                return Promise.resolve();
            })
            .catch((error) => console.warn('ERROR: ', error))
    }
}

export const getUserAddress = (id) => {
    return (dispatch, getState) => {
        const token = cookie.load('pm-token') || '';
        return fetch(`${serverConfig.apiHost}/users/${id}/addresses`, {
            method: 'GET',
            headers: {
                'Authorization': token
            }
        })
            .then((response) => response.json())
            .then((response) => dispatch(receiveUserData(response)))
            .catch((error) => console.warn('ERROR: ', error))
    }
}

export const receiveUserData = (value) => ({
    type: RECEIVE_USER_DATA,
    value
})

export const checkIfLoggedIn = () => {
    return (dispatch, getState) => {
        const isLoggednIn = !!cookie.load('pm-token') ? !!cookie.load('pm-token') : false;
        const userId      = cookie.load('pm-userId');
        dispatch(receiveLoggedInStatus(isLoggednIn));
        if (userId) {
            return dispatch(getUser(userId));
        }

        return Promise.resolve();
    }
}

export const receiveLoggedInStatus = (value) => ({
    type: RECEIVE_LOGGED_IN_STATUS,
    value: value ? value : !!cookie.load('pm-token')
})

export const destroyToken = () => {
    return (dispatch, getState) => {
        cookie.remove('pm-token');
        cookie.remove('pm-cartID');
        dispatch(receiveLoggedInStatus(null));
        location.reload(true);
    }
}

export const receiveErrors = (value) => ({
    type: RECEIVE_ERRORS,
    value
})
