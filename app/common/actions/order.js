import 'whatwg-fetch';
import serverConfig from '../../config/server';

export const RECEIVE_ORDER_INVOICE = 'RECEIVE_ORDER_INVOICE';
export const RECEIVE_PROCESSING_ERROR = 'RECEIVE_PROCESSING_ERROR';

export const fetchInvoice = ({
    orderId
} = {}) => {
    return (dispatch, getState) => {
        return fetch(`${serverConfig.apiHost}/order/${orderId}/invoice`)
            .then(response => response.json())
            .then(response => {
                if (response.error) {
                    throw new Error(response.error);
                }

                return Promise.resolve(dispatch(receiveInvoice(response.message)));
            });
    };
};

export const receiveInvoice = (value) => ({
    type: RECEIVE_ORDER_INVOICE,
    value
});

export const receiveProcessingError = (value) => ({
    type: RECEIVE_PROCESSING_ERROR,
    value
})
