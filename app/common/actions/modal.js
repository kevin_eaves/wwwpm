export const UPDATE_AVAILABLE_ITEM_DETAILS = 'UPDATE_AVAILABLE_ITEM_DETAILS';
export const RESET_MODAL_VALUES = 'RESET_MODAL_VALUES';
export const RECEIVE_ERRORS = 'RECEIVE_ERRORS';
export const RECIEVE_SUCCESS = 'RECIEVE_SUCCESS';
export const RESET_MODAL_NOTIFICATIONS = 'RESET_MODAL_NOTIFICATIONS';

export const updatedAvailableItemDetails = (value) => ({
    type: UPDATE_AVAILABLE_ITEM_DETAILS,
    value
});

export const resetModalValues = () => ({
    type: RESET_MODAL_VALUES,
    value : {}
})

export const receiveErrors = (value) => ({
    type: RECEIVE_ERRORS,
    value
})

export const receiveSuccess = (value) => ({
    type: RECIEVE_SUCCESS,
    value
})

export const resetModalNotifications = () => ({
    type:RESET_MODAL_NOTIFICATIONS,
    value: {}
})
