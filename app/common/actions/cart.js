import 'whatwg-fetch';
import querystring from 'querystring';
import { browserHistory } from 'react-router';
import serverConfig from '../../config/server';
import slug from 'slug';
import cookie from 'react-cookie';
import Promise from 'bluebird';
import {
    partSearch,
    modal,
    order
} from './index';

export const UPDATE_DELIVERY_TYPE          = 'UPDATE_DELIVERY_TYPE';
export const UPDATE_DELIVERY_INSURANCE     = 'UPDATE_DELIVERY_INSURANCE';
export const POPULATE_BILLING_INFO         = 'POPULATE_BILLING_INFO';
export const ADD_PART_TO_CART              = 'ADD_PART_TO_CART';
export const RECEIVE_CART_PARTS            = 'RECEIVE_CART_PARTS';
export const RECEIVE_SHIPPING_OPTIONS      = 'RECEIVE_SHIPPING_OPTIONS';
export const UPDATE_CART_QUANTITY          = 'UPDATE_CART_QUANTITY';
export const RECEIVE_VEHICLE_ID            = 'RECEIVE_VEHICLE_ID';
export const RECEIVE_PARTS_VEHICLE_DETAILS = 'RECEIVE_PARTS_VEHICLE_DETAILS';
export const CLEAR_SHIPPING_OPTIONS        = 'CLEAR_SHIPPING_OPTIONS';
export const LOADING_SHIPPING_OPTIONS      = 'LOADING_SHIPPING_OPTIONS';
export const SET_CHECKOUT_STEP             = 'SET_CHECKOUT_STEP';
export const CHECKOUT_COMPLETE             = 'CHECKOUT_COMPLETE';
export const UPDATE_CART_FREIGHT           = 'UPDATE_CART_FREIGHT';
export const RECEIVE_SHIPPING_ERROR        = 'RECEIVE_SHIPPING_ERROR';

export const checkoutComplete = order => ({
    ...order,
    type: CHECKOUT_COMPLETE
});

export const setCheckoutStep = ({ step }) => ({
    type: SET_CHECKOUT_STEP,
    step
});

export const updateCartFreight = (value) => ({
    type: UPDATE_CART_FREIGHT,
    value
});

export const clearShippingOptions = () => ({
    type:CLEAR_SHIPPING_OPTIONS
})

export const updateDeliveryType = (value) => ({
    type: UPDATE_DELIVERY_TYPE,
    value
});

export const updateDeliveryInsurance = (value) => ({
    type: UPDATE_DELIVERY_INSURANCE,
    value
});

export const populateBillingInfo = (value) => ({
    type: POPULATE_BILLING_INFO,
    value
});

export const updateCartQuantity = ({
    partId,
    quantity,
    freight = 0,
    syncEvent = false
}) => (dispatch, getState) => {
    const {
        parts: {
            items: {
                [partId]: part
            }
        }
    } = getState();

    return dispatch({
        type: UPDATE_CART_QUANTITY,
        partId,
        quantity,
        freight: freight || part.ProductInfo.Inventory.FreightCost,
        syncEvent
    });
};

export const receiveQuantity = (value) => ({
    type: RECEIVE_QUANTITY,
    value
});

export const fetchVehicleId = () => {
    return (dispatch, getState) => {
        const {
            year,
            make,
            model,
            engine
        } = getState().partSearch.filters;

        if(!year || !make || !model || !engine){
            return Promise.resolve();
        }

        return fetch(`${serverConfig.apiHost}/vehicles?makeID=${make}&modelID=${model}&year=${year}&engConfId=${engine}`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
            .then((response) => response.json())
            .then(([ vehicle ] = []) =>  vehicle.WHIVehicleID);
    }
}

export const updatePartQuantity = ({ partId, quantity, vehicleId, merge }) => {
    return (dispatch, getState) => {
        if (merge) {
            const {
                cart: {
                    items: {
                        [partId]: {
                            quantity: mergeQuantity = 0
                        } = {}
                    } = {}
                } = {}
            } = getState();

            quantity += mergeQuantity;
        }

        dispatch(updateCartQuantity({
            partId,
            quantity
        }));

        const {
            subGroup
        } = getState().categories.selected;

        const {
            WHIProductId
        } = getState().parts.items[partId].ProductInfo;

        const requestBody = {
            cartId: cookie.load('pm-cartID'),
            partId: WHIProductId,
            vehicleId,
            quantity
        };

        return fetch(`${serverConfig.apiHost}/carts/parts`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'cache-control': 'no-cache',
                'pragma': 'no-cache'
            },
            body: JSON.stringify(requestBody)
        })
            .then(response => response.json())
            .then(response => {
                if (response.error) {
                    throw response.message;
                }

                return Promise.resolve(response);
            });
    }
}

export const fetchCart = () => {
    return (dispatch, getState) => {
        if (cookie.load("pm-cartID")) {
            return getCart();
        }

        return createCart();

        function createCart () {
            return fetch(`${serverConfig.apiHost}/carts/`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    enabled                 : true,
                    countryCode             : 'CA'
                })
            })
                .then(response => response.json())
                .then(response => {
                    if (response.error) {
                        throw response.message;
                    }
                    cookie.save('pm-cartID', response.GUID, serverConfig.cookieOptions);

                    return response;
                });
        }
        function getCart () {
            const guid   = cookie.load("pm-cartID");
            const query  = { shoppingCartID: guid };

            return fetch(`${serverConfig.apiHost}/carts/?${querystring.stringify(query)}`, {
                method: 'GET',
                headers: {
                    'cache-control': 'no-cache',
                    'pragma': 'no-cache'
                }
            })
                .then(response => response.json())
                .then(response => {
                    if (response.error) {
                        throw response.message;
                    }

                    dispatch(updateCartFreight(Number(response.FreightCost)));

                    return Promise.resolve(response);
                });
        }
    }
};

export const makeTransaction = (paypal) => {
    return (dispatch, getState) =>{

        const {
            form:{
                ProcessCheckoutForm:{
                    values
                }
            },
            user:{
                data:{
                    userId
                }
            },
            cart:{
                shippingType,
                billingSameAsShipping,
                insuranceCost
            },
            promotions: {
                code
            }
        } = getState();

        let query = {
            userId:userId || '',
            cartId: cookie.load('pm-cartID'),
            shippingId: shippingType,
            country:values.country_shipping,
            shippingAddress:{
                firtName: values.firstName_shipping,
                lastName: values.lastName_shipping,
                phone: values.phone_shipping,
                email:values.email_shipping,
                company:values.company_shipping,
                address1:values.address_shipping,
                address2: values.second_address_shipping || '',
                city: values.city_shipping,
                region : values.province_shipping,
                postalCode : values.postalCode_shipping
            },
            returnUrl: serverConfig.baseUrl + '/cart/checkout/process',
            cancelUrl: serverConfig.baseUrl + '/cart/checkout',
            promoCode: code,
            purchaseOrderNumber: values.purchaseOrderNumber,
            shippingInsurance: !!insuranceCost && !!values.deliveryInsurance
        }

        if(values.paymentTypes == 'credit'){
            query.cardNumber = values.cardNumber;
            query.expiry = values.month.toString() + values.year.toString().slice(-2);
            query.cvv = values.cvv;
        }
        if(values.sameAsShippingAddress){
            query.billingAddress = query.shippingAddress;
        }else{
            query.billingAddress = {
                firtName: values.firstName_billing,
                lastName: values.lastName_billing,
                phone: values.phone_billing,
                email:values.email_billing,
                company:values.company_billing,
                address1:values.address_billing,
                address2: values.second_address_billing || '',
                city: values.city_billing,
                region : values.province_billing,
                postalCode : values.postalCode_billing
            };
        }

        return paypal ? payWithPaypal(query) : payWithCredit(query);

    }
}

const payWithCredit = (query) =>{
    return fetch(`${serverConfig.apiHost}/order`, {
            headers:{
                'Content-Type': 'application/json',
                'cache-control': 'no-cache',
                'pragma': 'no-cache'
            },
            method:'POST',
            body:JSON.stringify(query)
        })
        .then(response => response.json())
        .then(response => {
            if(response.error){
                throw response.error;
            }

            return response;
        });
};

const payWithPaypal = (query) =>{
    return fetch(`${serverConfig.apiHost}/order/paypal`, {
            headers:{
                'Content-Type': 'application/json',
                'cache-control': 'no-cache',
                'pragma': 'no-cache'
            },
            method:'POST',
            body: JSON.stringify(query)
        })
        .then(response => response.json())
        .then(response => {
            if (response.error) {
                throw response.error;
            }

            const [
                paymentDetailsLink,
                approvalLink,
                executeLink
            ] = response.links;

            return approvalLink;
        });
};

export const finalizePaypalPayment = (values) =>{
    return (dispatch, getState) =>{
        const query = {
            paymentId:values.paymentId,
            payerId: values.PayerID,
            token: values.token
        };

        return fetch(`${serverConfig.apiHost}/order/paypal/accept`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'cache-control': 'no-cache',
                'pragma': 'no-cache'
            },
            body: JSON.stringify(query)
        })
        .then(response => response.json())
    }
}

export const fetchCartParts = () => {
    return (dispatch, getState) => {
        return dispatch(fetchCart())
            .then(cartData => {
                const guidID = cookie.load("pm-cartID") || null;
                return fetch(`${serverConfig.apiHost}/carts/parts?shoppingCartID=${guidID}`, {
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'cache-control': 'no-cache',
                        'pragma': 'no-cache'
                    }
                })
                    .then(response => response.json())
                    .then(response => {
                        if (response.error) {
                            throw response.message;
                        }

                        const partIds = response.ShoppingCartParts.map((part) => part.ProductInfo.WHIProductId);
                        return {partIds, parts: response.ShoppingCartParts};
                    })
                    .then(response => {
                        response.parts.map((item) => {
                            dispatch(partSearch.receivePart(item));

                            if(item.Vehicle){
                                dispatch(receivePartsVehicleDetails({
                                    id: item.ProductInfo.WHIProductId,
                                    vehicle: item.Vehicle
                                }));
                            }
                        });

                        return response.parts;
                    })
                    .then(parts => Promise.mapSeries(
                        parts.map(part => dispatch(updateCartQuantity({
                            partId: part.ProductInfo.WHIProductId,
                            quantity: part.Quantity,
                            freight: part.NonCanusaFreight,
                            syncEvent: true
                        }))),
                        part => part
                    ));
            });
    }
};

export const recieveShippingOptions = (value = []) => ({
    type: RECEIVE_SHIPPING_OPTIONS,
    value
});

export const recieveShippingError = (value = []) => ({
    type: RECEIVE_SHIPPING_ERROR,
    value
});

export const fetchShippingOptions = ({
    postalCode,
    city,
    region,
    country
} = {}) => {
    return (dispatch, getState) => {
        if (!city || !region || !country || !postalCode) {
            return Promise.resolve();
        }

        dispatch({
            type: LOADING_SHIPPING_OPTIONS
        });

        const shippingQuery = querystring.stringify({
            cartId: cookie.load('pm-cartID'),
            postalCode,
            city,
            region,
            country
        });

        return fetch(`${serverConfig.apiHost}/shipping?${shippingQuery}`, {
            headers: {
                'cache-control': 'no-cache',
                'pragma': 'no-cache'
            }
        })
            .then(response => response.json())
            .then(({ insuranceCost, options, error, message }) => {
                if (error) {
                    dispatch(clearShippingOptions());
                    dispatch(recieveShippingError(true));

                    throw 'Invalid address provided';
                }

                dispatch(recieveShippingOptions(options));
                dispatch(updateDeliveryInsurance(insuranceCost));

                return options;
            });
    }
}

export const addPartWithQuote = (code) => {
    return (dispatch, getState) => {
        return fetch(`${serverConfig.apiHost}/carts/parts`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                cartId: cookie.load('pm-cartID'),
                code
            })
        })
            .then(response => response.json())
            .then(response => {
                if (response.error) {
                    throw new Error(response.error);
                }
                return Promise.resolve(response);
            })
    }
}

export const submitOrder = () => {
    return (dispatch, getState) => {
        console.log('Run fetch to submit order!');
        // make api request
            // if no error resolve Promise
            // if error reject Promise
        return Promise.resolve({
            orderId: 1234
        });
    }
}

export const receivePartsVehicleDetails = (value) => ({
    type: RECEIVE_PARTS_VEHICLE_DETAILS,
    value
})
