import 'whatwg-fetch';
import querystring from 'querystring';
import { browserHistory } from 'react-router';
import serverConfig from '../../config/server';
import slug from 'slug';
import ServerLocation from "react-router-server-location";

export const RECEIVE_ERROR = 'RECEIVE_ERROR';
export const RECEIVE_PROMO = 'RECEIVE_PROMO';

export const updateError = (value) => ({
    type: RECEIVE_ERROR,
    value
})

export const updatePromo = (value) => ({
    type: RECEIVE_PROMO,
    value
})

// START PROMO CODE Action
export const applyPromoCode = (code) => {
    return (dispatch, getState) => {
        return fetch(`${serverConfig.apiHost}/promo/${code}`)
            .then(response => response.json())
            .then(response => {
                if (response.error) {
                    return dispatch(updateError(response.error));
                }
                dispatch(updatePromo(response));

                return Promise.resolve(response);
            });
    }
}
// END PROMO CODE ACTION
