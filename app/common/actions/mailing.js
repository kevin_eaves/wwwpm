import serverConfig from '../../config/server';

export const FORM_SUBMISSION = 'FORM_SUBMISSION';

export const formSubmission = ({ name }) => ({
    type: FORM_SUBMISSION,
    name
});

export const sendVehicleAvailabilityRequest = (values) => {
    return (dispatch, getState) => {
        console.log("Send Vehicle Availability Request", values);
        return Promise.resolve();
    }
}

export const submitRequestForQuote = ({
    partId,
    vehicleId,
    name,
    email,
    phone,
    notes
} = {}) => (dispatch, getState) => {
    const {
        parts: {
            items: {
                [partId]: part = {}
            } = {}
        } = {}
    } = getState();

    dispatch(formSubmission({
        name: `Request for Availability - ${part.ProductInfo.PartLabel}`
    }));

    return fetch(`${serverConfig.apiHost}/products/request`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            partId,
            vehicleId,
            name,
            email,
            phone,
            notes
        })
    })
        .then(response => response.json())
        .then(response => {
            if (response.error) {
                throw new Error(response.error);
            }

            return response;
        });
};

export const sendContactForm = ({
    email,
    name,
    notes,
    phone = ''
}) => (dispatch, getState) => {
    dispatch(formSubmission({
        name: 'Contact Form'
    }));

    return fetch(`${serverConfig.apiHost}/contact`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            email,
            name,
            notes,
            phone
        })
    })
        .then(response => response.json());
};
