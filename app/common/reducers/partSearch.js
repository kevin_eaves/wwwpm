import extend from 'extend';
import {
    POPULATE_YEAR_OPTIONS,
    SET_FILTER_MAKE,
    SET_FILTER_MODEL,
    RECEIVE_YEAR,
    SET_FILTER_ENGINE,
    RECEIVE_MAKES,
    RECEIVE_MODELS,
    RECEIVE_ENGINES,
    RECEIVE_TEMPORARY_FILTERS
} from '../actions/partSearch';

const initialState = {
    availableYears: [],
    availableMakes: {},
    availableModels: {},
    availableEngines: {},
    filters: {
        year: null,
        make: null,
        model: null,
        engine: null,
        tempValues: {
            year: null,
            make: null,
            model: null,
            engine: null
        }
    }
};

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case POPULATE_YEAR_OPTIONS:
            return {
                ...state,
                availableYears: action.value
            }
        case RECEIVE_YEAR: {
            return {
                ...state,
                filters: {
                    ...state.filters,
                    year: action.value
                }
            }
        }
        case SET_FILTER_MAKE: {
            return {
                ...state,
                filters: {
                    ...state.filters,
                    make: action.value
                }
            }
        }
        case SET_FILTER_MODEL: {
            return {
                ...state,
                filters: {
                    ...state.filters,
                    model: action.value
                }
            }
        }
        case SET_FILTER_ENGINE: {
            return {
                ...state,
                filters: {
                    ...state.filters,
                    engine: action.value
                }
            }
        }
        case RECEIVE_TEMPORARY_FILTERS:
            return {
                ...state,
                filters: {
                    ...state.filters,
                    tempValues: {
                        ...state.filters.tempValues,
                        ...action.value
                    }
                }
            }
        case RECEIVE_MAKES:
            const makes = Object.keys(action.value)
                .reduce((makeList, part) => ({
                    ...makeList,
                    [action.value[part].MakeId]: action.value[part]
                }), {});

            return {
                ...state,
                availableMakes: makes
            };
        case RECEIVE_MODELS:
            const models = Object.keys(action.value)
                .reduce((makeList, part) => ({
                    ...makeList,
                    [action.value[part].ModelID]: action.value[part]
                }), {});

            return {
                ...state,
                availableModels: models
            };
        case RECEIVE_ENGINES:
            const engines = Object.keys(action.value)
                .reduce((makeList, part) => ({
                    ...makeList,
                    [action.value[part].EngineConfigId]: action.value[part]
                }), {});

            return {
                ...state,
                availableEngines: engines
            }
        default:
            return {
                ...initialState,
                ...state
            };
    }
};


// helpers
function keysrt(key,desc) {
    return function(a,b){
        return desc ? ~~(a[key] < b[key]) : ~~(a[key] > b[key]);
    }
}
