import extend from 'extend';
import {
    UPDATE_AVAILABLE_ITEM_DETAILS,
    RESET_MODAL_VALUES,
    RECEIVE_ERRORS,
    RECIEVE_SUCCESS,
    RESET_MODAL_NOTIFICATIONS
} from '../actions/modal';

const initialState = {
    body: {},
    error: undefined,
    success: undefined
};

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case UPDATE_AVAILABLE_ITEM_DETAILS:
            return {
                ...state,
                body: action.value
            }
        case RESET_MODAL_VALUES:
            return {
                ...state,
                body: action.value
            }
        case RESET_MODAL_NOTIFICATIONS:
            return{
                ...state,
                error:undefined,
                success:undefined
            }
        case RECEIVE_ERRORS:
            return {
                ...state,
                error: action.value
            }
        case RECIEVE_SUCCESS:
            return {
                ...state,
                success:action.value
            }
        default:
            return {
                ...initialState,
                ...state
            };
    }
};
