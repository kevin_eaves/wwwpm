import extend from 'extend';
import {
    RECEIVE_USER_DATA,
    RECEIVE_LOGGED_IN_STATUS,
    RECEIVE_ERRORS
} from '../actions/user';

const initialState = {
    data: {},
    isLoggedIn: false,
    error: undefined
}

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case RECEIVE_USER_DATA:
            return {
                ...state,
                data: action.value
            }
        case RECEIVE_LOGGED_IN_STATUS:
            return {
                ...state,
                isLoggedIn: action.value
            }
        case RECEIVE_ERRORS:
            return {
                ...state,
                error: action.value
            }
        default:
            return {
                ...initialState,
                ...state
            };
    }
};
