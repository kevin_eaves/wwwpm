import extend from 'extend';
import {
    RECEIVE_GROUPS,
    SET_CATEGORY_GROUP,
    SET_CATEGORY_SUB_GROUP,
    WIPE_SEARCH_HISTORY
} from '../actions/partSearch';

const initialState = {
    available: {},
    selected: {
        group: 0,
        subGroup: 0
    }
}

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case RECEIVE_GROUPS:
            if (!action.value) {
                return {
                    ...state,
                    available: {}
                }
            }
            let available = { parents: {}, subGroups: {} };
            available.parents = action.value.parents
                .reduce((groups, item) => ({
                    ...groups,
                    [item.WHGroupId]: item
                }), {});
            available.subGroups = action.value.subGroups
                .reduce((groups, item) => ({
                    ...groups,
                    [item.PartTypeNumber]: item
                }), {});

            return {
                ...state,
                available
            }
        case SET_CATEGORY_GROUP:
            return {
                ...state,
                selected: {
                    ...state.selected,
                    group: action.value,
                    subGroup: 0
                }
            }
        case SET_CATEGORY_SUB_GROUP:
            return {
                ...state,
                selected: {
                    ...state.selected,
                    subGroup: action.value
                }
            }
        case WIPE_SEARCH_HISTORY:
            return {
                ...state,
                selected: {
                    group: 0,
                    subGroup: 0
                }
            }
        default:
            return {
                ...initialState,
                ...state
            };
    }
};
