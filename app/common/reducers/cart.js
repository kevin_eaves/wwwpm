import extend from 'extend';
import {
    UPDATE_CART_QUANTITY,
    UPDATE_DELIVERY_TYPE,
    UPDATE_DELIVERY_INSURANCE,
    POPULATE_BILLING_INFO,
    RECEIVE_CART_PARTS,
    UPDATE_CART_FREIGHT,
    RECEIVE_SHIPPING_OPTIONS,
    CLEAR_SHIPPING_OPTIONS,
    LOADING_SHIPPING_OPTIONS,
    RECEIVE_SHIPPING_ERROR
} from '../actions/cart';

const initialState = {
    billingSameAsShipping: true,
    items: {},
    tax: 0,
    total: 0,
    freight: 0,
    shippingType: null,
    insuranceCost: 0,
    shippingError:false,
    shippingLoading: false,
    availableShippingTypes: {}
};

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case LOADING_SHIPPING_OPTIONS:
            return {
                ...state,
                shippingLoading: true
            };
        case UPDATE_CART_QUANTITY:
            const defaultFreight = state[partId] ? state[partId].freight : 0;
            const {
                partId,
                freight = defaultFreight,
                quantity = 0
            } = action;

            if (quantity <= 0) {
                const {
                    [partId.toString()]: ignore,
                    ...items
                } = state.items;

                return {
                    ...state,
                    items
                };
            }

            return {
                ...state,
                items: {
                    ...state.items,
                    [partId]: {
                        ...state[partId],
                        quantity,
                        freight
                    }
                }
            };
        case RECEIVE_CART_PARTS:
            return {
                ...state,
                items: action.value
            }
        case UPDATE_DELIVERY_TYPE:
            return {
                ...state,
                shippingType: parseInt(action.value, 10)
            }
        case UPDATE_DELIVERY_INSURANCE:
            return {
                ...state,
                insuranceCost: parseFloat(action.value, 10)
            }
        case UPDATE_CART_FREIGHT:
            return {
                ...state,
                freight: parseInt(action.value, 10)
            }
        case POPULATE_BILLING_INFO:
            return {
                ...state,
                billingSameAsShipping: action.value
            }
        case CLEAR_SHIPPING_OPTIONS:
            return{
                ...state,
                shippingLoading: false,
                shippingType: null,
                availableShippingTypes:{}
            }
        case RECEIVE_SHIPPING_OPTIONS:
            if (typeof action.value === 'string' || action.value instanceof String) {
                return {
                    ...state,
                    shippingLoading: false,
                    availableShippingTypes: action.value
                };
            }
            const shippingOptions = action.value
                .reduce((cache, option) => ({
                    ...cache,
                    [option.shippingOptionId]: option
                }), {});
            return {
                ...state,
                shippingLoading: false,
                shippingError: false,
                availableShippingTypes: {
                    ...shippingOptions
                }
            };
        case RECEIVE_SHIPPING_ERROR:
            return {
                ...state,
                shippingError: action.value
            }
        default:
            return {
                ...initialState,
                ...state
            };
    }
};
