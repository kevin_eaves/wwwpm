import {
    RECEIVE_ORDER_INVOICE,
    RECEIVE_PROCESSING_ERROR
} from '../actions/order';

const initialState = {
    errorMessage: "",
    invoice: {},
    countries: {
        1: { value: 'CA', label: 'Canada', icon: 'flag-o' },
        2: { value: 'US', label: 'United States', icon: 'flag-o' }
    },
    regions: {
        1: { value: 'AB', label: 'Alberta', icon: 'globe', country: 1 },
        2: { value: 'BC', label: 'British Columbia', icon: 'globe', country: 1 },
        3: { value: 'MB', label: 'Manitoba', icon: 'globe', country: 1 },
        4: { value: 'NB', label: 'New Brunswick', icon: 'globe', country: 1 },
        5: { value: 'NL', label: 'Newfoundland and Labrador', icon: 'globe', country: 1 },
        6: { value: 'NT', label: 'Northwest Territories', icon: 'globe', country: 1 },
        7: { value: 'NS', label: 'Nova Scotia', icon: 'globe', country: 1 },
        8: { value: 'NU', label: 'Nunavut', icon: 'globe', country: 1 },
        9: { value: 'ON', label: 'Ontario', icon: 'globe', country: 1 },
        10: { value: 'PE', label: 'Prince Edward Island', icon: 'globe', country: 1 },
        11: { value: 'QC', label: 'Quebec', icon: 'globe', country: 1 },
        12: { value: 'SK', label: 'Saskatchewan', icon: 'globe', country: 1 },
        13: { value: 'YT', label: 'Yukon Territory', icon: 'globe', country: 1 },
        14: { value: 'AL', label: 'Alabama', icon: 'globe', country: 2 },
        15: { value: 'AK', label: 'Alaska', icon: 'globe', country: 2 },
        16: { value: 'AZ', label: 'Arizona', icon: 'globe', country: 2 },
        17: { value: 'AR', label: 'Arkansas', icon: 'globe', country: 2 },
        18: { value: 'CA', label: 'California', icon: 'globe', country: 2 },
        19: { value: 'CO', label: 'Colorado', icon: 'globe', country: 2 },
        20: { value: 'CT', label: 'Connecticut', icon: 'globe', country: 2 },
        21: { value: 'DE', label: 'Delaware', icon: 'globe', country: 2 },
        22: { value: 'DC', label: 'District of Columbia', icon: 'globe', country: 2 },
        23: { value: 'FL', label: 'Florida', icon: 'globe', country: 2 },
        24: { value: 'GA', label: 'Georgia', icon: 'globe', country: 2 },
        25: { value: 'HI', label: 'Hawaii', icon: 'globe', country: 2 },
        26: { value: 'ID', label: 'Idaho', icon: 'globe', country: 2 },
        27: { value: 'IL', label: 'Illinois', icon: 'globe', country: 2 },
        28: { value: 'IN', label: 'Indiana', icon: 'globe', country: 2 },
        29: { value: 'IA', label: 'Iowa', icon: 'globe', country: 2 },
        30: { value: 'KS', label: 'Kansas', icon: 'globe', country: 2 },
        31: { value: 'KY', label: 'Kentucky', icon: 'globe', country: 2 },
        32: { value: 'LA', label: 'Louisiana', icon: 'globe', country: 2 },
        33: { value: 'ME', label: 'Maine', icon: 'globe', country: 2 },
        34: { value: 'MD', label: 'Maryland', icon: 'globe', country: 2 },
        35: { value: 'MA', label: 'Massachusetts', icon: 'globe', country: 2 },
        36: { value: 'MI', label: 'Michigan', icon: 'globe', country: 2 },
        37: { value: 'MN', label: 'Minnesota', icon: 'globe', country: 2 },
        38: { value: 'MS', label: 'Mississippi', icon: 'globe', country: 2 },
        39: { value: 'MO', label: 'Missouri', icon: 'globe', country: 2 },
        40: { value: 'MT', label: 'Montana', icon: 'globe', country: 2 },
        41: { value: 'NE', label: 'Nebraska', icon: 'globe', country: 2 },
        42: { value: 'NV', label: 'Nevada', icon: 'globe', country: 2 },
        43: { value: 'NH', label: 'New Hampshire', icon: 'globe', country: 2 },
        44: { value: 'NJ', label: 'New Jersey', icon: 'globe', country: 2 },
        45: { value: 'NM', label: 'New Mexico', icon: 'globe', country: 2 },
        46: { value: 'NY', label: 'New York', icon: 'globe', country: 2 },
        47: { value: 'NC', label: 'North Carolina', icon: 'globe', country: 2 },
        48: { value: 'ND', label: 'North Dakota', icon: 'globe', country: 2 },
        49: { value: 'OH', label: 'Ohio', icon: 'globe', country: 2 },
        50: { value: 'OK', label: 'Oklahoma', icon: 'globe', country: 2 },
        51: { value: 'OR', label: 'Oregon', icon: 'globe', country: 2 },
        52: { value: 'PA', label: 'Pennsylvania', icon: 'globe', country: 2 },
        53: { value: 'RI', label: 'Rhode Island', icon: 'globe', country: 2 },
        54: { value: 'SC', label: 'South Carolina', icon: 'globe', country: 2 },
        55: { value: 'SD', label: 'South Dakota', icon: 'globe', country: 2 },
        56: { value: 'TN', label: 'Tennessee', icon: 'globe', country: 2 },
        57: { value: 'TX', label: 'Texas', icon: 'globe', country: 2 },
        58: { value: 'UT', label: 'Utah', icon: 'globe', country: 2 },
        59: { value: 'VT', label: 'Vermont', icon: 'globe', country: 2 },
        60: { value: 'VA', label: 'Virginia', icon: 'globe', country: 2 },
        61: { value: 'WA', label: 'Washington', icon: 'globe', country: 2 },
        62: { value: 'WV', label: 'West Virginia', icon: 'globe', country: 2 },
        63: { value: 'WI', label: 'Wisconsin', icon: 'globe', country: 2 },
        64: { value: 'WY', label: 'Wyoming', icon: 'globe', country: 2 }
    }
};

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case RECEIVE_ORDER_INVOICE:
            return {
                ...state,
                invoice: action.value
            };
        case RECEIVE_PROCESSING_ERROR:
            return {
                ...state,
                errorMessage: action.value
            }
        default:
            return state;
    }
}
