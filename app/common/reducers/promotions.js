import extend from 'extend';
import {
    RECEIVE_ERROR,
    RECEIVE_PROMO
} from '../actions/promotions';

const initialState = {
    error: null,
    code: '',
    discount: 0
}

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case RECEIVE_ERROR:
            return {
                ...state,
                code: null,
                discount: 0,
                error: action.value
            }
        case RECEIVE_PROMO:
            return {
                ...state,
                code: action.value.PromoCode,
                discount:  action.value.Discount || 0,
                error: null
            }
        default:
            return {
                ...initialState,
                ...state
            };
    }
};
