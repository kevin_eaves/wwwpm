import extend from 'extend';
import {
    RECEIVE_PARTS,
    RECEIVE_PART,
    RECEIVE_QUANTITY,
    RECEIVE_FITMENTS,
    WIPE_SEARCH_HISTORY,
    CLEAR_PART_SEARCH
} from '../actions/partSearch';
import {
    RECEIVE_PARTS_VEHICLE_DETAILS
} from '../actions/cart';
import naturalSort from 'javascript-natural-sort';

const initialState = {
    items: {},
    searchedIds: []
};

function cmp(a, b) {
    if (a > b) return +1;
    if (a < b) return -1;
    return 0;
}

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case RECEIVE_PARTS:
            const parts = Object.keys(action.value)
                .reduce((orderList, part) => ({
                    ...orderList,
                    [action.value[part].ProductInfo.WHIProductId]: action.value[part]
                }), {});

            const available = action.value.filter((item)=>{
                return item.ProductInfo.Inventory.QuantityOnHand > 0;
            })
            .sort(function(a , b){
                var partnumberA = a.ProductInfo.PartNumber
                var partnumberB = b.ProductInfo.PartNumber

                return naturalSort(partnumberA, partnumberB);
            })
            .map((item) =>  item.ProductInfo.WHIProductId);

            const unavailable = action.value.filter((item)=>{
                return item.ProductInfo.Inventory.QuantityOnHand <= 0;
            })
            .sort(function(a , b){
                var partnumberA = a.ProductInfo.PartNumber
                var partnumberB = b.ProductInfo.PartNumber

                return naturalSort(partnumberA, partnumberB);
            })
            .map((item) =>  item.ProductInfo.WHIProductId);

            const partIds = [
                ...available,
                ...unavailable
            ];

            return {
                ...state,
                items: parts,
                searchedIds: partIds
            };
        case RECEIVE_PART:
            return {
                ...state,
                items: {
                    ...state.items,
                    [action.value.ProductInfo.WHIProductId]: {
                        ...state.items[action.value.ProductInfo.WHIProductId],
                        ProductInfo: action.value.ProductInfo
                    }
                }
            }
        case RECEIVE_PARTS_VEHICLE_DETAILS:
            return {
                ...state,
                items: {
                    ...state.items,
                    [action.value.id]: {
                        ...state.items[action.value.id],
                        vehicle: action.value.vehicle
                    }
                }
            }
        case CLEAR_PART_SEARCH:
            return {
                ...state,
                searchedIds: []
            }
        case RECEIVE_FITMENTS:

            const fitments = action.value.fitment.sort((a, b)=>{
                return cmp( b.Year, a.Year ) || cmp( a.MakeName, b.MakeName ) || cmp(a.ModelName, b.ModelName);
            })

            return {
                ...state,
                items: {
                    ...state.items,
                    [action.value.WHIProductId]: {
                        ...state.items[action.value.WHIProductId],
                        fitments
                    }
                }
            }
        case RECEIVE_QUANTITY:
            return {
                ...state,
                items: {
                    ...state.items,
                    [action.value.WHIProductId]: {
                        ...state.items[action.value.WHIProductId],
                        quantity: action.value.quantity
                    }
                }
            }
        case WIPE_SEARCH_HISTORY:
            return {
                ...state,
                searchedIds: action.value
            }
        default:
            return {
                ...initialState,
                ...state
            };
    }
};
