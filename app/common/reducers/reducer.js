import extend from 'extend';

const initialState = {};

export default (state = initialState, action = {}) => {
    switch (action.type) {

        default:
            return {
                ...initialState,
                ...state
            };
    }
}
