import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import { routerReducer } from 'react-router-redux';
import partSearch from './partSearch';
import parts from './parts';
import cart from './cart';
import modal from './modal';
import categories from './categories';
import user from './user';
import order from './order';
import promotions from './promotions';

export default combineReducers({
    partSearch,
    parts,
    form: formReducer,
    modal,
    cart,
    categories,
    user,
    order,
    promotions,
    routing: routerReducer
});
