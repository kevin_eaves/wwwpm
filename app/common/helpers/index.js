export const sortByAttribute = (attribute, direction = 'ASC') => {
    return (a, b) => {
        if (a[attribute] < b[attribute]){
            return direction === 'ASC'
                ?   -1
                :   1;
        }
        if (a[attribute] > b[attribute]){
            return direction === 'ASC'
                ?   1
                :   -1;
        }
        return 0;
    };
}

export const getParameterByName = (name, url) => {
    if(typeof window == 'undefined'){
        return;
    }
    if(!url){
        url = window.location.href;
    };
    name          = name.replace(/[\[\]]/g, "\\$&");
    const regex   = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)");
    const results = regex.exec(url);
    if(!results){
        return null;
    };
    if(!results[2]){
        return '';
    };
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}