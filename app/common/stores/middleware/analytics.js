import {
    LOCATION_CHANGE
} from 'react-router-redux';
import {
    CHECKOUT_COMPLETE,
    SET_CHECKOUT_STEP,
    UPDATE_CART_QUANTITY
} from '../../actions/cart';
import {
    PART_VIEW_DETAILS,
    RECEIVE_PARTS
} from '../../actions/partSearch';
import {
    FORM_SUBMISSION
} from '../../actions/mailing';

export default ({ getState }) => next => action => {
    if (typeof window === 'undefined' || action.syncEvent) {
        return next(action);
    }

    window.dataLayer = window.dataLayer || [];

    switch (action.type) {
        case FORM_SUBMISSION:
            const {
                name: formName
            } = action;

            dataLayer.push({
                event: 'formSubmission',
                eventAction: formName,
                eventLabel: window.location.pathname
            });

            break;
        case CHECKOUT_COMPLETE:
            const {
                cart: {
                    items: purchasedCartItems
                } = {},
                parts: {
                    items: purchasedParts
                } = {}
            } = getState();

            const purchasedProducts = Object.keys(purchasedParts)
                .filter(id => Object.keys(purchasedCartItems).indexOf(id) > -1)
                .map(id => purchasedParts[id]);

            dataLayer.push({
                event: 'purchase',
                ecommerce: {
                    purchase: {
                        actionField: {
                            id: action.OrderId,
                            revenue: action.Total,
                            tax: 0,
                            shipping: 0
                        },
                        products: purchasedProducts.map(product => ({
                            id: product.ProductInfo.WHIProductId,
                            name: product.ProductInfo.PartLabel,
                            price: product.ProductInfo.Inventory.Cost,
                            brand: product.ProductInfo.ManuFacturerName
                        }))
                    }
                }
            });

            break;
        case SET_CHECKOUT_STEP:
            const {
                step
            } = action;
            const {
                cart: {
                    items: cartItems
                } = {},
                parts: {
                    items: parts
                } = {}
            } = getState();

            const products = Object.keys(parts)
                .filter(id => Object.keys(cartItems).indexOf(id) > -1)
                .map(id => parts[id]);

            dataLayer.push({
                event: 'checkout',
                ecommerce: {
                    checkout: {
                        actionField: {
                            step
                        },
                        products: products.map(product => ({
                            id: product.ProductInfo.WHIProductId,
                            name: product.ProductInfo.PartLabel,
                            price: product.ProductInfo.Inventory.Cost,
                            brand: product.ProductInfo.ManuFacturerName
                        }))
                    }
                }
            });

            break;
        case LOCATION_CHANGE:
            const {
                payload: {
                    pathname
                } = {}
            } = action;

            if (pathname.indexOf('/car-part') !== 0) {
                break;
            }

            const partIdFromLocation = pathname.substring(pathname.lastIndexOf('-') + 1);
            const {
                parts: {
                    items: {
                        [partIdFromLocation]: locationPart = {}
                    } = {}
                } = {}
            } = getState();

            dataLayer.push({
                ecommerce: {
                    detail: {
                        products: [{
                            id: locationPart.ProductInfo.WHIProductId,
                            name: locationPart.ProductInfo.PartLabel,
                            price: locationPart.ProductInfo.Inventory.Cost,
                            brand: locationPart.ProductInfo.ManuFacturerName
                        }]
                    }
                }
            });

            break;
        case PART_VIEW_DETAILS:
            const {
                partId: detailsId
            } = action;
            const {
                parts: {
                    items: {
                        [detailsId]: detailsPart = {}
                    } = {}
                } = {}
            } = getState();

            dataLayer.push({
                event: 'productClick',
                products: [{
                    id: detailsId,
                    name: detailsPart.ProductInfo.PartLabel,
                    price: detailsPart.ProductInfo.Inventory.Cost,
                    brand: detailsPart.ProductInfo.ManufacturerName
                }]
            });

            break;
        case UPDATE_CART_QUANTITY:
            const {
                partId,
                quantity
            } = action;
            const {
                parts: {
                    items: {
                        [partId]: part = {}
                    } = {}
                } = {},
                cart: {
                    items: {
                        [partId]: {
                            quantity: previousQuantity = 0
                        } = {}
                    } = {}
                } = {}
            } = getState();

            if (quantity === previousQuantity) {
                break;
            }

            const quantityIncreased = quantity > previousQuantity;

            dataLayer.push({
                event: quantityIncreased ? 'addToCart' : 'removeFromCart',
                ecommerce: {
                    currency: 'CAD',
                    [quantityIncreased ? 'add' : 'remove']: {
                        products: [{
                            id: partId,
                            name: part.ProductInfo.PartLabel,
                            price: part.ProductInfo.Inventory.Cost,
                            brand: part.ProductInfo.ManufacturerName,
                            quantity: quantityIncreased
                                ?   quantity - previousQuantity
                                :   previousQuantity - quantity
                        }]
                    }
                }
            });

            break;
        case RECEIVE_PARTS:
            const {
                value: categoryParts = []
            } = action;
            const {
                categories: {
                    selected: {
                        subGroup
                    } = {}
                } = {}
            } = getState();

            if (!categoryParts) {
                break;
            }

            dataLayer.push({
                event: 'productImpression',
                ecommerce: {
                    currency: 'CAD',
                    impressions: categoryParts.map(product => ({
                        id: product.ProductInfo.WHIProductId,
                        name: product.ProductInfo.PartLabel,
                        price: product.ProductInfo.Inventory.Cost,
                        brand: product.ProductInfo.ManuFacturerName
                    }))
                }
            });

            break;
        default:
            // Do nothing
    }

    return next(action);
};
