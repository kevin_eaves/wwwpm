import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import analyticsMiddleware from './middleware/analytics';

import rootReducer from '../reducers';

export default (initialState) => createStore(
    rootReducer,
    initialState,
    compose(
        applyMiddleware(thunkMiddleware),
        applyMiddleware(analyticsMiddleware),
        typeof window !== 'undefined' && window.devToolsExtension ? window.devToolsExtension() : f => f
    )
);
