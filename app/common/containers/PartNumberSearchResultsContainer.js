import { connect } from 'react-redux';
import PartNumberSearchResults from '../components/PartNumberSearchResults';

const mapStateToProps = ({ parts }, { location }) => ({
    parts: parts.searchedIds.map((id) => parts.items[id]),
    title: `Results based on part number ${location.query.partNumber}`
});

const mapDispatchToProps = (dispatch, {}) => ({})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PartNumberSearchResults);
