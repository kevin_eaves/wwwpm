import React from 'react';
import { connect } from 'react-redux';
import { isSubmitting } from 'redux-form';
import { browserHistory } from 'react-router';
import CartUpdated from '../components/CartUpdated';

import {
    modal
} from '../actions';

const mapDispatchToProps = (dispatch) => ({
    dismissModal () {
        dispatch(modal.resetModalValues());
    },
    viewCart () {
        dispatch(modal.resetModalValues());
        browserHistory.push('/cart');
    }
});

export default connect(
    null,
    mapDispatchToProps
)(CartUpdated);
