import { connect } from 'react-redux';
import CheckoutSummary from '../components/_forms/CheckoutSummary';

const mapStateToProps = ({ parts, cart }, ownProps) => {
    const orderParts = ownProps.parts.reduce((cache, part) => {
        cache.push(part);

        if (part.ProductInfo.Inventory.CoreCost > 0) {
            cache.push({
                quantity: part.quantity,
                ProductInfo: {
                    PartLabel: 'Core',
                    PartNumber: part.ProductInfo.PartNumber,
                    Inventory: {
                        Cost: part.ProductInfo.Inventory.CoreCost
                    }
                },
                vehicle: {}
            });
        }

        return cache;
    }, []);

    return {
        parts: orderParts
    };
};

export default connect(
    mapStateToProps
)(CheckoutSummary);
