import React from 'react';
import { connect } from 'react-redux';
import BillingFieldset from '../components/_forms/BillingFieldset';

const mapStateToProps = ({
    form: {
        ProcessCheckoutForm: {
            values = {}
        }
    },
    order: {
        countries,
        regions
    }
}, ownProps) => ({
    ...ownProps,
    countriesOptions: Object.keys(countries)
        .map(id => countries[id]),
    regionOptions: Object.keys(regions)
        .map(id => regions[id])
        .filter(region => {
            if (!values.country_billing) {
                return false;
            }

            const selectedCountry = Object.keys(countries)
                .find(id => countries[id].value === values.country_billing);

            return region.country == selectedCountry
        })
});

export default connect(
    mapStateToProps
)(BillingFieldset);
