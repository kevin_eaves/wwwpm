import React from 'react';
import { connect } from 'react-redux';
import slug from 'slug';
import ResultsList from '../components/ResultsList';
import {
    partSearch,
    modal
} from '../actions';

class BrowseYearEngine extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
            isFetching: true
        };
    }

    static preloadData ({ params, store } = {}) {
        return store.dispatch(partSearch.fetchMakes())
            .then(makes => makes.find(make => slug(make.MakeName) == params.make))
            .then(({ MakeId }) => {
                store.dispatch(partSearch.receiveTemporaryFilter({make: MakeId}));
                return MakeId;
            })
            .then((MakeId) => {
                return store.dispatch(partSearch.fetchModels({
                    makeId: MakeId
                }))
                    .then((results) => ({
                        results,
                        MakeId
                    }));

            })
            .then(({ MakeId, results }) => {
                const modelId = results.find((item) => item.ModelName === params.model).ModelID;

                return store.dispatch(partSearch.fetchEngines({
                    make: MakeId,
                    model: modelId
                }))
            });
    }

    componentWillMount () {
        const {
            params,
            modelId
        } = this.props;

        const {
            store: {
                dispatch
            }
        } = this.context;

        return dispatch(partSearch.fetchMakes())
            .then(makes => makes.find(make => slug(make.MakeName) == params.make))
            .then(({ MakeId }) => {
                dispatch(partSearch.receiveTemporaryFilter({make: MakeId}));
                return MakeId;
            })
            .then((MakeId) => {
                dispatch(partSearch.fetchModels({
                    makeId: MakeId
                }));

                return MakeId;
            })
            .then((MakeId) => dispatch(partSearch.fetchEngines({
                make: MakeId,
                model: modelId
            })))
            .then(() => this.setState({
                isFetching: false
            }));
    }

    shouldComponentUpdate (props, state) {
        return this.state.isFetching !== state.isFetching;
    }

    render () {
        const {
            store: {
                dispatch
            }
        } = this.context;
        const {
            makeId
        } = this.props;

        return (
            <ResultsList
                {...this.props}
                isFetching={this.state.isFetching}
                extraColumns={true}
            />
        );
    }
}

BrowseYearEngine.contextTypes = {
    store: React.PropTypes.object.isRequired
};

const mapStateToProps = ({ partSearch }, { params }) => {
    const makeId = Object.keys(partSearch.availableMakes)
        .find(id => slug(partSearch.availableMakes[id].MakeName) == params.make);
    const modelId = Object.keys(partSearch.availableModels)
        .find(id => slug(partSearch.availableModels[id].ModelName) == params.model);
    const makeName = partSearch.availableMakes[makeId] && partSearch.availableMakes[makeId].MakeName;
    const modelName = partSearch.availableModels[modelId] && partSearch.availableModels[modelId].ModelName;

    const results = Object.keys(partSearch.availableEngines)
        .map(id => ({
            id: partSearch.availableEngines[id].EngineConfigId,
            name: partSearch.availableEngines[id].EngineDescription,
            year: partSearch.availableEngines[id].Year,
            url: `/${slug(partSearch.availableEngines[id].Year)}_${slug(makeName)}_${slug(modelName)}_${slug(partSearch.availableEngines[id].EngineDescription)}`
        }))
        .sort((a, b) => {
            if (a.name > b.name) {
                return 1;
            }

            if (a.name < b.name) {
                return -1;
            }

            return 0;
        })
        .reduce((cache, item) => {
            return {
                ...cache,
                [item.year]: [
                    ...cache[item.year] || [],
                    item
                ]
            }
        }, {});

    return {
        makeId,
        modelId,
        title: `${makeName}/${modelName}`,
        instructionalText: 'Choose a Vehicle Engine',
        reverse: true,
        results
    };
};

export default connect(
    mapStateToProps
)(BrowseYearEngine);
