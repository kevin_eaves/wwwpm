import React from 'react';
import { connect } from 'react-redux';
import { submit } from 'redux-form';
import Cart from '../components/Cart';
import {
    cart,
    modal,
    order
} from '../actions';

class CartContainer extends React.Component {
    static preloadData ({ store } = {}) {
        return store.dispatch(cart.fetchCartParts());
    }

    render () {
        return (
            <Cart
                title={this.props.title}
                pathname={this.props.pathname}
                openLoginModal={this.props.openLoginModal}
                remoteSubmit={this.props.remoteSubmit}
                paymentTypes={this.props.paymentTypes}
            />
        );
    }
}

CartContainer.contextTypes = {
    store: React.PropTypes.object.isRequired
};

const mapStateToProps = (state, ownProps) => {
    const pathname      = ownProps.location.pathname,
          urlFragment   = pathname.split('/'),
          titleLc       = urlFragment[urlFragment.length - 1],
          title         = titleLc.charAt(0).toUpperCase() + titleLc.slice(1);

    const {
      ProcessCheckoutForm: {
          values: {
              paymentTypes
          } = {}
      } = {}
  } = state.form;

    return {
        title,
        pathname,
        paymentTypes
    };
}

const mapDispatchToProps = (dispatch) => ({
    openLoginModal () {
        dispatch(modal.updatedAvailableItemDetails({modalType: "LoginModal"}));
    },
    remoteSubmit () {
        dispatch(order.receiveProcessingError(''));
        dispatch(submit('ProcessCheckoutForm'));
    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CartContainer);
