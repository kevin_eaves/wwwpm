import React from 'react';
import { connect } from 'react-redux';
import QuantityPicker from '../components/QuantityPicker';
import {
    cart,
    modal
} from '../actions';

const mapStateToProps = ({ parts: { items }, cart }, { partId }) => {
    const {
        [partId]: {
            ProductInfo: {
                Inventory: {
                    QuantityOnHand = 0
                } = {}
            } = {}
        } = {}
    } = items;
    const {
        items: {
            [partId]: {
                quantity = 1
            }
        }
    } = cart;

    return {
        max: QuantityOnHand,
        value: quantity
    };
};

const mapDispatchToProps = (dispatch, { partId }) => ({
    adjustQuantity (value) {
        const quantity = parseInt(value, 10);
        dispatch(cart.clearShippingOptions());
        dispatch(cart.updatePartQuantity({
            partId,
            quantity
        }))
            .then(() => {
                if (quantity === 0) {
                    return Promise.resolve();
                }

                return dispatch(modal.updatedAvailableItemDetails({
                    modalType: 'CartUpdated'
                }));
            })
            .then(() => dispatch(cart.fetchCart()));
    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(QuantityPicker);
