import React from 'react';
import { connect } from 'react-redux';
import Temp from '../components/Temp';

const mapStateToProps = ({ reducer }) => ({
    reducer:reducer
})

const mapDispatchToProps = (dispatch) => ({  })

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Temp);
