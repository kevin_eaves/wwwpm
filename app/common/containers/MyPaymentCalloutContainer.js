import React from 'react';
import { connect } from 'react-redux';
import {  } from '../actions';
import MyPaymentCallout from '../components/MyPaymentCallout';

const mapStateToProps = ({}, ownProps) => ({
    orderNumber: ownProps.location.query.orderNumber
});

const mapDispatchToProps = (dispatch) => ({});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MyPaymentCallout);
