import React from 'react';
import { connect } from 'react-redux';
import App from '../components/App';

const mapStateToProps = ({ reducer }) => ({
    reducer:reducer
})

const mapDispatchToProps = (dispatch) => ({  })

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App);
