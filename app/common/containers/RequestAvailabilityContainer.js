import React from 'react';
import { connect } from 'react-redux';
import {
    SubmissionError
} from 'redux-form';
import RequestAvailability from '../components/_forms/RequestAvailability';
import {
    cart,
    mailing,
    modal
} from '../actions';

const mapStateToProps = ({ modal, parts }) => {
    const {
        body: {
            partId
        } = {}
    } = modal;
    const {
        items: {
            [partId]: {
                ProductInfo = {}
            } = {}
        } = {}
    } = parts;

    return {
        partId,
        partTitle: ProductInfo.PartLabel,
        partNumber: ProductInfo.PartNumber,
        manufacturer: ProductInfo.ManuFacturerName
    };
};

const mapDispatchToProps = (dispatch) => ({
    submitRequestForQuote ({
        partId,
        name,
        email,
        phone,
        notes
    }) {
        return dispatch(cart.fetchVehicleId())
            .then(vehicleId => dispatch(mailing.submitRequestForQuote({
                partId,
                vehicleId,
                name,
                email,
                phone,
                notes
            })))
            .then(() => dispatch(modal.resetModalValues()))
            .catch(({ message }) => {
                throw new SubmissionError({
                    _error: message
                });
            });
    }
});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onSubmit ({
        name,
        email,
        phone,
        notes
    } = {}) {
        return dispatchProps.submitRequestForQuote({
            name,
            email,
            phone,
            notes,
            partId: stateProps.partId
        });
    }
})

export default connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
)(RequestAvailability);
