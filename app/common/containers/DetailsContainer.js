import React from 'react';
import { connect } from 'react-redux';
import PartDetails from '../components/Details';
import {
    partSearch
} from '../actions';

class DetailsContainer extends React.Component {
    static preloadData ({ params, store } = {}) {
        return store.dispatch(partSearch.fetchPartIfNeeded(params.WHIProductId, true));
    }

    componentDidMount () {
        const {
            params
        } = this.props;

        const {
            store: {
                dispatch
            }
        } = this.context;

        dispatch(partSearch.fetchPartIfNeeded(params.WHIProductId, true));
    }

    render() {
        const {
            part,
            group,
            images,
            wipeSearchHistory,
            populateUrl
        } = this.props;

        return (
            <PartDetails
                part={part}
                group={group}
                images={images}
                wipeSearchHistory={wipeSearchHistory}
                populateUrl={populateUrl}
            />
        );
    }
}

DetailsContainer.contextTypes = {
    store: React.PropTypes.object.isRequired
};

const mapStateToProps = ({ reducer, parts, categories }, { params }) => {
    const part = parts.items[params.WHIProductId];

    if(part){
        const group  = categories.selected;
        const images = part.ProductInfo.Images
        ?   part.ProductInfo.Images.split(';')
        :   undefined;

        return {
            part,
            group,
            images
        }
    }
};

const mapDispatchToProps = (dispatch) => ({
    populateUrl () {
        dispatch(partSearch.updateSearchUrl());
    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DetailsContainer);
