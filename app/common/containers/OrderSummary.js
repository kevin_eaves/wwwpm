import React from 'react';
import { connect } from 'react-redux';
import { browserHistory, router } from 'react-router';
import OrderSummary from '../components/OrderSummary';
import taxData from '../helpers/canadianSalesTax';
import decimal from 'decimal.js';

import {
    cart,
    order
} from '../actions';

const mapStateToProps = ({ parts, cart, form, promotions, order }) => {
    let taxOption = { tax: 0.00 };
    const {
        shippingType,
        availableShippingTypes: {
            [cart.shippingType]: {
                cost: shipping = 0
            } = {}
        } = {}
    } = cart;
    const {
        ProcessCheckoutForm: {
            values: {
                province_shipping,
                sameAsShippingAddress,
                deliveryInsurance,
                paymentTypes
            } = {}
        } = {},
        remoteSubmit: {
            triggerSubmit
        } = {}
    } = form;

    let province = 'ON';
    if (province_shipping) {
        province = province_shipping;

        if (taxData[province]) {
            taxOption = taxData[province].taxes
                .find(({ code }) => code === 'GST' || code === 'HST')
        }
    }

    const orderData = Object.keys(cart.items)
        .reduce((data, id) => {
            const {
                items: {
                    [id]: {
                        ProductInfo: {
                            Inventory = {}
                        } = {}
                    } = {}
                } = {}
            } = parts;

            const {
                items: {
                    [id]: cartData
                } = {}
            } = cart;

            const coreCost = new decimal(Inventory.CoreCost)
                .toFixed(2);

            const subWithoutCore = new decimal(data.subWithoutCore)
                .plus(
                    new decimal(Inventory.Cost)
                        .times(cartData.quantity)
                )
                .toFixed(2);

            const sub = new decimal(data.subTotal)
                .plus(
                    new decimal(Inventory.Cost)
                        .plus(data.coreCost)
                        .times(cartData.quantity)
                );

            return {
                totalPartsInOrder: data.totalPartsInOrder += cartData.quantity,
                subTotal: sub.toFixed(2),
                ehc: data.ehc += Inventory.EHC * cartData.quantity,
                freight: cart.freight,
                coreCost: data.coreCost
                    .plus(
                        new decimal(Inventory.CoreCost)
                            .times(cartData.quantity)
                    ),
                subWithoutCore
            };
        }, {
            totalPartsInOrder: 0,
            ehc: 0,
            subTotal: 0,
            coreCost: new decimal(0),
            freight: 0,
            subWithoutCore: 0
        });

        // calculate the order totals
        const taxCode = taxOption.code;
        const shippingInsurance = deliveryInsurance
            ?   new decimal(cart.insuranceCost)
                    .toFixed(2)
            :   0;
        const discountPrice = new decimal(orderData.subTotal)
            .times(promotions.discount)
            .toFixed(2);
        const taxPercent = new decimal(taxOption.tax).toFixed(2);
        const modifiedSubtotal = new decimal(orderData.subTotal)
            .minus(discountPrice)
            .plus(shipping)
            .plus(shippingInsurance)
            .toFixed(2);
        const tax = new decimal(orderData.subWithoutCore)
            .plus(orderData.ehc)
            .plus(orderData.freight)
            .plus(orderData.coreCost)
            .plus(shipping)
            .plus(shippingInsurance)
            .minus(discountPrice)
            .times(taxPercent)
            .toFixed(2);
        const total = new decimal(orderData.subWithoutCore)
            .plus(orderData.ehc)
            .plus(orderData.freight)
            .plus(orderData.coreCost)
            .plus(shipping)
            .plus(shippingInsurance)
            .minus(discountPrice)
            .plus(tax)
            .toFixed(2);

    return {
        parts: Object.keys(cart.items)
            .map(partId => {
                const {
                    items: {
                        [partId]: part
                    }
                } = parts;
                const {
                    items: {
                        [partId]: {
                            quantity = 1
                        }
                    }
                } = cart;

                return {
                    ...part,
                    quantity
                };
            }),
        fees: {
            taxCode,
            taxPercent: taxPercent * 100,
            subTotal: orderData.subTotal,
            subWithoutCore: orderData.subWithoutCore,
            modifiedSubtotal,
            shipping: shipping ? Number(shipping) : 0,
            tax,
            ehc: orderData.ehc,
            freight: orderData.freight,
            core: orderData.coreCost,
            discountPercent: (promotions.discount * 100),
            discount: discountPrice,
            insuranceCost: shippingInsurance,
            total
        },
        paymentTypes,
        shippingSelected: !!shippingType,
        isSubmitting: triggerSubmit && !order.errorMessage ? true : false
    }
};



const mapDispatchToProps = (dispatch) => ({
    handleSubmit (value) {
        if (value == "paypal") {
            return dispatch(cart.makeTransaction(true))
                .then(approvalLink => {
                    window.location.href = approvalLink.href;
                })
                .catch(err => browserHistory
                    .push(`/cart?error=${err}`)
                );
        }

        return dispatch(cart.makeTransaction(false))
            .then(details => {
                dispatch(cart.checkoutComplete(details));
                dispatch(order.receiveProcessingError(""));
                browserHistory
                    .push(`/cart/checkout/success?orderNumber=${details.OrderId}`);
            })
            .catch(err => dispatch(order.receiveProcessingError(err)));
    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(OrderSummary);
