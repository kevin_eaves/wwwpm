import React from 'react';
import { connect } from 'react-redux';
import PartItem from '../components/PartItem';
import {
    modal,
    cart,
    partSearch
} from '../actions';

const mapStateToProps = ({ reducer, partResults }) => ({});

const mapDispatchToProps = (dispatch) => ({
    handleAvailabilityRequest ({ partId }) {
        dispatch(modal.updatedAvailableItemDetails({
            modalType: 'RequestAvailabilityModal',
            partId
        }));
    },
    addCartItem ({ partId, quantity }) {
        dispatch(modal.updatedAvailableItemDetails({
            modalType: 'CartUpdated'
        }))
        dispatch(cart.fetchVehicleId())
            .then((vehicleId) => dispatch(cart.updatePartQuantity({
                partId,
                quantity,
                vehicleId
            })))
            .then(() => dispatch(cart.fetchCartParts()));
    },
    viewItem ({ partId }) {
        return dispatch(partSearch.viewPartDetails({
            partId
        }));
    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PartItem);
