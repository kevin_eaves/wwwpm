import React from 'react';
import { connect } from 'react-redux';
import DefaultTemplate from '../components/_templates/DefaultTemplate';
import {
    modal,
    cart,
    partSearch,
    user
} from '../actions';

class DefaultTemplateContainer extends React.Component {
    static preloadData ({ store, params = {} } = {}) {
        return store.dispatch(partSearch.fetchYears())
            .then(() => store.dispatch(partSearch.fetchMakes()))
            .then(() => store.dispatch(cart.fetchCartParts()));
    }

    componentDidMount () {
        const {
            store: {
                dispatch
            }
        } = this.context;

        dispatch(partSearch.fetchYears())
            .then(() => dispatch(partSearch.fetchMakes()))
            .then(() => dispatch(cart.fetchCartParts()));
    }

    render() {
        const {
            cartCount,
            handleModalRequest,
            children,
            handleLogout
        } = this.props;

        return (
            <DefaultTemplate
                cartCount={cartCount}
                handleModalRequest={handleModalRequest}
                handleLogout={handleLogout}
            >
                {children}
            </DefaultTemplate>
        );
    }
}

DefaultTemplateContainer.contextTypes = {
    store: React.PropTypes.object.isRequired
};

const mapStateToProps = ({ cart }) => ({
    cartCount: Object.keys(cart.items)
        .reduce((total, partId) => total += parseInt(cart.items[partId].quantity, 10), 0)
});

const mapDispatchToProps = (dispatch) => ({
    handleModalRequest (value) {
        dispatch(modal.updatedAvailableItemDetails(value));
    },
    handleLogout () {
        dispatch(user.destroyToken());
    }
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DefaultTemplateContainer);
