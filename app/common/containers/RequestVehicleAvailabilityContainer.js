import React from 'react';
import { connect } from 'react-redux';
import RequestAvailability from '../components/_forms/RequestVehicleAvailability';
import {
    mailing,
    modal
} from '../actions';

const mapStateToProps = ({ modal }) => ({
    car: modal.body
});

const mapDispatchToProps = (dispatch) => ({
    onSubmit (values) {
        dispatch(mailing.sendVehicleAvailabilityRequest(values))
            .then(() => dispatch(modal.resetModalValues()));
    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RequestAvailability);
