import React from 'react';
import { connect } from 'react-redux';
import slug from 'slug';
import ResultsList from '../components/ResultsList';
import {
    partSearch,
    modal
} from '../actions';

class BrowseModels extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
            isFetching: true
        };
    }

    static preloadData ({ params, store } = {}) {
        return store.dispatch(partSearch.fetchMakes())
            .then(makes => makes.find(make => slug(make.MakeName) == params.make))
            .then(({ MakeId }) => {
                store.dispatch(partSearch.receiveTemporaryFilter({make: MakeId}));
                return MakeId;
            })
            .then((MakeId) => store.dispatch(partSearch.fetchModels({
                makeId: MakeId
            })));
    }

    componentWillMount () {
        const {
            params
        } = this.props;

        const {
            store: {
                dispatch
            }
        } = this.context;

        return dispatch(partSearch.fetchMakes())
            .then(makes => makes.find(make => slug(make.MakeName) == params.make))
            .then(({ MakeId }) => {
                dispatch(partSearch.receiveTemporaryFilter({make: MakeId}));
                return MakeId;
            })
            .then((MakeId) => dispatch(partSearch.fetchModels({
                makeId: MakeId
            })))
            .then(() => this.setState({
                isFetching: false
            }));
    }

    shouldComponentUpdate (props, state) {
        if (props.location.pathname != this.props.location.pathname) {
            const {
                params
            } = props;

            const {
                store: {
                    dispatch
                }
            } = this.context;

            this.state.isFetching !== state.isFetching;
            return dispatch(partSearch.fetchMakes())
                .then(makes => makes.find(make => slug(make.MakeName) == params.make))
                .then(({ MakeId }) => {
                    dispatch(partSearch.receiveTemporaryFilter({make: MakeId}));
                    return MakeId;
                })
                .then((MakeId) => dispatch(partSearch.fetchModels({
                    makeId: MakeId
                })))
                .then(() => this.setState({
                    isFetching: false
                }));
        }

        return this.state.isFetching !== state.isFetching;
    }

    render () {
        const {
            store: {
                dispatch
            }
        } = this.context;
        const {
            makeId
        } = this.props;

        return (
            <ResultsList
                {...this.props}
                isFetching={this.state.isFetching}
            />
        );
    }
}

BrowseModels.contextTypes = {
    store: React.PropTypes.object.isRequired
};

const mapStateToProps = ({ partSearch }, { params }) => {
    const makeId = Object.keys(partSearch.availableMakes)
        .find(id => slug(partSearch.availableMakes[id].MakeName) == params.make);

    return {
        makeId,
        title: partSearch.availableMakes[makeId].MakeName + ` Parts`,
        description: partSearch.availableMakes[makeId].description,
        instructionalText: 'Select a model',
        results: Object.keys(partSearch.availableModels)
            .map(id => ({
                id: partSearch.availableModels[id].ModelID,
                name: partSearch.availableModels[id].ModelName,
                url: `/vehicles/${slug(partSearch.availableMakes[makeId].MakeName)}/${slug(partSearch.availableModels[id].ModelName)}`
            }))
            .sort((a, b) => {
                if (a.name > b.name) {
                    return 1;
                }

                if (a.name < b.name) {
                    return -1;
                }

                return 0;
            })
            .reduce((cache, item) => {
                const firstLetter = item.name.charAt(0).toUpperCase();

                return {
                    ...cache,
                    [firstLetter]: [
                        ...cache[firstLetter] || [],
                        item
                    ]
                }
            }, {})
    };
};

export default connect(
    mapStateToProps
)(BrowseModels);
