import React from 'react';
import { browserHistory, router } from 'react-router';
import { connect } from 'react-redux';
import CartPreview from '../components/CartPreview';
import decimal from 'decimal.js';
import {
    cart,
    modal
} from '../actions';

const mapStateToProps = ({ parts, cart }) => {
    const orderData = Object.keys(cart.items)
        .reduce((data, id) => {
            const {
                items: {
                    [id]: {
                        ProductInfo: {
                            Inventory: {
                                Cost = 0,
                                CoreCost = 0,
                                EHC = 0
                            } = {}
                        } = {}
                    } = {}
                } = {}
            } = parts;
            const {
                freight,
                items: {
                    [id]: cartData
                } = {}
            } = cart;

            const sub = new decimal(data.subTotal)
                .plus(
                    new decimal(Cost)
                        .plus(CoreCost)
                        .plus(EHC)
                        .times(cartData.quantity)
                );

            return {
                totalPartsInOrder: data.totalPartsInOrder += cartData.quantity,
                subTotal: sub.toFixed(2),
                ehc: data.ehc += EHC,
                freight
            };
        }, {
            totalPartsInOrder: 0,
            ehc: 0,
            freight: 0,
            core: 0,
            subTotal: 0
        });

        orderData.subTotal = new decimal(orderData.subTotal).plus(orderData.freight);

    return {
        totalPartsInOrder: orderData.totalPartsInOrder > 0
            ?   `${orderData.totalPartsInOrder} item(s)`
            :   'No Items',
        parts: Object.keys(cart.items)
            .map(partId => {
                const {
                    items: {
                        [partId]: part
                    }
                } = parts;
                const {
                    items: {
                        [partId]: {
                            quantity = 1
                        }
                    }
                } = cart;

                return {
                    ...part,
                    quantity
                };
            }),
        subTotal: orderData.subTotal
    }
};

const mapDispatchToProps = (dispatch) => ({
    openLoginModal () {
        dispatch(modal.updatedAvailableItemDetails({modalType: "LoginModal"}));
    },
    openQuoteCodeModal () {
        dispatch(modal.updatedAvailableItemDetails({modalType: "QuoteCodeModal"}));
    }
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CartPreview);
