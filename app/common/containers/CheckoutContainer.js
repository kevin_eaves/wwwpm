import React from 'react';
import { browserHistory, router } from 'react-router';
import { connect } from 'react-redux';
import Checkout from '../components/_forms/ProcessCheckout';
import taxData from '../helpers/canadianSalesTax';
import decimal from 'decimal.js';

import {
    cart,
    partSearch,
    modal,
    order
} from '../actions';
let total = 0;

class CheckoutContainer extends React.Component {

    render() {
        const {
            parts,
            availableShippingTypes,
            shippingType,
            formDetails,
            billingSameAsShipping,
            fees,
            handleUpdatingDeliveryType,
            pathname,
            handleShippingRequest,
            handlePopulateBillingAddress,
            handlePaypalCheckout,
            onSubmit,
            totalPartsInOrder,
            clearShippingOptions,
            promoCode,
            recalculateShippingOptions,
            setCheckoutStep,
            shippingError,
            errorMessage,
            clearOrderProcessingError,
            shippingValues,
            paymentTypes,
            remoteSubmit,
            setIsSubmitting,
            previousSubmitValue,
            scrollToErrors
        } = this.props;

        return (
            <Checkout
                remoteSubmit={remoteSubmit}
                paymentTypes={paymentTypes}
                parts={parts}
                availableShippingTypes={availableShippingTypes}
                shippingType={shippingType}
                formDetails={formDetails}
                billingSameAsShipping={billingSameAsShipping}
                fees={fees}
                pathname={pathname}
                handlePaypalCheckout={handlePaypalCheckout}
                handleUpdatingDeliveryType={handleUpdatingDeliveryType}
                handleShippingRequest={handleShippingRequest}
                handlePopulateBillingAddress={handlePopulateBillingAddress}
                clearShippingOptions={clearShippingOptions}
                onSubmit={onSubmit}
                totalPartsInOrder={totalPartsInOrder}
                promoCode={promoCode}
                recalculateShippingOptions={recalculateShippingOptions}
                setCheckoutStep={setCheckoutStep}
                shippingError={shippingError}
                errorMessage={errorMessage}
                clearOrderProcessingError={clearOrderProcessingError}
                shippingValues={shippingValues}
                setIsSubmitting={setIsSubmitting}
                previousSubmitValue={previousSubmitValue}
                scrollToErrors={scrollToErrors}
            />
        );
    }
}

CheckoutContainer.contextTypes = {
    store: React.PropTypes.object.isRequired
};

const mapStateToProps = ({ cart, parts, form, promotions, order }) => {
    let taxOption = { tax: 0.00 };

    const {
        ProcessCheckoutForm: {
            values: {
                province_shipping,
                sameAsShippingAddress = true,
                deliveryInsurance,
                paymentTypes
            } = {}
        } = {}
    } = form;

    const {
        availableShippingTypes: {
            [cart.shippingType]: {
                cost: shipping = 0
            } = {}
        } = {}
    } = cart;

    let province = 'ON';
    if (province_shipping) {
        province = province_shipping;

        if (taxData[province]) {
            taxOption = taxData[province].taxes
                .find(({ code }) => code === 'GST' || code === 'HST')
        }
    }

    const shippingType = cart.availableShippingTypes[cart.shippingType] &&
        cart.availableShippingTypes[cart.shippingType].label || '';

    let shippingValues = {};
    if (form.ProcessCheckoutForm) {
        const oldSuffix = '_shipping';
        const newSuffix = '_billing';

        let registeredFields = form.ProcessCheckoutForm.registeredFields;
        if (form.ProcessCheckoutForm.registeredFields.constructor != Array) {
            registeredFields = Object.values(form.ProcessCheckoutForm.registeredFields);
        }

        Object.keys(form.ProcessCheckoutForm.values).forEach(field => {
            const prefix = field.substring(0, field.length - oldSuffix.length);
            if (form.ProcessCheckoutForm.fields) {
                if (!form.ProcessCheckoutForm.fields[`${prefix}${newSuffix}`]) {
                    form.ProcessCheckoutForm.fields[`${prefix}${newSuffix}`] = {
                        visited: false
                    };
                }

                if (sameAsShippingAddress === true) {
                    form.ProcessCheckoutForm.values[`${prefix}${newSuffix}`] = form.ProcessCheckoutForm.values[field];
                } else {
                    if (registeredFields.find(field => field.name == `${prefix}${newSuffix}`) && form.ProcessCheckoutForm.fields[`${prefix}${newSuffix}`].visited == false) {
                        if (
                            `${prefix}${newSuffix}` == 'country_billing' ||
                            `${prefix}${newSuffix}` == 'province_billing'
                        ) {
                            if (form.ProcessCheckoutForm.fields[`${prefix}${newSuffix}`].touched != true) {
                                form.ProcessCheckoutForm.values[`${prefix}${newSuffix}`] = form.ProcessCheckoutForm.values[field];
                                form.ProcessCheckoutForm.fields[`${prefix}${newSuffix}`].touched = true;
                            }
                        }
                    }
                }
            }
            if (form.ProcessCheckoutForm.values[field]) {
                shippingValues[field] = form.ProcessCheckoutForm.values[field];
            }
        })
    }

    const orderData = Object.keys(cart.items)
        .reduce((data, id) => {
            const {
                items: {
                    [id]: {
                        ProductInfo: {
                            Inventory = {}
                        } = {}
                    } = {}
                } = {}
            } = parts;
            const {
                items: {
                    [id]: cartData
                } = {}
            } = cart;

            const sub = new decimal(data.subTotal)
                .plus(
                    new decimal(Inventory.Cost)
                        .plus(Inventory.CoreCost)
                        .times(cartData.quantity)
                );

            return {
                totalPartsInOrder: data.totalPartsInOrder += cartData.quantity,
                subTotal: sub.toFixed(2),
                ehc: data.ehc += Inventory.EHC,
                freight: cart.freight
            };
        }, {
            totalPartsInOrder: 0,
            ehc: 0,
            freight: 0,
            subTotal: 0
        });

    const shippingInsurance = deliveryInsurance
        ?   new decimal(cart.insuranceCost)
                .toFixed(2)
        :   0

    const taxPercent = new decimal(taxOption.tax).toFixed(2);

    const taxCode = taxOption.code;

    const discountPrice = new decimal(orderData.subTotal)
        .times(promotions.discount)
        .toFixed(2);

    const tax = new decimal(orderData.subTotal)
        .plus(orderData.ehc)
        .plus(orderData.freight)
        .plus(shipping)
        .plus(shippingInsurance)
        .minus(discountPrice)
        .times(taxPercent)
        .toFixed(2);

    const modifiedSubtotal = new decimal(orderData.subTotal)
        .minus(discountPrice)
        .plus(shipping)
        .plus(shippingInsurance)
        .toFixed(2);

    const total = new decimal(orderData.subTotal)
        .minus(discountPrice)
        .plus(orderData.freight)
        .plus(orderData.ehc)
        .plus(shipping)
        .plus(shippingInsurance)
        .plus(tax)
        .toFixed(2);

    return {
        totalPartsInOrder: orderData.totalPartsInOrder,
        parts: Object.keys(cart.items)
            .map(partId => {
                const {
                    items: {
                        [partId]: part
                    }
                } = parts;
                const {
                    items: {
                        [partId]: {
                            quantity = 1
                        }
                    }
                } = cart;

                return {
                    ...part,
                    quantity
                };
            }),
        availableShippingTypes: cart.availableShippingTypes,
        shippingType,
        formDetails: form.ProcessCheckoutForm,
        billingSameAsShipping: cart.billingSameAsShipping,
        promoCode: promotions.code,
        shippingError: cart.shippingError,
        fees: {
            taxCode,
            taxPercent: taxPercent * 100,
            subTotal: orderData.subTotal,
            modifiedSubtotal,
            shipping: shipping ? Number(shipping) : 0,
            tax,
            ehc: orderData.ehc,
            freight: orderData.freight,
            discount: discountPrice,
            insuranceCost: shippingInsurance,
            total
        },
        errorMessage: order.errorMessage,
        shippingValues,
        paymentTypes
    }
};

const mapDispatchToProps = (dispatch) => ({
    clearShippingOptions(){
        return dispatch(cart.clearShippingOptions());
    },
    makeTransaction (parts){
        return dispatch(cart.makeTransaction(parts))
    },
    handlePopulateBillingAddress (checked) {
        dispatch(cart.populateBillingInfo(checked));
    },
    handleUpdatingDeliveryType (shippingOptionId) {
        dispatch(cart.updateDeliveryType(shippingOptionId));
    },
    handleShippingRequest (address) {
        return dispatch(cart.fetchShippingOptions(address))
            .catch(e => {
                // Do nothing
            });
    },
    clearOrderProcessingError () {
        dispatch(order.receiveProcessingError(""));
    },
    onSubmit (values, action, args) {
        if (!!Object.keys(args.validate(values)).length) {
            return false;
        }

        if (values.paymentTypes === 'paypal') {
            return dispatch(cart.makeTransaction(true))
                .then(approvalLink => {
                    window.location.href = approvalLink.href;
                })
                .catch(err => browserHistory
                    .push(`/cart?error=${err}`)
                );
        }

        return dispatch(cart.makeTransaction(false))
            .then(details => {
                dispatch(cart.checkoutComplete(details));
                dispatch(order.receiveProcessingError(""));
                browserHistory
                    .push(`/cart/checkout/success?orderNumber=${details.OrderId}`);
            })
            .catch(err => dispatch(order.receiveProcessingError(err)));
    },
    setCheckoutStep ({ step }) {
        return dispatch(cart.setCheckoutStep({
            step
        }));
    }
});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    recalculateShippingOptions () {
        const {
            formDetails: {
                values = {}
            } = {}
        } = stateProps;

        dispatchProps.clearShippingOptions();

        return dispatchProps.handleShippingRequest({
            postalCode: values.postalCode_shipping,
            city: values.city_shipping,
            region: values.province_shipping,
            country: values.country_shipping
        });
    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
)(CheckoutContainer);
