import React from 'react';
import { connect } from 'react-redux';
import PartSearch from '../components/CarSearch';
import {
    partSearch
} from '../actions';
import { sortByAttribute } from '../helpers';

const mapStateToProps = ({ reducer, partSearch }) => ({
    reducer:reducer,
    year: {
        options: partSearch.availableYears.map(year => ({value: year})),
        value:  partSearch.filters.year
    },
    make:{
        options: Object.keys(partSearch.availableMakes)
            .map(key => partSearch.availableMakes[key])
            .sort(sortByAttribute('MakeName')),
        value:  partSearch.availableMakes[partSearch.filters.make],
        disabled: !partSearch.filters.year
    },
    model:{
        options: Object.keys(partSearch.availableModels)
            .map(key => partSearch.availableModels[key])
            .sort(sortByAttribute('ModelName')),
        value:  partSearch.availableModels[partSearch.filters.model],
        disabled: !partSearch.filters.make
    },
    engine:{
        options: Object.keys(partSearch.availableEngines)
            .map(key => partSearch.availableEngines[key])
            .sort(sortByAttribute('EngineDescription')),
        value:  partSearch.availableEngines[partSearch.filters.engine],
        disabled: !partSearch.filters.model
    }
});

const mapDispatchToProps = (dispatch) => ({
    handleYearChange: (val) => dispatch(partSearch.handleYearChange(val)),
    handleMakeChange: (val) => dispatch(partSearch.handleMakeChange(val)),
    handleModelChange: (val) => {
        dispatch(partSearch.handleModelChange(val))
            .then((engines = []) => {
                if (engines.length === 1) {
                    dispatch(partSearch.handleEngineChange(engines[0].EngineConfigId))
                        .then(() => dispatch(partSearch.fetchGroups()));
                }
            });
    },
    handleEngineChange: (engine) => {
        dispatch(partSearch.handleEngineChange(engine ? engine : ""))
            .then(() => {
                if (engine) {
                    dispatch(partSearch.fetchGroups());
                }
            });
    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PartSearch);
