import React from 'react';
import { connect } from 'react-redux';
import CreateAccount from '../components/_forms/CreateAccount';
import {
    user
} from '../actions';

const mapStateToProps = ({ user }) => ({
    Error: user.error
});

const mapDispatchToProps = (dispatch) => ({
    onSubmit (values) {
        const {
            email,
            password,
            orderId
        } = values;

        dispatch(user.createUser({
            email,
            password,
            orderId
        }))
    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CreateAccount);
