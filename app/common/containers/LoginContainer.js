import React from 'react';
import { connect } from 'react-redux';
import Login from '../components/_forms/Login';
import {
    user,
    modal
} from '../actions';

const mapStateToProps = ({ }) => ({});

const mapDispatchToProps = (dispatch) => ({
    onSubmit (values) {
        dispatch(modal.resetModalNotifications())
        dispatch(user.authenticate({
            email   : values.email,
            password: values.password,
            cartId  : values.cartId
        }));
    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Login);
