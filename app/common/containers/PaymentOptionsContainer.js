import React from 'react';
import { connect } from 'react-redux';
import PaymentOptions from '../components/_forms/PaymentOptions';

const mapStateToProps = ({
    form: {
        ProcessCheckoutForm: {
            values: {
                paymentTypes
            } = {}
        }
    }
}) => ({
    paymentType: paymentTypes
});

export default connect(
    mapStateToProps
)(PaymentOptions);
