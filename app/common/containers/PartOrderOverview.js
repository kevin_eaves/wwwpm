import { connect } from 'react-redux';
import PartOrderOverview from '../components/PartOrderOverview';

const mapStateToProps = ({ parts, cart }, { id }) => {
    const {
        items: {
            [id]: {
                ProductInfo: {
                    Inventory = {},
                    ...ProductInfo
                } = {}
            } = {}
        }
    } = parts;
    const {
        items: {
            [id]: cartData
        } = {}
    } = cart;

    return {
        id,
        title: ProductInfo.PartLabel,
        partNumber: ProductInfo.PartNumber,
        manufacturer: ProductInfo.ManuFacturerName,
        coreCost: Inventory.CoreCost,
        freightCost: Inventory.FreightCost,
        price: Inventory.Cost,
        ehc: Inventory.EHC,
        total: (Inventory.Cost + Inventory.CoreCost + Inventory.FreightCost + Inventory.EHC) * cartData.quantity,
        url: ProductInfo.DetailsURL || '/',
        image: ProductInfo.Images && ProductInfo.Images.substr(0, ProductInfo.Images.indexOf(';'))
                ?   ProductInfo.Images.substr(0, ProductInfo.Images.indexOf(';'))
                :   ProductInfo.Images
    }
};

export default connect(
    mapStateToProps
)(PartOrderOverview);
