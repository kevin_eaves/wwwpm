import React from 'react';
import { connect } from 'react-redux';
import ShippingFieldset from '../components/_forms/ShippingFieldset';

import {
    cart
} from '../actions';

const mapStateToProps = ({
    form: {
        ProcessCheckoutForm: {
            values = {}
        }
    },
    order: {
        countries,
        regions
    },
    cart: {
        shippingLoading,
        insuranceCost
    }
}, ownProps) => ({
    ...ownProps,
    values,
    countriesOptions: Object.keys(countries)
        .map(id => countries[id]),
    regionOptions: Object.keys(regions)
        .map(id => regions[id])
        .filter(region => {
            if (!values.country_shipping) {
                return false;
            }

            const selectedCountry = Object.keys(countries)
                .find(id => countries[id].value === values.country_shipping);

            return region.country == selectedCountry
        }),
    isLoadingShippingOptions: shippingLoading,
    insuranceCost
});

const mapDispatchToProps = (dispatch) => ({
    clearShippingOptions(){
        return dispatch(cart.clearShippingOptions());
    },
    handleShippingRequest (address) {
        return dispatch(cart.fetchShippingOptions(address))
            .catch(e => {
                // Do nothing
            });
    }
});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    recalculateShippingOptions () {
        const values = stateProps.values;

        dispatchProps.clearShippingOptions();

        return dispatchProps.handleShippingRequest({
            postalCode: values.postalCode_shipping,
            city: values.city_shipping,
            region: values.province_shipping,
            country: values.country_shipping
        });
    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
)(ShippingFieldset);
