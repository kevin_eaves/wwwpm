import React from 'react';
import { connect } from 'react-redux';
import Home from '../components/Home';
import slug from 'slug';
import {
    modal,
    cart,
    partSearch
} from '../actions';

class HomeContainer extends React.Component {
    static preloadData ({ store, params = {} } = {}) {
        const searchParams = parseSearchParams(params);

        return store.dispatch(partSearch.fetchYears())
            .then(() => store.dispatch(partSearch.fetchMakes()))
            .then(() => store.dispatch(partSearch.loadSearchBasedOnUrl(searchParams)))
            .then(() => store.dispatch(cart.fetchCartParts()));
    }

    constructor (props) {
        super(props);

        this.state = {
            mounted: true
        };
    }

    componentDidMount () {
        if (this.state.mounted && this.props.car.length > 4) {
            return;
        }

        const {
            context: {
                store
            },
            props: {
                routeParams
            }
        } = this;

        this.setState({
            mounted: true
        });

        return this.constructor.preloadData({
            store: this.context.store,
            params: this.props.params
        });
    }

    componentWillUnmount () {
        const {
            context: {
                store: {
                    dispatch
                }
            }
        } = this;

        this.setState({
            mounted: false
        });
        return dispatch(partSearch.wipeNecessarySearchValues('nuke'));
    }

    render() {
        const {
            showResults,
            car,
            selectedGroup,
            isLoading,
            urlParams,
            availableGroups,
            location,
            children,
            parsedVehicle,
            handleVehicleRequest,
            title
        } = this.props;

        return (
            <Home
                title={title}
                showResults={showResults}
                car={car}
                selectedGroup={selectedGroup}
                isLoading={isLoading}
                urlParams={urlParams}
                availableGroups={availableGroups}
                location={location}
                parsedVehicle={parsedVehicle}
                handleVehicleRequest={handleVehicleRequest}
            >
                {children}
            </Home>
        );
    }
}

HomeContainer.contextTypes = {
    store: React.PropTypes.object.isRequired
};

const mapStateToProps = ({ reducer, categories, partSearch }, ownProps) => {
    const year          = partSearch.filters.year;
    const make          = partSearch.filters.make && partSearch.availableMakes[partSearch.filters.make].MakeName;
    const model         = partSearch.filters.model && partSearch.availableModels[partSearch.filters.model].ModelName;
    const engine        = partSearch.filters.engine && partSearch.availableEngines[partSearch.filters.engine].EngineDescription;
    const car           = `${year ? `${year} ` : ''} ${make ? make : ''} ${model ? model : ''}${engine ? ` with ${engine}` : ''}`;
    const params        = ownProps.params;
    const selectedGroup = {
        group: categories.selected.group > 0 && categories.available.parents && categories.available.parents[categories.selected.group]
            ?   categories.available.parents[categories.selected.group].GroupName
            :   '',
        subGroup: categories.selected.subGroup > 0 && categories.available.subGroups && categories.available.subGroups[categories.selected.subGroup]
            ?   categories.available.subGroups[categories.selected.subGroup].PartTypeDescription
            :   ''
    };

    let isLoading = false;
    if(make && model && engine && Object.keys(categories.available).length < 1){
        isLoading = true;
    }

    let subGroupTitle = '';
    let groupTitle = '';
    let title = 'Home';

    if(params.group){
        const parentGroups = categories.available.parents;
        if (parentGroups) {
            const parentGroup = Object.keys(parentGroups).find((group) => {
                return slug(parentGroups[group].GroupName) == params.group;
            })

            groupTitle = parentGroups[parentGroup] ? parentGroups[parentGroup].GroupName : '';
        }
    }
    if(params.subGroup){
        const subGroups = categories.available.subGroups;
        if (subGroups) {
            const subGroup = Object.keys(subGroups).find((group) => {
                return slug(subGroups[group].PartTypeDescription) == params.subGroup;
            })
            subGroupTitle =  subGroups[subGroup] ? subGroups[subGroup].PartTypeDescription : '';
        }
    }

    const parsedParams = parseSearchParams(params);
    const isCategoryPage = Object
        .values(parsedParams)
        .some(val => val !== undefined);

    if (isCategoryPage) {
        const {
            year,
            make,
            model,
            engine
        } = parsedParams;

        title = `${year} ${make} ${model} ${engine} ${groupTitle || ''} ${subGroupTitle || ''}`.trim();
    }

    return {
        reducer:reducer,
        showResults: categories.available.parents && Object.keys(categories.available.parents).length,
        car,
        title: title || defaultTitle,
        selectedGroup,
        isLoading,
        urlParams: parseSearchParams(ownProps.params),
        availableGroups: categories.available,
        location: ownProps.location,
        parsedVehicle: {
            year,
            make,
            model,
            engine
        }
    }
}

const mapDispatchToProps = (dispatch) => ({
    handleVehicleRequest ({ year, make, model, engine }) {
        dispatch(modal.updatedAvailableItemDetails({
            modalType :'RequestVehiclePartsModal',
            year,
            make,
            model,
            engine
        }));
    }
});

function parseSearchParams ({ splat, ...params }) {
    if (typeof splat === 'undefined') {
        return params;
    }

    const [
        vehicleParams,
        group,
        subGroup
    ] = splat.split('/');
    const [
        year,
        make,
        model,
        engine
    ] = vehicleParams.split('_');

    return {
        year,
        make,
        model,
        engine,
        group,
        subGroup
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HomeContainer);
