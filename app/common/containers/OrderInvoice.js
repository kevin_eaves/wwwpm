import React from 'react';
import { connect } from 'react-redux';
import OrderInvoice from '../components/OrderInvoice';
import {
    order
} from '../actions';

class ViewOrderInvoice extends React.Component {
    constructor () {
        super();

        this.state = {
            isFetching: true
        };
    }

    static preloadData ({ params, store } = {}) {
        return store.dispatch(order.fetchInvoice({
            orderId: params.orderId
        }));
    }

    componentWillMount () {
        const {
            props: {
                params
            },
            context: {
                store: {
                    dispatch
                }
            }
        } = this;

        return dispatch(order.fetchInvoice({
            orderId: params.orderId
        }))
            .then(() => this.setState({
                isFetching: false
            }));
    }

    shouldComponentUpdate (props, state) {
        return this.state.isFetching !== state.isFetching;
    }

    render () {
        return (
            <OrderInvoice
                {...this.props}
                isFetching={this.state.isFetching}
            />
        );
    }
}

ViewOrderInvoice.contextTypes = {
    store: React.PropTypes.object.isRequired
};

const mapStateToProps = ({ order }) => ({
    invoice: order.invoice
});

export default connect(
    mapStateToProps
)(ViewOrderInvoice);
