import React from 'react';
import { connect } from 'react-redux';
import YearEngineForm from '../components/_forms/YearEngineForm';
import {
    modal,
    partSearch
} from '../actions';

const mapStateToProps = ({  partSearch }) => ({
    filters: partSearch.filters.tempValues,
    years: partSearch.availableYears.map(year => ({value: year})),
    engines: Object.keys(partSearch.availableEngines)
            .map(id => ({
                value: partSearch.availableEngines[id].EngineConfigId,
                label: partSearch.availableEngines[id].EngineDescription
            })
    )
})

const mapDispatchToProps = (dispatch) => ({
    searchForPart ({ year, make, model, engine }) {
        dispatch(partSearch.receiveYear(year));
        dispatch(partSearch.setFilterMake(make));
        dispatch(partSearch.setFilterModel(model));
        dispatch(partSearch.setFilterEngine(engine));
        dispatch(modal.resetModalValues());
        dispatch(partSearch.updateSearchUrl());
        dispatch(partSearch.fetchGroups({ year, make, model, engine }));
    },
    setYear (value, filters) {
        return Promise.resolve(dispatch(partSearch.receiveTemporaryFilter({
            year: value,
            engine: null
        })))
            .then(() => dispatch(partSearch.fetchEngines({
                year: value,
                make: filters.make,
                model: filters.model
            })));
    },
    setEngine (value) {
        dispatch(partSearch.receiveTemporaryFilter({engine: value}));
    }
});

const mergeProps = (stateProps, dispatchProps) => ({
    ...stateProps,
    setEngine:  dispatchProps.setEngine,
    searchForPart: dispatchProps.searchForPart,
    setYear (value) {
        dispatchProps.setYear(value, stateProps.filters);
    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
)(YearEngineForm);
