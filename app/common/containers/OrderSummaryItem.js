import { connect } from 'react-redux';
import OrderSummaryItem from '../components/OrderSummaryItem';

const mapStateToProps = ({ parts, cart }, { id }) => {
    const {
        items: {
            [id]: {
                vehicle = {},
                ProductInfo: {
                    Inventory = {},
                    ...ProductInfo
                } = {}
            } = {}
        }
    } = parts;

    const {
        items: {
            [id]: cartData
        } = {}
    } = cart;

    return {
        id,
        title: ProductInfo.PartLabel,
        partNumber: ProductInfo.PartNumber,
        manufacturer: ProductInfo.ManuFacturerName,
        coreCost: Inventory.CoreCost * cartData.quantity,
        freightCost: Inventory.FreightCost,
        price: Inventory.Cost,
        quantity: cartData.quantity,
        ehc: Inventory.EHC * cartData.quantity,
        total: Inventory.Cost * cartData.quantity,
        url: ProductInfo.DetailsURL || '/',
        image: ProductInfo.Images && ProductInfo.Images.substr(0, ProductInfo.Images.indexOf(';'))
                ?   ProductInfo.Images.substr(0, ProductInfo.Images.indexOf(';'))
                :   ProductInfo.Images,
        vehicle
    }
};

export default connect(
    mapStateToProps
)(OrderSummaryItem);
