import React from 'react';
import { connect } from 'react-redux';
import { getFormSyncErrors, hasSubmitFailed } from 'redux-form';
import DeliveryOptions from '../components/_forms/DeliveryOptions';
import decimal from 'decimal.js';

import {
    cart
} from '../actions';

const mapStateToProps = ({
    cart: {
        availableShippingTypes,
        shippingType,
        shippingLoading,
        insuranceCost,
        shippingError
    } = {},
    ...state
}) => {

    let submitted = false;
    if (state.form.ProcessCheckoutForm.submitFailed || state.form.ProcessCheckoutForm.submitSucceeded) {
        submitted = true;
        state.form.ProcessCheckoutForm.beenSubmitted = true;
    }

    let deliveryInsurance = 0;
    let values = {};
    if (state.form.ProcessCheckoutForm && state.form.ProcessCheckoutForm.values) {
        deliveryInsurance = state.form.ProcessCheckoutForm.values.deliveryInsurance;
        values = state.form.ProcessCheckoutForm.values;
    }

    let pleaseChooseOption = false;
    if (state.form.ProcessCheckoutForm.beenSubmitted && getFormSyncErrors('ProcessCheckoutForm')(state) || state.form.ProcessCheckoutForm.values.deliveryType && state.form.ProcessCheckoutForm.beenSubmitted && getFormSyncErrors('ProcessCheckoutForm')(state)) {
        pleaseChooseOption = getFormSyncErrors('ProcessCheckoutForm')(state).deliveryType;
    }

    return {
        values,
        deliveryInsurance,
        insuranceCostValue: insuranceCost
            ?   new decimal(insuranceCost)
                    .toFixed(2)
            :   0,
        availableShippingTypes,
        shippingType: availableShippingTypes[shippingType] &&
            availableShippingTypes[shippingType].label || '',
        loading: shippingLoading,
        pleaseChooseOption,
        error: shippingError
    }
};

const mapDispatchToProps = (dispatch) => ({
    clearShippingOptions(){
        return dispatch(cart.clearShippingOptions());
    },
    handleShippingRequest (address) {
        return dispatch(cart.fetchShippingOptions(address))
            .catch(e => {
                // Do nothing
            });
    },
    handleUpdatingDeliveryType (shippingOptionId) {
        dispatch(cart.updateDeliveryType(shippingOptionId));
    }
});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    recalculateShippingOptions () {
        const values = stateProps.values;
        dispatchProps.clearShippingOptions();

        return dispatchProps.handleShippingRequest({
            postalCode: values.postalCode_shipping,
            city: values.city_shipping,
            region: values.province_shipping,
            country: values.country_shipping
        });
    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
)(DeliveryOptions);
