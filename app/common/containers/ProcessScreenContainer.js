import React from 'react';
import { connect } from 'react-redux';
import ProcessScreen from '../components/ProcessScreen';
import { browserHistory } from 'react-router';
import {
    cart
} from '../actions';
import querystring from 'querystring';


class ProcessScreenContainer extends React.Component {

    componentDidMount(){
        const paypalValues = querystring.parse(window.location.search.slice(1, window.location.search.length));
        this.props.finalizePaypalPayment(paypalValues)
        .then((result)=>{
            browserHistory.push(`/cart/checkout/success?orderNumber=${result.OrderId}`)
        })
    }

    render(){
        return(
            <ProcessScreen />
        )
    }
}

const mapDispatchToProps = (dispatch, { partId }) => ({
    finalizePaypalPayment (values) {
        return dispatch(cart.finalizePaypalPayment(values));
    }
});

export default connect(
    null,
    mapDispatchToProps
)(ProcessScreenContainer);
