import React from 'react';
import { connect } from 'react-redux';
import Modal from '../components/Modal';
import {
    modal
} from '../actions';

const mapStateToProps = ({ reducer, modal }) => ({
    modalType: modal.body.modalType || '',
    error: modal.error,
    success: modal.success
});

const mapDispatchToProps = (dispatch) => ({
    resetModal () {
        dispatch(modal.resetModalValues())
        //time of animation
        setTimeout(()=>{
            dispatch(modal.resetModalNotifications())
        }, 300);
    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Modal);
