import React from 'react';
import { connect } from 'react-redux';
import PartsTemplate from '../components/_templates/PartsTemplate';
import {
    user,
    modal,
    cart,
    partSearch
} from '../actions';

class PartsTemplateContainer extends React.Component {
    static preloadData ({ store } = {}) {
        return store.dispatch(cart.fetchCartParts());
    }

    componentDidMount () {
        const {
            store: {
                dispatch
            }
        } = this.context;

        dispatch(cart.fetchCartParts());
    }

    render() {
        const {
            cartCount,
            handleModalRequest,
            handleLogout,
            children
        } = this.props;

        return (
            <PartsTemplate
                cartCount={cartCount}
                handleModalRequest={handleModalRequest}
                handleLogout={handleLogout}
            >
                {children}
            </PartsTemplate>
        );
    }
}

PartsTemplateContainer.contextTypes = {
    store: React.PropTypes.object.isRequired
};

const mapStateToProps = ({ cart }) => ({
    cartCount: Object.keys(cart.items)
        .reduce((total, partId) => total += parseInt(cart.items[partId].quantity, 10), 0)
})

const mapDispatchToProps = (dispatch) => ({
    handleModalRequest (value) {
        dispatch(modal.updatedAvailableItemDetails(value));
    },
    handleLogout () {
        dispatch(user.destroyToken());
    }
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PartsTemplate);
