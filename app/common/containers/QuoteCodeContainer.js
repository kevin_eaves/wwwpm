import React from 'react';
import { connect } from 'react-redux';
import { SubmissionError } from 'redux-form';
import QuoteCode from '../components/_forms/QuoteCode';
import {
    cart,
    modal
} from '../actions';

const mapStateToProps = ({ }) => ({});

const mapDispatchToProps = (dispatch) => ({
    onSubmit ({quoteCode}) {
        return dispatch(cart.addPartWithQuote(quoteCode))
            .then(() => dispatch(cart.fetchCartParts()))
            .then(() => dispatch(modal.resetModalValues()))
            .catch(({ message }) => {
                throw new SubmissionError({
                    _error: message
                });
            });
    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(QuoteCode);
