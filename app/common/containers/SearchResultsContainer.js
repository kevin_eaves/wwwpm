import React from 'react';
import { connect } from 'react-redux';
import SearchResults from '../components/SearchResults';
import {
    partSearch
} from '../actions';
import slug from 'slug';



const mapStateToProps = ({ reducer, categories, partResults, parts, routing }, ownProps) => ({
    showLoader: !!categories.selected.subGroup,
    categories: categories.available,
    parts: parts.searchedIds.map((id) => parts.items[id]),
    selectedGroup: ownProps,
    routing,
    vehicleSlug: routing.locationBeforeTransitions && routing.locationBeforeTransitions.pathname.split('/')[1]
});

const mapDispatchToProps = (dispatch, { pathName, selectedGroup, urlParams, availableGroups }) => ({
    selectGroupFilter (group) {
        dispatch(partSearch.handleGroupChange(group));
    },
    selectSubGroupFilter (group) {
        dispatch(partSearch.handleSubGroupChange(group));
        dispatch(partSearch.fetchPartSearchResults(group));
    }
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SearchResults);
