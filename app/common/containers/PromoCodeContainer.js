import React from 'react';
import { connect } from 'react-redux';
import PromoCode from '../components/_forms/PromoCode';
import {
    promotions
} from '../actions';

const mapStateToProps = ({ promotions }) => ({
    error: promotions.error,
    code: promotions.code,
    discount: parseFloat(promotions.discount) * 100
});

const mapDispatchToProps = (dispatch) => ({
    fetchPromo (promo) {
        dispatch(promotions.applyPromoCode(promo));
    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PromoCode);
