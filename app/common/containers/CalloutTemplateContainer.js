import React from 'react';
import { connect } from 'react-redux';
import CalloutTemplate from '../components/_templates/CalloutTemplate';
import {
    modal,
    cart,
    partSearch
} from '../actions';

class CalloutTemplateContainer extends React.Component {
    static preloadData ({ store } = {}) {
        return store.dispatch(cart.fetchCartParts());
    }

    componentDidMount () {
        const {
            store: {
                dispatch
            }
        } = this.context;

        dispatch(cart.fetchCartParts());
    }

    render() {
        const {
            cartCount,
            handleModalRequest,
            children
        } = this.props;

        return (
            <CalloutTemplate
                cartCount={cartCount}
                handleModalRequest={handleModalRequest}
            >
                {children}
            </CalloutTemplate>
        );
    }
}

CalloutTemplateContainer.contextTypes = {
    store: React.PropTypes.object.isRequired
};

const mapStateToProps = ({ cart }) => ({
    cartCount: cart.items.length
})

const mapDispatchToProps = (dispatch) => ({
    handleModalRequest (value) {
        dispatch(modal.updatedAvailableItemDetails(value));
    }
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CalloutTemplateContainer);
