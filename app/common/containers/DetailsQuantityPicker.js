import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import TransitionGroup from 'react-addons-css-transition-group';
import QuantityPicker from '../components/QuantityPicker';
import {
    cart,
    modal
} from '../actions';

class DetailsQuantityPicker extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
            quantity: 1,
            submitted: false
        };
    }

    handleSubmit = () => {
        const {
            state: {
                quantity
            },
            props: {
                adjustQuantity
            }
        } = this;

        adjustQuantity(quantity);
        this.adjustQuantity(1);

        this.setState({
            submitted: true
        });
    }

    adjustQuantity = (quantity) => {
        this.setState({ quantity });
    }

    render () {
        const {
            state: {
                quantity,
                submitted
            },
            props: {
                max,
                quantityInCart = 0
            }
        } = this;

        return (
            <TransitionGroup
                transitionName="defaultTransitionGroup"
                transitionEnterTimeout={0}
                transitionLeaveTimeout={0}
            >
                {
                    submitted
                        ?   <p key="submitted">
                                Item added! <Link className="_textLink" to="/cart">View cart</Link>
                            </p>
                        :   <div key="picker">
                                <QuantityPicker
                                    value={quantity}
                                    max={max - quantityInCart}
                                    adjustQuantity={this.adjustQuantity}
                                    onSubmit={this.handleSubmit}
                                />
                            </div>
                }
            </TransitionGroup>
        );
    }
}

const mapStateToProps = ({ parts: { items }, cart }, { partId }) => {
    const part = items[partId].ProductInfo;
    const quantityInCart = cart.items[partId];

    return {
        max: part.Inventory.QuantityOnHand,
        quantityInCart
    };
};

const mapDispatchToProps = (dispatch, { partId }) => ({
    adjustQuantity (value) {
        const quantity = parseInt(value, 10);

        dispatch(cart.fetchVehicleId())
            .then((vehicleId) => dispatch(cart.updatePartQuantity({
                partId,
                quantity,
                vehicleId,
                merge: true
            })))
            .then(() => dispatch(modal.updatedAvailableItemDetails({
                modalType: 'CartUpdated'
            })));
    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DetailsQuantityPicker);
