import React from 'react';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import Cart from '../components/_forms/Contact';
import {
    mailing,
    modal
} from '../actions';

const mapStateToProps = ({}) => ({});

const mapDispatchToProps = (dispatch) => ({
    onSubmit(values){
        dispatch(modal.resetModalNotifications())
        dispatch(mailing.sendContactForm(values))
        .then((response)=>{
            if(response.error){
                throw response.error;
            }
            dispatch(modal.receiveSuccess(response.message));
        })
        .catch((error)=>{
            dispatch(modal.receiveErrors(error));
        });
    }
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Cart);
