import React from 'react';
import slug from 'slug';
import { connect } from 'react-redux';
import ResultsList from '../components/ResultsList';
import {
    partSearch
} from '../actions';

class BrowseMakes extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
            isFetching: true
        };
    }

    static preloadData ({ params, store } = {}) {
        return store.dispatch(partSearch.fetchMakes());
    }

    componentWillMount () {
        const {
            store: {
                dispatch
            }
        } = this.context;

        return dispatch(partSearch.fetchMakes())
            .then(() => this.setState({
                isFetching: false
            }));
    }

    shouldComponentUpdate (props, state) {
        return this.state.isFetching !== state.isFetching;
    }

    render () {
        return (
            <ResultsList
                {...this.props}
                isFetching={this.state.isFetching}
            />
        );
    }
}

BrowseMakes.contextTypes = {
    store: React.PropTypes.object.isRequired
};

const mapStateToProps = ({ partSearch }) => ({
    title: 'Vehicle Makes',
    instructionalText: 'Select a make',
    results: Object.keys(partSearch.availableMakes)
        .map(id => ({
            id: partSearch.availableMakes[id].MakeId,
            name: partSearch.availableMakes[id].MakeName,
            url: `/vehicles/${slug(partSearch.availableMakes[id].MakeName)}`
        }))
        .sort((a, b) => {
            if (a.name > b.name) {
                return 1;
            }

            if (a.name < b.name) {
                return -1;
            }

            return 0;
        })
        .reduce((cache, item) => {
            const firstLetter = item.name.charAt(0).toUpperCase();

            return {
                ...cache,
                [firstLetter]: [
                    ...cache[firstLetter] || [],
                    item
                ]
            }
        }, {})
});

export default connect(
    mapStateToProps
)(BrowseMakes);
