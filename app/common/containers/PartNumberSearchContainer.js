import React from 'react';
import { connect } from 'react-redux';
import PartNumberSearch from '../components/_forms/PartNumberSearch';
import {
    partSearch
} from '../actions';

const mapStateToProps = ({ form }) => ({
    active: form && form.PartNumberSearchForm && !!form.PartNumberSearchForm.values
})

const mapDispatchToProps = (dispatch) => ({});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PartNumberSearch);
