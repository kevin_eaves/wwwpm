import { CronJob } from 'cron';
import sitemap from 'sitemap';
import fs from 'fs';
import slug from 'slug';
import {
    dirname,
    join
} from 'path';
import mkdirp from 'mkdirp';
import serverConfig from '../../config/server';
import 'whatwg-fetch';
import Promise from 'bluebird';

// export default new CronJob({
//     // cronTime: '* * * * *',
//     cronTime: '* * * * *',
//     onTick: generateSitemap,
//     start: true,
//     timeZone: 'UTC'
// });

const baseUrl = 'https://partsmonkey.com/';
const sitemapDir = join(__dirname, '../../../public/sitemaps');

export default () => mkdirp(sitemapDir, err => {
    if (err) {
        return console.log('Could not create sitemap directory');
    }

    console.log('generatedSitemap');
    console.time('exec');

    let vehicleSitemaps = {
        index: sitemap.createSitemap({
            hostname: baseUrl,
            urls: [
                {
                    url: '',
                    changefreq: 'monthly'
                },
                {
                    url: 'about',
                    changefreq: 'monthly'
                },
                {
                    url: 'shipping-and-return-warranty',
                    changefreq: 'monthly'
                },
                {
                    url: 'privacy-policy',
                    changefreq: 'monthly'
                },
                {
                    url: 'terms-of-use',
                    changefreq: 'monthly'
                },
                {
                    url: 'cart/checkout',
                    changefreq: 'monthly'
                }
            ]
        })
    };

    return fetch(`${serverConfig.apiHost}/years`)
        .then(response => response.json())
        .then(years => Promise.mapSeries(
            [ ...years ],
            year => {
                vehicleSitemaps[year] = undefined;
                let tempSitemap = {};

                return fetch(`${serverConfig.apiHost}/years/${year}/makes`)
                    .then(response => response.json())
                    .then(makes => Promise.mapSeries(
                        [ ...makes ],
                        make => {

                            // Create sitemap for current year/ make
                            tempSitemap[make.MakeName] = sitemap.createSitemap({
                                hostname: baseUrl
                            });

                            return fetch(`${serverConfig.apiHost}/years/${year}/makes/${make.MakeId}/models`)
                                .then(response => response.json())
                                .then(models => Promise.mapSeries(
                                    [ ...models ],
                                    model => {

                                        // Add entry for current year/ make/ model
                                        tempSitemap[make.MakeName].add(formatItem({
                                            url: slug(`${year}_${make.MakeName}_${model.ModelName}`)
                                        }));

                                        return fetch(`${serverConfig.apiHost}/years/${year}/makes/${make.MakeId}/models/${model.ModelID}/engines`)
                                            .then(response => response.json())
                                            .then(engines => Promise.mapSeries(
                                                [ ...engines ],
                                                engine => {

                                                    // Add entry for current year/ make/ model/ engine
                                                    tempSitemap[make.MakeName].add(formatItem({
                                                        url: slug(`${year}_${make.MakeName}_${model.ModelName}_${engine.EngineDescription}`)
                                                    }));

                                                    return fetch(`${serverConfig.apiHost}/groups?year=${year}&makeId=${make.MakeId}&modelId=${model.ModelID}&engineId=${engine.EngineConfigId}`)
                                                        .then(response => response.json())
                                                        .then(categories => {

                                                            // Add entries for current categories
                                                            return categories.parents
                                                                .map(category => {
                                                                    const vehicleUrl = slug(`${year}_${make.MakeName}_${model.ModelName}_${engine.EngineDescription}`);

                                                                    tempSitemap[make.MakeName]
                                                                        .add(formatItem({
                                                                            url: vehicleUrl + '/' + slug(category.GroupName)
                                                                        }));

                                                                    return categories.subGroups
                                                                        .filter(group => category.subGroups.indexOf(parseInt(group.PartTypeNumber, 10)) > -1)
                                                                        .map(group => tempSitemap[make.MakeName]
                                                                            .add(formatItem({
                                                                                url: vehicleUrl + '/' + slug(category.GroupName) + '/' + slug(group.PartTypeDescription)
                                                                            }))
                                                                        )
                                                                });
                                                        })
                                                        .catch(e => console.warn(`Failed to fetch categories for ${year} / ${make.MakeName} / ${model.ModelName} / ${engine.EngineDescription}`, e));
                                                }
                                            ))
                                            .catch(e => console.warn(`Failed to fetch engines for ${year} / ${make.MakeName} / ${model.ModelName}`));
                                    }
                                ))
                                .catch(e => console.warn(`Failed to fetch models for ${year} / ${make.MakeName}`));
                        }
                    ))
                    .then(() => mkdirp(join(sitemapDir, `${year}`), err => {
                        if (err) {
                            return console.warn(`Could not create directory for:`, year);
                        }

                        const yearIndex = sitemap.buildSitemapIndex({
                            urls: Object.keys(tempSitemap)
                                .map(make => `${baseUrl}sitemaps/${year}/${slug(make)}.xml`)
                        });

                        writeSitemap(({
                            identifier: join(sitemapDir, `${year}`, 'index.xml'),
                            data: yearIndex.toString()
                        }));

                        return Object
                            .keys(tempSitemap)
                            .forEach(make => {
                                const makeSitemap = tempSitemap[make];

                                const fileName = `${slug(make)}.xml`;
                                const dir = join(
                                    sitemapDir,
                                    year !== 'index' ? `${year}` : ''
                                );

                                return mkdirp(dir, err => {
                                    if (err) {
                                        return console.warn(`Could not make directory`, dir);
                                    }

                                    return writeSitemap({
                                        identifier: join(dir, fileName),
                                        data: makeSitemap.toString()
                                    });
                                });
                            });
                    }))
                    .catch(e => console.warn(`Failed to fetch makes for ${year}`, e));
            }
        ))
        .then(() => {
            writeSitemap({
                identifier: join(sitemapDir, 'index.xml'),
                data: vehicleSitemaps.index.toString()
            });

            const generatedSitemap = sitemap.buildSitemapIndex({
                urls: Object.keys(vehicleSitemaps)
                    .map(name => name === 'index'
                        ?   `${baseUrl}sitemaps/${name}.xml`
                        :   `${baseUrl}sitemaps/${name}/index.xml`
                    )
            });

            console.timeEnd('exec');

            return writeSitemap({
                identifier: join(sitemapDir, '../sitemap.xml'),
                data: generatedSitemap.toString()
            });
        })
        .catch(console.log);
});

function writeSitemap ({ identifier, data }) {
    return fs.writeFile(identifier, data, { flag: 'w' }, err => {
        if (err) {
            console.warn('Could not write sitemap for:', identifier);
        }
    });
}

function formatItem ({ url, changefreq = 'weekly' }) {
    return {
        url,
        changefreq
    };
}
