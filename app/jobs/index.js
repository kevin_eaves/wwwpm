import generateSitemap from './generate-sitemap';

// Only run jobs on first instance if running PM2 cluster
if (process.env.NODE_ENV === 'production' && process.env.NODE_APP_INSTANCE && process.env.NODE_APP_INSTANCE === '0') {
    generateSitemap();
}
