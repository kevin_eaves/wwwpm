const apiHost = (() => {
    if (process.env.API_HOST) {
        return process.env.API_HOST;
    }

    if (typeof window !== 'undefined') {
        return window.location.origin + '/api';
    }

    return 'http://api.partsmonkey.dev:8080';
})();

const baseUrl = (() => {
    if (typeof window !== 'undefined') {
        return window.location.origin;
    }

    return 'http://partsmonkey.dev:3000';
})();

const cookieOptions = (() => {
    let options = {
        path: '/',
        secure: false
    };

    if (typeof window !== 'undefined' && window.location.origin.indexOf('partsmonkey.com') > -1) {
        options.secure = true;
    }

    return options;
})();

export default {
    port: process.env.APP_PORT || 8080,
    paypalHost: process.env.API_PAYPAL_HOST || 'https://api.sandbox.paypal.com',
    baseUrl,
    apiHost,
    cookieOptions
};
